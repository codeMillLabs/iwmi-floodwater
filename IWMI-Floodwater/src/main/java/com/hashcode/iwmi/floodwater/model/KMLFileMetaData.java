/*
 * FILENAME
 *     KMLFileMetaData.java
 *
 * FILE LOCATION
 *     $Source$
 *
 * VERSION
 *     $Id$
 *         @version       $Revision$
 *         Check-Out Tag: $Name$
 *         Locked By:     $Lockers$
 *
 * FORMATTING NOTES
 *     * Lines should be limited to 78 characters.
 *     * Files should contain no tabs.
 *     * Indent code using four-character increments.
 *
 * COPYRIGHT
 *     Copyright (C) 2013 hashCode Solutions Pty. Ltd. All rights reserved.
 *     This software is the confidential and proprietary information of
 *     hashCode Solutions ("Confidential Information"). You shall not
 *     disclose such Confidential Information and shall use it only in
 *     accordance with the terms of the licence agreement you entered into
 *     with hashCode Solutions.
 */

package com.hashcode.iwmi.floodwater.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.codehaus.jackson.annotate.JsonProperty;

import com.hashcode.iwmi.floodwater.domain.GeoCoordinate;

//
// IMPORTS
// NOTE: Import specific classes without using wildcards.
//

/**
 * <p>
 * KML File meta data model for JSON.
 * </p>
 * 
 * @author Amila Silva
 * @email amilasilva88@gmail.com
 **/
public class KMLFileMetaData implements Serializable
{

    private static final long serialVersionUID = -779519435941598578L;

    private String id;
    private String name;
    private int version;

    @JsonProperty("map_zoom_level")
    private int mapZoomLevel;

    @JsonProperty("cam_position")
    private GeoCoordinate camPosition;

    @JsonProperty("kml_files")
    private List<String> kmlFiles = new ArrayList<>();

    /**
     * <p>
     * Getter for id.
     * </p>
     * 
     * @return the id
     */
    public String getId()
    {
        return id;
    }

    /**
     * <p>
     * Setting value for id.
     * </p>
     * 
     * @param id
     *            the id to set
     */
    public void setId(String id)
    {
        this.id = id;
    }

    /**
     * <p>
     * Getter for name.
     * </p>
     * 
     * @return the name
     */
    public String getName()
    {
        return name;
    }

    /**
     * <p>
     * Setting value for name.
     * </p>
     * 
     * @param name
     *            the name to set
     */
    public void setName(String name)
    {
        this.name = name;
    }

    /**
     * <p>
     * Getter for version.
     * </p>
     * 
     * @return the version
     */
    public int getVersion()
    {
        return version;
    }

    /**
     * <p>
     * Setting value for version.
     * </p>
     * 
     * @param version
     *            the version to set
     */
    public void setVersion(int version)
    {
        this.version = version;
    }

    /**
     * <p>
     * Getter for mapZoomLevel.
     * </p>
     * 
     * @return the mapZoomLevel
     */
    public int getMapZoomLevel()
    {
        return mapZoomLevel;
    }

    /**
     * <p>
     * Setting value for mapZoomLevel.
     * </p>
     * 
     * @param mapZoomLevel
     *            the mapZoomLevel to set
     */
    public void setMapZoomLevel(int mapZoomLevel)
    {
        this.mapZoomLevel = mapZoomLevel;
    }

    /**
     * <p>
     * Getter for camPosition.
     * </p>
     * 
     * @return the camPosition
     */
    public GeoCoordinate getCamPosition()
    {
        return camPosition;
    }

    /**
     * <p>
     * Setting value for camPosition.
     * </p>
     * 
     * @param camPosition
     *            the camPosition to set
     */
    public void setCamPosition(GeoCoordinate camPosition)
    {
        this.camPosition = camPosition;
    }

    /**
     * <p>
     * Getter for kmlFiles.
     * </p>
     * 
     * @return the kmlFiles
     */
    public List<String> getKmlFiles()
    {
        return kmlFiles;
    }

    /**
     * <p>
     * Setting value for kmlFiles.
     * </p>
     * 
     * @param kmlFiles
     *            the kmlFiles to set
     */
    public void setKmlFiles(List<String> kmlFiles)
    {
        this.kmlFiles = kmlFiles;
    }

    /**
     * {@inheritDoc}
     *
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString()
    {
        return String.format(
            "KMLFileMetaData [id=%s, name=%s, version=%s, mapZoomLevel=%s, camPosition=%s, kmlFiles=%s]", id, name,
            version, mapZoomLevel, camPosition, kmlFiles);
    }

}
