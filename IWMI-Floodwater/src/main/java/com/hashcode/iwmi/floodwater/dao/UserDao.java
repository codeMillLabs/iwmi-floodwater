/*
 * FILENAME
 *     UserDao.java
 *
 * FILE LOCATION
 *     $Source$
 *
 * VERSION
 *     $Id$
 *         @version       $Revision$
 *         Check-Out Tag: $Name$
 *         Locked By:     $Lockers$
 *
 * FORMATTING NOTES
 *     * Lines should be limited to 78 characters.
 *     * Files should contain no tabs.
 *     * Indent code using four-character increments.
 *
 * COPYRIGHT
 *     Copyright (C) 2013 hashCode Solutions Pty. Ltd. All rights reserved.
 *     This software is the confidential and proprietary information of
 *     hashCode Solutions ("Confidential Information"). You shall not
 *     disclose such Confidential Information and shall use it only in
 *     accordance with the terms of the licence agreement you entered into
 *     with hashCode Solutions.
 */

package com.hashcode.iwmi.floodwater.dao;

import com.hashcode.iwmi.floodwater.domain.User;

//
// IMPORTS
// NOTE: Import specific classes without using wildcards.
//

/**
 * <p>
 * UserDao interface.
 * </p>
 *
 * @author Manuja
 *
 * @version $Id$
 */
public interface UserDao extends GenericDao<User, Long>
{
    /**
     * <p>
     * Authenticate user for given user name and password.
     * </p>
     *
     * @param username - user name
     * @param password - password
     * 
     * @return user instance
     */
    User authenticateUser(String username, String password);
    
    /**
     * <p>
     * Get user for username.
     * </p>
     *
     * @param username - user name
     * 
     * @return user instance
     */
    User getUser(String username);
}
