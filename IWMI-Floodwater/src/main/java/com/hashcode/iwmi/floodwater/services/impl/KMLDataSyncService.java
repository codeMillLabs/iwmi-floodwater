/*
 * FILENAME
 *     KMLDataSyncService.java
 *
 * FILE LOCATION
 *     $Source$
 *
 * VERSION
 *     $Id$
 *         @version       $Revision$
 *         Check-Out Tag: $Name$
 *         Locked By:     $Lockers$
 *
 * FORMATTING NOTES
 *     * Lines should be limited to 78 characters.
 *     * Files should contain no tabs.
 *     * Indent code using four-character increments.
 *
 * COPYRIGHT
 *     Copyright (C) 2014 hashCode Solutions Pty. Ltd. All rights reserved.
 *     This software is the confidential and proprietary information of
 *     hashCode Solutions ("Confidential Information"). You shall not
 *     disclose such Confidential Information and shall use it only in
 *     accordance with the terms of the licence agreement you entered into
 *     with hashCode Solutions.
 */

package com.hashcode.iwmi.floodwater.services.impl;

import static com.hashcode.iwmi.floodwater.util.FloodwaterUtils.getLastModifiedDate;
import static com.hashcode.iwmi.floodwater.util.FloodwaterUtils.isModified;

import java.time.LocalDateTime;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import com.hashcode.iwmi.floodwater.dao.GCMRegIdDao;
import com.hashcode.iwmi.floodwater.domain.GCMRegId;
import com.hashcode.iwmi.floodwater.model.NotificationMessage;
import com.hashcode.iwmi.floodwater.services.DataSyncService;
import com.hashcode.iwmi.floodwater.util.Post2GCM;

//
// IMPORTS
// NOTE: Import specific classes without using wildcards.
//

/**
 * <p>
 * KML map data Sync service with registered clients.
 * </p>
 *
 * @author Amila Silva
 * @email amilasilva88@gmail.com
 * @version $Id$
 **/
@Service
public class KMLDataSyncService implements DataSyncService
{
    private static final Logger log = LoggerFactory.getLogger(KMLDataSyncService.class);

    @Value("${kml.meta.prop.filepath}")
    private String kmlMetaFile;

    @Value("${flood.app.apikey}")
    private String apiKey;

    @Autowired
    private GCMRegIdDao gcmRegIdDao;

    private static LocalDateTime cacheLastModified = LocalDateTime.now().minusDays(10000);

    /**
     * {@inheritDoc}
     *
     * @see com.hashcode.iwmi.floodwater.services.DataSyncService#checkForDataModification()
     */
    @Scheduled(cron = "* 0/2 * * * ?")
    @Override
    public void checkForDataModification()
    {
        try
        {
            log.debug("checkForDataModification [ last Modified date : {} ]", cacheLastModified);
            LocalDateTime kmlMetaLastModified = getLastModifiedDate(kmlMetaFile);

            boolean isModified = isModified(cacheLastModified, kmlMetaLastModified);

            if (isModified)
            {
                NotificationMessage message = new NotificationMessage();
                message.createData(NotificationMessage.SYNC_MAP, "Sync KML Map", "");

                List<GCMRegId> regIds = gcmRegIdDao.findAll();
                regIds.stream().forEach(i -> {
                    message.addRegId(i.getRegId());
                });

                log.info("Data Sync will post to GCM in a while, Message :{}", message);
                new Thread(() -> Post2GCM.post(apiKey, message)).start();
            }
            cacheLastModified = kmlMetaLastModified;
            log.debug("checkForDataModification [ Modified : {} ]", isModified);
        }
        catch (Exception e)
        {
            log.error("Error occurred while checking for KML meta data modification,", e);
        }
    }

}
