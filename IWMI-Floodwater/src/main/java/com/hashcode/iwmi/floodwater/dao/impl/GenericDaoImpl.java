/*
 * FILENAME
 *     GenericDaoImpl.java
 *
 * FILE LOCATION
 *     $Source$
 *
 * VERSION
 *     $Id$
 *         @version       $Revision$
 *         Check-Out Tag: $Name$
 *         Locked By:     $Lockers$
 *
 * FORMATTING NOTES
 *     * Lines should be limited to 78 characters.
 *     * Files should contain no tabs.
 *     * Indent code using four-character increments.
 *
 * COPYRIGHT
 *     Copyright (C) 2014 hashCode Solutions Pty. Ltd. All rights reserved.
 *     This software is the confidential and proprietary information of
 *     hashCode Solutions ("Confidential Information"). You shall not
 *     disclose such Confidential Information and shall use it only in
 *     accordance with the terms of the licence agreement you entered into
 *     with hashCode Solutions.
 */
package com.hashcode.iwmi.floodwater.dao.impl;

import java.io.Serializable;
import java.lang.reflect.ParameterizedType;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceContextType;
import javax.persistence.Query;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Projections;
import org.springframework.transaction.annotation.Transactional;

import com.hashcode.iwmi.floodwater.dao.GenericDao;

/**
 * <p>
 * Generic Dao Implementation.
 * </p>
 * 
 * @author manuja
 * 
 * @param <T>
 * @param <PK>
 * 
 */
@Transactional
public abstract class GenericDaoImpl<T, PK extends Serializable> implements GenericDao<T, PK>
{
    protected EntityManager entityManager;
    private final Class<T> persistentClass;

    /**
     * <p>
     * Default constructor.
     * </p>
     **/
    @SuppressWarnings("unchecked")
    public GenericDaoImpl()
    {
        this.persistentClass =
            (Class<T>) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0];
    }

    /**
     * <p>
     * Setter for entityManager (injected by Spring configuration).
     * </p>
     * 
     * @param anEntityManager
     *            the entity manager for this Dao
     **/
    @PersistenceContext(type = PersistenceContextType.TRANSACTION)
    public void setEntityManager(final EntityManager anEntityManager)
    {
        this.entityManager = anEntityManager;
    }

    /**
     * 
     * {@inheritDoc}
     */
    public void create(final T inNewInstance)
    {
        entityManager.persist(inNewInstance);

    }

    /**
     * 
     * {@inheritDoc}
     */
    public void delete(final T inPersistentObject)
    {
        entityManager.remove(inPersistentObject);
    }

    /**
     * 
     * {@inheritDoc}
     */
    public T findById(final PK inId)
    {
        return entityManager.find(persistentClass, inId);
    }

    /**
     * 
     * {@inheritDoc}
     */
    public T update(final T inTransientObject)
    {
        return entityManager.merge(inTransientObject);
    }

    /**
     * 
     * {@inheritDoc}
     */
    @SuppressWarnings("unchecked")
    public List<T> findAll()
    {
        Query query = entityManager.createQuery("FROM " + persistentClass.getName());
        return query.getResultList();
    }

    /**
     * {@inheritDoc}
     * 
     * @see com.hashcode.telcoapp.common.dao.GenericDao#countAll()
     */
    public long countAll()
    {
        Session session = (Session) entityManager.getDelegate();
        Criteria crit = session.createCriteria(persistentClass).setProjection(Projections.rowCount());

        return (Long) crit.list().get(0);
    }
}
