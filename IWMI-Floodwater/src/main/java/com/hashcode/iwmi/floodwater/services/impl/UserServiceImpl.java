/*
 * FILENAME
 *     UserServiceImpl.java
 *
 * FILE LOCATION
 *     $Source$
 *
 * VERSION
 *     $Id$
 *         @version       $Revision$
 *         Check-Out Tag: $Name$
 *         Locked By:     $Lockers$
 *
 * FORMATTING NOTES
 *     * Lines should be limited to 78 characters.
 *     * Files should contain no tabs.
 *     * Indent code using four-character increments.
 *
 * COPYRIGHT
 *     Copyright (C) 2013 hashCode Solutions Pty. Ltd. All rights reserved.
 *     This software is the confidential and proprietary information of
 *     hashCode Solutions ("Confidential Information"). You shall not
 *     disclose such Confidential Information and shall use it only in
 *     accordance with the terms of the licence agreement you entered into
 *     with hashCode Solutions.
 */

package com.hashcode.iwmi.floodwater.services.impl;

import static com.hashcode.iwmi.floodwater.util.FloodwaterUtils.encrypt;

import java.security.NoSuchAlgorithmException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hashcode.iwmi.floodwater.dao.UserDao;
import com.hashcode.iwmi.floodwater.domain.User;
import com.hashcode.iwmi.floodwater.services.UserService;

//
// IMPORTS
// NOTE: Import specific classes without using wildcards.
//

/**
 * <p>
 * User service implementation.
 * </p>
 *
 * @author Manuja
 *
 * @version $Id$
 */
@Service
public class UserServiceImpl implements UserService
{

    @Autowired
    private UserDao userDao;
    
    /**
     * {@inheritDoc}
     * 
     * @see UserService#authenticateUser(String, String)
     */
    @Override
    public User authenticateUser(String username, String password) throws NoSuchAlgorithmException
    {
        return userDao.authenticateUser(username, encrypt(password));
    }

    /**
     * {@inheritDoc}
     *
     * @see UserService#create(User)
     */
    @Override
    public void create(User user) throws NoSuchAlgorithmException
    {
        user.setPassword(encrypt(user.getPassword()));
        userDao.create(user);
    }

    /**
     * {@inheritDoc}
     *
     * @seeUserService#update(User)
     */
    @Override
    public void update(User user) throws NoSuchAlgorithmException
    {
        user.setPassword(encrypt(user.getPassword()));
        userDao.update(user);
    }
}
