/*
 * FILENAME
 *     AddWaterLevelController.java
 *
 * FILE LOCATION
 *     $Source$
 *
 * VERSION
 *     $Id$
 *         @version       $Revision$
 *         Check-Out Tag: $Name$
 *         Locked By:     $Lockers$
 *
 * FORMATTING NOTES
 *     * Lines should be limited to 78 characters.
 *     * Files should contain no tabs.
 *     * Indent code using four-character increments.
 *
 * COPYRIGHT
 *     Copyright (C) 2014 hashCode Solutions Pty. Ltd. All rights reserved.
 *     This software is the confidential and proprietary information of
 *     hashCode Solutions ("Confidential Information"). You shall not
 *     disclose such Confidential Information and shall use it only in
 *     accordance with the terms of the licence agreement you entered into
 *     with hashCode Solutions.
 */

package com.hashcode.iwmi.floodwater.controllers;

import java.time.LocalDate;
import java.util.List;

import javax.transaction.Transactional;

import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.hashcode.iwmi.floodwater.dao.ActualWaterLevelDao;
import com.hashcode.iwmi.floodwater.dao.PredictedWaterLevelDao;
import com.hashcode.iwmi.floodwater.dao.WaterLevelMeasuringPointDao;
import com.hashcode.iwmi.floodwater.domain.ActualWaterLevel;
import com.hashcode.iwmi.floodwater.domain.PredictedWaterLevel;
import com.hashcode.iwmi.floodwater.domain.WaterLevelMeasuringPoint;

//
// IMPORTS
// NOTE: Import specific classes without using wildcards.
//

/**
 * <p>
 * Controller to handle Add actual and predicted water levels.
 * </p>
 * 
 *
 * @author Amila Silva
 * @email amilasilva88@gmail.com
 **/
@Controller
public class WaterLevelDataController
{
    private static final Logger log = LoggerFactory.getLogger(WaterLevelDataController.class);

    @Autowired
    private WaterLevelMeasuringPointDao waterLevelMeasuringPointDao;

    @Autowired
    private ActualWaterLevelDao actualWaterLevelDao;

    @Autowired
    private PredictedWaterLevelDao predictedWaterLevelDao;

    @Transactional
    @RequestMapping(value = "/loadActualLevelForm", method = {
        RequestMethod.GET
    })
    public String loadActualLevelForm(@RequestParam long levelId, Model model)
    {
        log.info("Request came to load actual form data");

        ActualWaterLevel actualWaterLevel = actualWaterLevelDao.findById(levelId);
        model.addAttribute("actualWaterLevel", actualWaterLevel);
        
        List<WaterLevelMeasuringPoint> measuringPoints = waterLevelMeasuringPointDao.findAll();
        model.addAttribute("measuring_points", measuringPoints);

        return "AddActualWaterLevel";
    }

    @Transactional
    @RequestMapping(value = "/saveActualLevelForm", method = {
        RequestMethod.POST
    })
    @ResponseBody
    public String saveActualLevelForm(@RequestBody String waterData)
    {
        log.debug("Request came to save actual form data, Water Level Data : {} ", waterData);
        JSONObject response = new JSONObject();
        try
        {
            JSONObject waterJson = new JSONObject(waterData);

            ActualWaterLevel waterlevelData = new ActualWaterLevel();
            waterlevelData.setMeasuringPoint(waterLevelMeasuringPointDao.findById(waterJson.getLong("measurePointId")));
            waterlevelData.setDate(LocalDate.parse(waterJson.getString("recordDate")));
            waterlevelData.setWaterLevel(waterJson.getDouble("waterLevel"));
            waterlevelData.setComment(waterJson.getString("comment"));

            actualWaterLevelDao.create(waterlevelData);

            response.put("status", "success");
            log.info("Saved actual water level, [ Id : {} ] ", waterlevelData.getId());
        }
        catch (Exception e)
        {
        	response.put("status", "failed");
            log.error("Error occurred during the actual water level saving, ", e);
        }
        return response.toString();
    }
    
    @Transactional
    @RequestMapping(value = "/updateActualLevelForm", method = {
        RequestMethod.POST, RequestMethod.PUT
    })
    public String updateActualLevelForm(@RequestBody String waterData, Model model)
    {
        log.debug("Request came to update actual form data, Water Level Data : {} ", waterData);
        try
        {
            JSONObject waterJson = new JSONObject(waterData);

            ActualWaterLevel waterlevelData = actualWaterLevelDao.findById(waterJson.getLong("waterLevelId"));
            waterlevelData.setMeasuringPoint(waterLevelMeasuringPointDao.findById(waterJson.getLong("measurePointId")));
            waterlevelData.setDate(LocalDate.parse(waterJson.getString("recordDate")));
            waterlevelData.setWaterLevel(waterJson.getDouble("waterLevel"));
            waterlevelData.setComment(waterJson.getString("comment"));

            actualWaterLevelDao.update(waterlevelData);
            
            ActualWaterLevel actualWaterLevel = actualWaterLevelDao.findById(waterlevelData.getId());
            model.addAttribute("actualWaterLevel", actualWaterLevel);

            List<WaterLevelMeasuringPoint> measuringPoints = waterLevelMeasuringPointDao.findAll();
            model.addAttribute("measuring_points", measuringPoints);
            model.addAttribute("successMsg", "Actual water Level data updated");
            log.info("Updated actual water level, [ Id : {} ] ", waterlevelData.getId());
        }
        catch (Exception e)
        {
            log.error("Error occurred during the actual water level updating, ", e);
            model.addAttribute("errorMsg", "Failed to updated the Actual water level");
        }
        return "";
    }

    @Transactional
    @RequestMapping(value = "/loadForecastedLevelForm", method = {
        RequestMethod.GET
    })
    public String loadForecastedLevelForm(@RequestParam long levelId, Model model)
    {
        log.info("Request came to load forcasted form data");

        PredictedWaterLevel predictedWaterLevel = predictedWaterLevelDao.findById(levelId);
        model.addAttribute("predictedWaterLevel", predictedWaterLevel);
        
        List<WaterLevelMeasuringPoint> measuringPoints = waterLevelMeasuringPointDao.findAll();
        model.addAttribute("measuring_points", measuringPoints);

        return "AddPredictedWaterLevel";
    }

    @Transactional
    @RequestMapping(value = "/saveForecastedLevelForm", method = {
        RequestMethod.POST
    })
    @ResponseBody
    public String saveForecastedLevelForm(@RequestBody String waterData)
    {
        log.debug("Request came to save forecasted form data, Water Level Data : {} ", waterData);
        JSONObject response = new JSONObject();
        try
        {
            JSONObject waterJson = new JSONObject(waterData);

            PredictedWaterLevel waterlevelData = new PredictedWaterLevel();
            waterlevelData.setMeasuringPoint(waterLevelMeasuringPointDao.findById(waterJson.getLong("measurePointId")));
            waterlevelData.setDate(LocalDate.parse(waterJson.getString("recordDate")));
            waterlevelData.setWaterLevel(Double.valueOf(waterJson.getString("waterLevel")));
            waterlevelData.setComment(waterJson.getString("comment"));

            predictedWaterLevelDao.create(waterlevelData);

            response.put("status", "success");
            log.info("Saved forecasted water level, [ Id : {} ] ", waterlevelData.getId());
        }
        catch (Exception e)
        {
        	response.put("status", "failed");
            log.error("Error occurred during the forecasted water level saving, ", e);
        }
        return response.toString();
    }
    
    
    @Transactional
    @RequestMapping(value = "/updateForcastedLevelForm", method = {
        RequestMethod.POST, RequestMethod.PUT
    })
    public String updateForecastedLevelForm(@RequestBody String waterData, Model model)
    {
        log.debug("Request came to update forcated form data, Water Level Data : {} ", waterData);
        try
        {
            JSONObject waterJson = new JSONObject(waterData);

            PredictedWaterLevel waterlevelData = predictedWaterLevelDao.findById(waterJson.getLong("waterLevelId"));
            waterlevelData.setMeasuringPoint(waterLevelMeasuringPointDao.findById(waterJson.getLong("measurePointId")));
            waterlevelData.setDate(LocalDate.parse(waterJson.getString("recordDate")));
            waterlevelData.setWaterLevel(waterJson.getDouble("waterLevel"));
            waterlevelData.setComment(waterJson.getString("comment"));

            predictedWaterLevelDao.update(waterlevelData);
            
            PredictedWaterLevel predictedWaterLevel = predictedWaterLevelDao.findById(waterlevelData.getId());
            model.addAttribute("predictedWaterLevel", predictedWaterLevel);

            List<WaterLevelMeasuringPoint> measuringPoints = waterLevelMeasuringPointDao.findAll();
            model.addAttribute("measuring_points", measuringPoints);
            model.addAttribute("successMsg", "Forcasted water Level data updated");
            log.info("Saved actual water level, [ Id : {} ] ", waterlevelData.getId());
        }
        catch (Exception e)
        {
            log.error("Error occurred during the actual water level updating, ", e);
            model.addAttribute("errorMsg", "Failed to update the Forcasted water level");
        }
        return "";
    }

    @RequestMapping("/searchActualLevelsForm")
    public String searchActualLevels(Model model)
    {
        log.debug("Request came to load actual data form ");

        List<WaterLevelMeasuringPoint> measuringPoints = waterLevelMeasuringPointDao.findAll();
        model.addAttribute("measuring_points", measuringPoints);
        return "ActualWaterLevelData";
    }

    @Transactional
    @RequestMapping(value = "/searchActualLevels", method = {
        RequestMethod.POST
    })
    @ResponseBody
    public String searchActualLevels(@RequestBody String searchCriteria)
    {
        log.debug("Request came to search actual data");
        JSONObject searchCriteriaJson = new JSONObject(searchCriteria);

        return searchActualWaterLevelsRepository(searchCriteriaJson);
    }

    @Transactional
    @RequestMapping(value = "/deleteActualLevels", method = {
        RequestMethod.POST
    })
    @ResponseBody
    public String deleteActualLevels(@RequestBody String searchCriteria)
    {
        log.debug("Request came to delete actual data");
        JSONObject searchCriteriaJson = new JSONObject(searchCriteria);

        Long idToDelete = searchCriteriaJson.getLong("id");

        ActualWaterLevel actualWaterLevel = actualWaterLevelDao.findById(idToDelete);
        actualWaterLevel.getMeasuringPoint().removeActualReading(actualWaterLevel);
        actualWaterLevelDao.delete(actualWaterLevel);

        return searchActualWaterLevelsRepository(searchCriteriaJson);
    }

    @RequestMapping("/searchForecastedLevelsForm")
    public String searchPredictedLevelsForm(Model model)
    {
        log.debug("Request came to load forecasted data form");

        List<WaterLevelMeasuringPoint> measuringPoints = waterLevelMeasuringPointDao.findAll();
        model.addAttribute("measuring_points", measuringPoints);

        return "PredictedWaterLevelData";
    }

    @Transactional
    @RequestMapping(value = "/searchForecastedLevels", method = {
        RequestMethod.POST
    })
    @ResponseBody
    public String searchPredictedLevels(@RequestBody String searchCriteria)
    {
        log.debug("Request came to search forecasted data");
        JSONObject searchCriteriaJson = new JSONObject(searchCriteria);

        return searchPredictedLevelsRepository(searchCriteriaJson);
    }

    @Transactional
    @RequestMapping(value = "/deleteForecastedLevels", method = {
        RequestMethod.POST
    })
    @ResponseBody
    public String deletePredictedLevels(@RequestBody String searchCriteria)
    {
        log.debug("Request came to delete forecasted data");
        JSONObject searchCriteriaJson = new JSONObject(searchCriteria);

        Long idToDelete = searchCriteriaJson.getLong("id");

        PredictedWaterLevel predictedWaterLevel = predictedWaterLevelDao.findById(idToDelete);
        predictedWaterLevel.getMeasuringPoint().removePredictedReading(predictedWaterLevel);
        predictedWaterLevelDao.delete(predictedWaterLevel);

        return searchPredictedLevelsRepository(searchCriteriaJson);
    }

    @Transactional
    @RequestMapping(value = "/checkActualWaterLevel", method = {
        RequestMethod.POST
    })
    @ResponseBody
    public String checkActualWaterLevel(@RequestBody String waterLevelString)
    {
        log.info("Check actual water level request received {}", waterLevelString);
        JSONObject waterLevelJson = new JSONObject(waterLevelString);
        Long id = waterLevelJson.getLong("measurePointId");
        LocalDate date = LocalDate.parse(waterLevelJson.getString("recordDate"));

        List<ActualWaterLevel> actualWaterLevels = actualWaterLevelDao.getActualWaterLevels(id, date, date);
        JSONObject userData = new JSONObject();
        if (actualWaterLevels.isEmpty())
        {
            userData.put("status", "true");
        }
        else
        {
            userData.put("status", "false");
        }

        log.info("Response for checke mail {}", userData.toString());

        return userData.toString();
    }
    
    @Transactional
    @RequestMapping(value = "/checkActualWaterLevelWithId", method = {
        RequestMethod.POST
    })
    @ResponseBody
    public String checkActualWaterLevelWithId(@RequestBody String waterLevelString)
    {
        log.info("Check actual water level request received {}", waterLevelString);
        JSONObject waterLevelJson = new JSONObject(waterLevelString);
        Long measuringPointId = waterLevelJson.getLong("measurePointId");
        LocalDate date = LocalDate.parse(waterLevelJson.getString("recordDate"));
        Long waterLevelId = waterLevelJson.getLong("id");

        List<ActualWaterLevel> actualWaterLevels = actualWaterLevelDao.getActualWaterLevels(measuringPointId, date, date);
        JSONObject userData = new JSONObject();
        if (actualWaterLevels.isEmpty())
        {
            userData.put("status", "true");
        }
        else
        {
        	if(actualWaterLevels.get(0).getId().equals(waterLevelId))
        	{
        		userData.put("status", "true");
        	}
        	else
        	{
        		userData.put("status", "false");	
        	}
        }

        log.info("Response for checke mail {}", userData.toString());

        return userData.toString();
    }

    @Transactional
    @RequestMapping(value = "/checkPredictedWaterLevelWithId", method = {
        RequestMethod.POST
    })
    @ResponseBody
    public String checkPredictedWaterLevel(@RequestBody String waterLevelString)
    {
        log.info("Check predicted water level request received {}", waterLevelString);
        JSONObject waterLevelJson = new JSONObject(waterLevelString);
        Long id = waterLevelJson.getLong("measurePointId");
        LocalDate date = LocalDate.parse(waterLevelJson.getString("recordDate"));
        Long waterLevelId = waterLevelJson.getLong("id");

        List<PredictedWaterLevel> predictedWaterLevels = predictedWaterLevelDao.getPredictedWaterLevels(id, date, date);
        JSONObject userData = new JSONObject();
        if (predictedWaterLevels.isEmpty())
        {
            userData.put("status", "true");
        }
        else
        {
        	if(predictedWaterLevels.get(0).getId().equals(waterLevelId))
        	{
        		userData.put("status", "true");
        	}
        	else
        	{
        		userData.put("status", "false");	
        	}
        }


        log.info("Response for checke mail {}", userData.toString());

        return userData.toString();
    }
    
    @Transactional
    @RequestMapping(value = "/checkPredictedWaterLevel", method = {
        RequestMethod.POST
    })
    @ResponseBody
    public String checkPredictedWaterLevelWithId(@RequestBody String waterLevelString)
    {
        log.info("Check predicted water level request received {}", waterLevelString);
        JSONObject waterLevelJson = new JSONObject(waterLevelString);
        Long id = waterLevelJson.getLong("measurePointId");
        LocalDate date = LocalDate.parse(waterLevelJson.getString("recordDate"));

        List<PredictedWaterLevel> predictedWaterLevels = predictedWaterLevelDao.getPredictedWaterLevels(id, date, date);
        JSONObject userData = new JSONObject();
        if (predictedWaterLevels.isEmpty())
        {
            userData.put("status", "true");
        }
        else
        {
            userData.put("status", "false");
        }

        log.info("Response for checke mail {}", userData.toString());

        return userData.toString();
    }

    private String searchActualWaterLevelsRepository(JSONObject searchCriteriaJson)
    {
        Long measuringPoint = searchCriteriaJson.getLong("measurePointId");
        String fromDate = searchCriteriaJson.getString("fromDate");
        String toDate = searchCriteriaJson.getString("toDate");
        LocalDate fromLocalDate = LocalDate.parse(fromDate);
        LocalDate toLocalDate = LocalDate.parse(toDate);

        log.info("Search actual water levels, From Date {}, To Date {}, Measuring Point : {}", fromLocalDate, toLocalDate,  measuringPoint);

        List<ActualWaterLevel> actualWaterLevels =
            actualWaterLevelDao.getActualWaterLevels(measuringPoint, fromLocalDate, toLocalDate);

        JSONArray waterLevelArray = new JSONArray();
        actualWaterLevels.stream().forEach((level) -> {
            JSONObject waterLevelData = new JSONObject();
            waterLevelData.put("id", level.getId());
            waterLevelData.put("date", level.getDate().toString());
            waterLevelData.put("measuringPoint", level.getMeasuringPoint().getName());
            waterLevelData.put("level", level.getWaterLevel());
            waterLevelData.put("comment", level.getComment());
            waterLevelArray.put(waterLevelData);
        });
        log.info("Actual water level search results populated, Response : {}", waterLevelArray.length());

        return waterLevelArray.toString();
    }

    private String searchPredictedLevelsRepository(JSONObject searchCriteriaJson)
    {
    	Long measuringPoint = searchCriteriaJson.getLong("measurePointId");
        String fromDate = searchCriteriaJson.getString("fromDate");
        String toDate = searchCriteriaJson.getString("toDate");
        LocalDate fromLocalDate = LocalDate.parse(fromDate);
        LocalDate toLocalDate = LocalDate.parse(toDate);

        log.info("Search Forecasted water levels, From Date {}, To Date {}, Measuring Point : {}", fromLocalDate, toLocalDate,  measuringPoint);

        List<PredictedWaterLevel> predictedWaterLevels =
            predictedWaterLevelDao.getPredictedWaterLevels(measuringPoint, fromLocalDate, toLocalDate);

        JSONArray waterLevelArray = new JSONArray();
        predictedWaterLevels.stream().forEach((level) -> {
            JSONObject waterLevelData = new JSONObject();
            waterLevelData.put("id", level.getId());
            waterLevelData.put("date", level.getDate().toString());
            waterLevelData.put("measuringPoint", level.getMeasuringPoint().getName());
            waterLevelData.put("level", level.getWaterLevel());
            waterLevelData.put("comment", level.getComment());
            waterLevelArray.put(waterLevelData);
        });

        log.info("Forecasted water level search results populated, Response : {}", waterLevelArray.length());

        return waterLevelArray.toString();
    }
}
