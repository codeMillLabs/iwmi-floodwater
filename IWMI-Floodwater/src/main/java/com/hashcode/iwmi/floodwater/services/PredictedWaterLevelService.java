/*
 * FILENAME
 *     PredictedWaterLevelService.java
 *
 * FILE LOCATION
 *     $Source$
 *
 * VERSION
 *     $Id$
 *         @version       $Revision$
 *         Check-Out Tag: $Name$
 *         Locked By:     $Lockers$
 *
 * FORMATTING NOTES
 *     * Lines should be limited to 78 characters.
 *     * Files should contain no tabs.
 *     * Indent code using four-character increments.
 *
 * COPYRIGHT
 *     Copyright (C) 2013 hashCode Solutions Pty. Ltd. All rights reserved.
 *     This software is the confidential and proprietary information of
 *     hashCode Solutions ("Confidential Information"). You shall not
 *     disclose such Confidential Information and shall use it only in
 *     accordance with the terms of the licence agreement you entered into
 *     with hashCode Solutions.
 */

package com.hashcode.iwmi.floodwater.services;

import java.time.LocalDate;
import java.util.List;
import java.util.Map;

import com.hashcode.iwmi.floodwater.domain.PredictedWaterLevel;

//
// IMPORTS
// NOTE: Import specific classes without using wildcards.
//

/**
 * <p>
 * PredictedWaterLevelService interface.
 * </p>
 *
 * @author Manuja
 *
 * @version $Id$
 */
public interface PredictedWaterLevelService
{
    /**
     * <p>
     * Get predicted water levels for given name and date period.
     * </p>
     *
     * @param name measuring point name
     * @param date date from
     * @param period period of time
     * 
     * @return list of actual water data levels
     */
    List<PredictedWaterLevel> getPredictedWaterLevels(String name, LocalDate date, int period);
    
    
    /**
     * <p>
     * Get predicted water levels for given name and date period in a map
     * </p>
     *
     * @param name measuring point name
     * @param date date to
     * @param period period of time
     * 
     * @return Map of predicted water data levels by date
     */
    Map<LocalDate, Double> getPredictedWaterLevelsMap(String name, LocalDate date, int period, boolean isPast);
    
    /**
     * <p>
     * Get predicted water levels for given id and date period.
     * </p>
     *
     * @param id
     *            measuring point id
     * @param fromDate
     *            water level from date
     * @param toDate
     *            water level to date
     * 
     * @return list of actual water data levels
     */
    List<PredictedWaterLevel> getPredictedWaterLevels(Long id, LocalDate fromDate, LocalDate toDate);
}
