/*
 * Created on Nov 11, 2014
 * 
 * All sources, binaries and HTML pages (C) copyright 2014 by NextLabs, Inc.,
 * San Mateo CA, Ownership remains with NextLabs, Inc., All rights reserved
 * worldwide.
 *
 */
package com.hashcode.iwmi.floodwater.util;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.android.gcm.server.Message;
import com.google.android.gcm.server.MulticastResult;
import com.google.android.gcm.server.Sender;
import com.hashcode.iwmi.floodwater.model.NotificationMessage;

/**
 * <p>
 * Post messages to Google Cloud.
 * </p>
 *
 *
 * @author Amila Silva
 * @email amilasilva88@gmail.com
 *
 */
public class Post2GCM
{
    private static final Logger log = LoggerFactory.getLogger(Post2GCM.class);

    /**
     * <p>
     * Post notification data to Andorid apps
     * </p>
     *
     * @param apiKey
     *            apiKey from google
     * @param content
     *            message content
     */
    public static void post(String apiKey, NotificationMessage message)
    {

        try
        {
            Sender sender = new Sender(apiKey);
            Message notifyMsg =
                new Message.Builder().addData("message", message.getData().get("message"))
                    .addData("title", message.getData().get("title"))
                    .addData("message_type", message.getData().get("message_type")).build();

            MulticastResult results = sender.send(notifyMsg, message.getRegistration_ids(), 1);
            log.info("Message sent to GCM, Results :[ Total : {}, Sucesss: {}, Failures: {}]", results.getTotal(),
                results.getSuccess(), results.getFailure());

            //            URL url = new URL("https://android.googleapis.com/gcm/send");
            //            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            //            conn.setRequestMethod("POST");
            //            conn.setRequestProperty("Content-Type", "application/json");
            //            conn.setRequestProperty("Authorization", "key=" + apiKey);
            //
            //            conn.setDoOutput(true);
            //
            //            // 5. Add JSON data into POST request body
            //
            //            ObjectMapper mapper = new ObjectMapper();
            //
            //            DataOutputStream wr = new DataOutputStream(conn.getOutputStream());
            //            mapper.writeValue(wr, message);
            //
            //            wr.flush();
            //            wr.close();
            //
            //            int responseCode = conn.getResponseCode();
            //            log.info("Message post to GCM, Response code:{}", responseCode);
            //
            //            BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            //            String inputLine;
            //            StringBuffer response = new StringBuffer();
            //
            //            while ((inputLine = in.readLine()) != null)
            //            {
            //                response.append(inputLine);
            //            }
            //            in.close();

            // 7. Print result
            //            log.info("Message post to GCM, Response code:{}, Response :{}", responseCode, response.toString());

        }
        catch (Exception e)
        {
            log.error("Error occurred in posting data to GCM, ", e);
        }
    }

    public static void main(String[] args) throws IOException
    {

        String regId =
            "APA91bElik8MClJ-dlwTW80715NAatYv8P6KvtODpqh5Ez011HmWyuqghHmiSjeqB7RfamdsKy7a4w90Pg9u6b6JSc27Qz4sPThZd8l6KtDuIl2_4iOe3191koVTn1Fw69VPtCxEN-ZOTcIDlRwK_yhxru2EEcWIiQ";
        String regId2 =
            "APA91bGB7KSF7Z1n6S_vO3y2YJWGoUE5Bj2RzmCQBlQWNdLdXJYIlh0AOTNb0CdsvQqmM3OgzaXFN0hdfUPAXaXy46xrsojS863IdmGohGvCUzn1A-v9KlST3eJgxdCg0QY0bGVuPpoUyFwK4gwTtNQrXfQEGYWQNWwAuc4j148crumTcgstm-M";
        String regId3 =
            "APA91bHhMU4EQLJhoJxqAZix1iK0AEHRveJ4ZmI5ZYSM-CGfh6pFsA8IKMIf6DiVyQlNjp_5lfqoYvbV9y_hkyMGQXwiod9Uu8OvFL_jjF_wvTj0Z1FSKv3uQVIPzWdzVmLln1FT5TyYFj4VvjUs6PnxRgDPnpKeHA";
        String regId4 =
            "APA91bHhMU4EQLJhoJxqAZix1iK0AEHRveJ4ZmI5ZYSM-CGfh6pFsA8IKMIf6DiVyQlNjp_5lfqoYvbV9y_hkyMGQXwiod9Uu8OvFL_jjF_wvTj0Z1FSKv3uQVIPzWdzVmLln1FT5TyYFj4VvjUs6PnxRgDPnpKeHA";
        String regId5 =
            "APA91bHhMU4EQLJhoJxqAZix1iK0AEHRveJ4ZmI5ZYSM-CGfh6pFsA8IKMIf6DiVyQlNjp_5lfqoYvbV9y_hkyMGQXwiod9Uu8OvFL_jjF_wvTj0Z1FSKv3uQVIPzWdzVmLln1FT5TyYFj4VvjUs6PnxRgDPnpKeHA";

        List<String> regs = Arrays.asList(regId, regId2, regId3, regId4, regId5);

        Sender sender = new Sender("AIzaSyBwEGKxEegT5Ooqlviffswl_8ciUydNNiY");
        Message notifyMsg =
            new Message.Builder().addData("title", "Amila Silva @").addData("message", "oi oka nawathanawa..?")
                .addData("message_type", "ALERT").build();

        //        Result result = sender.sendNoRetry(notifyMsg, regId);
        //        log.info("Message sent to GCM, Results :[ Total : {}, Sucesss: {}]", result.getMessageId(), result.toString());

        MulticastResult result = sender.sendNoRetry(notifyMsg, regs);
        log.info("Message sent to GCM, Results :[ Sucesss : {}, Failures : {}, Total: {}]", result.getSuccess(),
            result.getFailure(), result.getTotal());
    

    }
}
