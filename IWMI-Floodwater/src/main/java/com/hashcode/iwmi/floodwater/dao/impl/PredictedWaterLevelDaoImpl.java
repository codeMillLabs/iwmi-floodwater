/*
 * FILENAME
 *     PredictedWaterLevelDaoImpl.java
 *
 * FILE LOCATION
 *     $Source$
 *
 * VERSION
 *     $Id$
 *         @version       $Revision$
 *         Check-Out Tag: $Name$
 *         Locked By:     $Lockers$
 *
 * FORMATTING NOTES
 *     * Lines should be limited to 78 characters.
 *     * Files should contain no tabs.
 *     * Indent code using four-character increments.
 *
 * COPYRIGHT
 *     Copyright (C) 2013 hashCode Solutions Pty. Ltd. All rights reserved.
 *     This software is the confidential and proprietary information of
 *     hashCode Solutions ("Confidential Information"). You shall not
 *     disclose such Confidential Information and shall use it only in
 *     accordance with the terms of the licence agreement you entered into
 *     with hashCode Solutions.
 */

package com.hashcode.iwmi.floodwater.dao.impl;

import java.time.LocalDate;
import java.util.List;

import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;
import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import com.hashcode.iwmi.floodwater.dao.PredictedWaterLevelDao;
import com.hashcode.iwmi.floodwater.domain.PredictedWaterLevel;

//
// IMPORTS
// NOTE: Import specific classes without using wildcards.
//

/**
 * <p>
 * Predicted Water Level Dao implementation.
 * </p>
 *
 * @author Manuja
 *
 * @version $Id$
 */
@Transactional
@Repository
public class PredictedWaterLevelDaoImpl extends GenericDaoImpl<PredictedWaterLevel, Long> implements
    PredictedWaterLevelDao
{
    private static final Logger log = LoggerFactory.getLogger(PredictedWaterLevelDaoImpl.class);

    /**
     * {@inheritDoc}
     *
     * @see PredictedWaterLevelDao#getPredictedWaterLevels(String, LocalDate, LocalDate)
     */
    @Override
    public List<PredictedWaterLevel> getPredictedWaterLevels(String name, LocalDate fromDate, LocalDate toDate)
    {
        log.info("Get predicted water levels for Name : {}, From Date : {}, To Date : {}", name, fromDate, toDate);

        String queryText =
            "SELECT wl FROM " + PredictedWaterLevel.class.getName() + " wl INNER JOIN wl.measuringPoint mp "
                + " WHERE mp.name like :name AND wl.date >= :fromDate AND wl.date < :toDate ORDER BY wl.date ASC ";

        TypedQuery<PredictedWaterLevel> query = entityManager.createQuery(queryText, PredictedWaterLevel.class);
        query.setParameter("name", name);
        query.setParameter("fromDate", fromDate);
        query.setParameter("toDate", toDate);

        List<PredictedWaterLevel> waterData = query.getResultList();
        log.debug("Get Predicted water levels, [ Results :{}]", waterData.size());
        return waterData;
    }

    /**
     * {@inheritDoc}
     *
     * @see PredictedWaterLevelDao#getPredictedWaterLevels(Long, LocalDate, LocalDate)
     */
    @Override
    public List<PredictedWaterLevel> getPredictedWaterLevels(Long id, LocalDate fromDate, LocalDate toDate)
    {
        log.info("Get predicted water levels for Id : {}, From Date : {}, To Date : {}", id, fromDate, toDate);

        String queryText =
            "SELECT wl FROM " + PredictedWaterLevel.class.getName() + " wl INNER JOIN wl.measuringPoint mp "
                + " WHERE wl.date >= :fromDate AND wl.date <= :toDate ";

        if (id > 0)
            queryText += " AND mp.id= :id ";
        queryText += " ORDER BY wl.date ASC";

        TypedQuery<PredictedWaterLevel> query = entityManager.createQuery(queryText, PredictedWaterLevel.class);
        if (id > 0)
            query.setParameter("id", id);
        query.setParameter("fromDate", fromDate);
        query.setParameter("toDate", toDate);

        List<PredictedWaterLevel> waterData = query.getResultList();
        log.debug("Get Predicted water levels, [ Results :{}]", waterData.size());
        return waterData;
    }

    /**
     * {@inheritDoc}
     *
     * @see PredictedWaterLevelDao#getPredictedWaterLevel(java.time.LocalDate)
     */
    @Override
    public PredictedWaterLevel getPredictedWaterLevel(LocalDate date)
    {
        try
        {
            log.debug("Get predicted water level for : {}", date);

            String queryText = "SELECT wl FROM " + PredictedWaterLevel.class.getName() + " wl WHERE wl.date = :date";

            TypedQuery<PredictedWaterLevel> query = entityManager.createQuery(queryText, PredictedWaterLevel.class);
            query.setParameter("date", date);
            query.setMaxResults(1);
            return query.getSingleResult();
        }
        catch (NoResultException e)
        {
            return null;
        }
    }

    /**
     * {@inheritDoc}
     *
     * @see com.hashcode.iwmi.floodwater.dao.PredictedWaterLevelDao#findPointsToAlert(java.time.LocalDate)
     */
    @Override
    public List<Object[]> findPointsToAlert(LocalDate date)
    {
        log.info("Find Points To Alert,  Date : {}", date);

        String queryText =
            "SELECT mp.name, wl.waterLevel FROM " + PredictedWaterLevel.class.getName() + " wl "
                + " INNER JOIN wl.measuringPoint mp "
                + " WHERE wl.date >= :fromDate AND wl.date <= :toDate "
                + " AND mp.riskLevelThreshold <= wl.waterLevel "
                + " ORDER BY mp.name ASC ";

        TypedQuery<Object[]> query = entityManager.createQuery(queryText, Object[].class);
 
        query.setParameter("fromDate", date);
        query.setParameter("toDate", date.plusDays(1));

        List<Object[]> waterData = query.getResultList();
        log.debug("No of Points To Alert, [ Results :{}]", waterData.size());
        return waterData;
    }

}
