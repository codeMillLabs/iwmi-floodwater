package com.hashcode.iwmi.floodwater.services;

public interface IPersonService
{
    public String getPersonName();
}
