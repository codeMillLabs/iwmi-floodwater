/*
 * FILENAME
 *     UserDataController.java
 *
 * FILE LOCATION
 *     $Source$
 *
 * VERSION
 *     $Id$
 *         @version       $Revision$
 *         Check-Out Tag: $Name$
 *         Locked By:     $Lockers$
 *
 * FORMATTING NOTES
 *     * Lines should be limited to 78 characters.
 *     * Files should contain no tabs.
 *     * Indent code using four-character increments.
 *
 * COPYRIGHT
 *     Copyright (C) 2013 hashCode Solutions Pty. Ltd. All rights reserved.
 *     This software is the confidential and proprietary information of
 *     hashCode Solutions ("Confidential Information"). You shall not
 *     disclose such Confidential Information and shall use it only in
 *     accordance with the terms of the licence agreement you entered into
 *     with hashCode Solutions.
 */

package com.hashcode.iwmi.floodwater.controllers;

import java.util.List;

import javax.transaction.Transactional;

import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.hashcode.iwmi.floodwater.dao.UserDao;
import com.hashcode.iwmi.floodwater.domain.User;
import com.hashcode.iwmi.floodwater.services.UserService;

//
// IMPORTS
// NOTE: Import specific classes without using wildcards.
//

/**
 * <p>
 * User data controller.
 * </p>
 *
 * @author Manuja
 *
 * @version $Id$
 */
@Controller
public class UserDataController
{
    private static final Logger log = LoggerFactory.getLogger(UserDataController.class);

    @Autowired
    private UserService userService;

    @Autowired
    private UserDao userDao;

    @RequestMapping("/addUser")
    public String addUser(Model model)
    {
        log.debug("Request came to add user");
        return "AddUser";
    }

    @Transactional
    @RequestMapping(value = "/saveUserForm", method = {
        RequestMethod.POST, RequestMethod.PUT
    })
    public String saveActualLevelForm(@RequestBody String userData, Model model)
    {
        log.debug("Request came to save user : {} ", userData);
        try
        {
            JSONObject userJason = new JSONObject(userData);

            User user = new User();
            user.setEmail(userJason.getString("email"));
            user.setFirstName(userJason.getString("firstName"));
            user.setLastName(userJason.getString("lastName"));
            user.setPassword(userJason.getString("password"));
            userService.create(user);

            model.addAttribute("message", "Successfully Saved");
            log.info("Saved user, [ Id : {} ] ", user.getId());
        }
        catch (Exception e)
        {
            log.error("Error occurred during user saving, ", e);
            model.addAttribute("message", "Error occurred during the save");
        }
        return "";
    }

    @Transactional
    @RequestMapping(value = "/checkEmail", method = {
        RequestMethod.POST
    })
    @ResponseBody
    public String checkEmail(@RequestBody String emailString)
    {
        log.info("Check email request received {}", emailString);
        JSONObject emailJson = new JSONObject(emailString);
        String email = emailJson.getString("email");

        User user = userDao.getUser(email);
        JSONObject userData = new JSONObject();
        if (user == null)
        {
            userData.put("status", "true");
        }
        else
        {
            userData.put("status", "false");
        }
        
        log.info("Response for checke mail {}", userData.toString());

        return userData.toString();
    }

    @RequestMapping("/searchUsersForm")
    public String userForm(Model model)
    {
        log.debug("Request came to add user");
        return "UserData";
    }

    @Transactional
    @RequestMapping(value = "/searchUsers", method = {
        RequestMethod.POST
    })
    @ResponseBody
    public String searchUsers(@RequestBody String searchCriteria)
    {
        log.info("Search users");
        return searchUsersRepository();
    }

    @Transactional
    @RequestMapping(value = "/deleteUsers", method = {
        RequestMethod.POST
    })
    @ResponseBody
    public String deleteUsers(@RequestBody String searchCriteria)
    {
        log.info("Delete users");
        JSONObject searchCriteriaJson = new JSONObject(searchCriteria);
        Long idToDelete = searchCriteriaJson.getLong("id");

        User user = userDao.findById(idToDelete);
        userDao.delete(user);
        userDao.findAll();

        return searchUsersRepository();
    }

    private String searchUsersRepository()
    {
        List<User> users = userDao.findAll();
        JSONArray userArray = new JSONArray();
        users.stream().forEach((level) -> {
            JSONObject userData = new JSONObject();
            userData.put("id", level.getId());
            userData.put("email", level.getEmail());
            userData.put("firstName", level.getFirstName());
            userData.put("lastName", level.getLastName());
            userArray.put(userData);
        });
        log.info("User search, Response : {}", userArray.length());
        return userArray.toString();
    }
}
