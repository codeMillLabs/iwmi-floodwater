/*
 * FILENAME
 *     WaterLevelMeasuringPointController.java
 *
 * FILE LOCATION
 *     $Source$
 *
 * VERSION
 *     $Id$
 *         @version       $Revision$
 *         Check-Out Tag: $Name$
 *         Locked By:     $Lockers$
 *
 * FORMATTING NOTES
 *     * Lines should be limited to 78 characters.
 *     * Files should contain no tabs.
 *     * Indent code using four-character increments.
 *
 * COPYRIGHT
 *     Copyright (C) 2014 hashCode Solutions Pty. Ltd. All rights reserved.
 *     This software is the confidential and proprietary information of
 *     hashCode Solutions ("Confidential Information"). You shall not
 *     disclose such Confidential Information and shall use it only in
 *     accordance with the terms of the licence agreement you entered into
 *     with hashCode Solutions.
 */

package com.hashcode.iwmi.floodwater.controllers;

import java.util.List;

import javax.transaction.Transactional;

import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.hashcode.iwmi.floodwater.dao.WaterLevelMeasuringPointDao;
import com.hashcode.iwmi.floodwater.domain.GeoCoordinate;
import com.hashcode.iwmi.floodwater.domain.WaterLevelMeasuringPoint;

//
// IMPORTS
// NOTE: Import specific classes without using wildcards.
//

/**
 * <p>
 * Water Level Measuring Point Controller to handle measuring points
 * functionalities
 * </p>
 *
 *
 * @author Amila Silva
 * @email amilasilva88@gmail.com
 **/
@Controller
public class WaterLevelMeasuringPointController {

	private static final Logger log = LoggerFactory
			.getLogger(WaterLevelMeasuringPointController.class);

	@Autowired
	private WaterLevelMeasuringPointDao waterLevelMeasuringPointDao;

	@RequestMapping(value = "/loadMeasuringPointForm", method = {
			RequestMethod.GET, RequestMethod.POST })
	public String loadMeasuringPointData(Model model) {
		log.debug("Request came to load measuring point form,  {} ");

		return "AddWaterMeasuringPoint";
	}
	
	@RequestMapping(value = "/loadMeasuringPointEditForm", method = {
			RequestMethod.GET})
	public String loadMeasuringPointEditData(@RequestParam long pointId, Model model) {
		log.debug("Request came to load measuring point edit form,  {} ");
		
		WaterLevelMeasuringPoint point = waterLevelMeasuringPointDao.findById(pointId);
		model.addAttribute("point", point);

		return "EditWaterMeasuringPoint";
	}

	@Transactional
	@RequestMapping(value = "/saveMeasuringPoint", method = {
			RequestMethod.POST, RequestMethod.PUT })
	public String saveActualLevelForm(@RequestBody String measuringPointData,
			Model model) {
		log.debug("Request came to save measuring point form data,  {} ",
				measuringPointData);
		try {
			JSONObject reqJson = new JSONObject(measuringPointData);

			WaterLevelMeasuringPoint point = new WaterLevelMeasuringPoint();
			point.setName(reqJson.getString("measuringName"));
			point.setGeoCoordinate(new GeoCoordinate(reqJson
					.getDouble("latitude"), reqJson.getDouble("longitute")));
			point.setModerateLevelThreshold(reqJson
					.getDouble("moderateLevelThreshold"));
			point.setRiskLevelThreshold(reqJson.getDouble("riskLevelThreshold"));
			waterLevelMeasuringPointDao.create(point);

			List<WaterLevelMeasuringPoint> measuringPoints = waterLevelMeasuringPointDao
					.findAll();
			model.addAttribute("measuring_points", measuringPoints);
			model.addAttribute("successMsg", "Actual water Level data saved");
			log.info("Saved water measuring point, [ Id : {} ] ", point.getId());
		} catch (Exception e) {
			log.error("Error occurred during the actual water level saving, ",
					e);
			model.addAttribute("errorMsg",
					"Failed to saved the Actual water level");
		}
		return "AddWaterMeasuringPoint";
	}
	
	@Transactional
	@RequestMapping(value = "/updateMeasuringPoint", method = {
			RequestMethod.POST, RequestMethod.PUT })
	public String updateMeasuringPoint(@RequestBody String measuringPointData,
			Model model) {
		log.debug("Request came to save measuring point form data,  {} ",
				measuringPointData);
		try {
			JSONObject reqJson = new JSONObject(measuringPointData);

			WaterLevelMeasuringPoint point = waterLevelMeasuringPointDao.findById(reqJson.getLong("pointId"));
			point.setName(reqJson.getString("measuringName"));
			point.setGeoCoordinate(new GeoCoordinate(reqJson
					.getDouble("latitude"), reqJson.getDouble("longitute")));
			point.setModerateLevelThreshold(reqJson
					.getDouble("moderateLevelThreshold"));
			point.setRiskLevelThreshold(reqJson.getDouble("riskLevelThreshold"));
			waterLevelMeasuringPointDao.update(point);

			List<WaterLevelMeasuringPoint> measuringPoints = waterLevelMeasuringPointDao
					.findAll();
			model.addAttribute("measuring_points", measuringPoints);
			model.addAttribute("successMsg", "Actual water Level data updated");
			log.info("Updated water measuring point, [ Id : {} ] ", point.getId());
		} catch (Exception e) {
			log.error("Error occurred during the actual water level updaing, ",
					e);
			model.addAttribute("errorMsg",
					"Failed to saved the Actual water level");
		}
		return "";
	}

	@RequestMapping(value = "/loadMeasuringPoints", method = {
			RequestMethod.POST, RequestMethod.GET })
	@ResponseBody
	public String loadMeasuringPoints(Model model) {
		log.debug("Request came to load measuring point");

		List<WaterLevelMeasuringPoint> points = waterLevelMeasuringPointDao
				.findAll();

		JSONArray measuringPoints = new JSONArray();

		points.stream().forEach(
				(p) -> {
					JSONObject pointData = new JSONObject();
					pointData.put("id", p.getId());
					pointData.put("measuringPoint", p.getName());
					pointData.put("coordinate", "["
							+ p.getGeoCoordinate().getLat() + ", "
							+ p.getGeoCoordinate().getLng() + "]");
					pointData.put("moderateLevelThreshold",
							p.getModerateLevelThreshold());
					pointData.put("riskLevelThreshold",
							p.getRiskLevelThreshold());

					measuringPoints.put(pointData);
				});

		log.info("Measuring points loaded, Response : {}",
				measuringPoints.length());

		return measuringPoints.toString();
	}

	@Transactional
	@RequestMapping(value = "/deletePoint", method = { RequestMethod.POST })
	@ResponseBody
	public String deleteActualLevels(@RequestBody String searchCriteria) {
		log.debug("Request came to delete point");
		JSONObject searchCriteriaJson = new JSONObject(searchCriteria);

		Long idToDelete = searchCriteriaJson.getLong("id");

		WaterLevelMeasuringPoint point = waterLevelMeasuringPointDao
				.findById(idToDelete);
		waterLevelMeasuringPointDao.delete(point);

		List<WaterLevelMeasuringPoint> points = waterLevelMeasuringPointDao
				.findAll();

		JSONArray measuringPoints = new JSONArray();

		points.stream().forEach(
				(p) -> {
					JSONObject pointData = new JSONObject();
					pointData.put("id", p.getId());
					pointData.put("measuringPoint", p.getName());
					pointData.put("coordinate", "["
							+ p.getGeoCoordinate().getLat() + ", "
							+ p.getGeoCoordinate().getLng() + "]");
					pointData.put("moderateLevelThreshold",
							p.getModerateLevelThreshold());
					pointData.put("riskLevelThreshold",
							p.getRiskLevelThreshold());

					measuringPoints.put(pointData);
				});

		log.info("Measuring points loaded, Response : {}",
				measuringPoints.length());

		return measuringPoints.toString();
	}
}
