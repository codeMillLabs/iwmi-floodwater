/*
 * FILENAME
 *     WaterLevelAlertServiceImpl.java
 *
 * FILE LOCATION
 *     $Source$
 *
 * VERSION
 *     $Id$
 *         @version       $Revision$
 *         Check-Out Tag: $Name$
 *         Locked By:     $Lockers$
 *
 * FORMATTING NOTES
 *     * Lines should be limited to 78 characters.
 *     * Files should contain no tabs.
 *     * Indent code using four-character increments.
 *
 * COPYRIGHT
 *     Copyright (C) 2014 hashCode Solutions Pty. Ltd. All rights reserved.
 *     This software is the confidential and proprietary information of
 *     hashCode Solutions ("Confidential Information"). You shall not
 *     disclose such Confidential Information and shall use it only in
 *     accordance with the terms of the licence agreement you entered into
 *     with hashCode Solutions.
 */

package com.hashcode.iwmi.floodwater.services.impl;

import static com.hashcode.iwmi.floodwater.model.NotificationMessage.ALERT_TYPE;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import com.hashcode.iwmi.floodwater.dao.GCMRegIdDao;
import com.hashcode.iwmi.floodwater.dao.PredictedWaterLevelDao;
import com.hashcode.iwmi.floodwater.domain.GCMRegId;
import com.hashcode.iwmi.floodwater.model.NotificationMessage;
import com.hashcode.iwmi.floodwater.services.WaterLevelAlertService;
import com.hashcode.iwmi.floodwater.util.Post2GCM;

//
// IMPORTS
// NOTE: Import specific classes without using wildcards.
//

/**
 * <p>
 * Implementation of the WaterLevelAlertService.
 * </p>
 *
 * @author Amila Silva
 *
 * @version $Id$
 **/
@Service
public class WaterLevelAlertServiceImpl implements WaterLevelAlertService
{

    private static final Logger log = LoggerFactory.getLogger(WaterLevelAlertServiceImpl.class);

    @Value("${flood.alert.title}")
    private String alertTitle;

    @Value("${flood.alert.message}")
    private String alertMessage;

    @Value("${flood.app.apikey}")
    private String apiKey;

    @Autowired
    private PredictedWaterLevelDao predictedWaterLevelDao;

    @Autowired
    private GCMRegIdDao gcmRegIdDao;

    /**
     * {@inheritDoc}
     *
     * @see com.hashcode.iwmi.floodwater.services.WaterLevelAlertService#generateAlert()
     */
//    @Scheduled(cron = "0/5 * * * * ?")
    @Scheduled(cron = "0 0 7 1/1 * ?")
    @Override
    public void generateAlert()
    {
        try
        {
            log.info("Check High risk of water level to generate alerts");
            LocalDate tomorrow = LocalDate.now(ZoneId.systemDefault()).plusDays(1);
            List<Object[]> measuringPoints = predictedWaterLevelDao.findPointsToAlert(tomorrow);

            StringBuilder sbuff = new StringBuilder("");

            if (measuringPoints.isEmpty())
            {
                log.info("No High risk of water level to generate alert through all data collection points");
                return;
            }
            
            measuringPoints.stream().forEach(p -> {
                sbuff.append(" " + p[0] + " : " + p[1] + ", ");
            });
            alertMessage = alertMessage.replace("{LOCATIONS}", sbuff.toString());

            NotificationMessage message = new NotificationMessage();
            message.createData(ALERT_TYPE, alertTitle, alertMessage);

            List<GCMRegId> regIds = gcmRegIdDao.findAll();
            regIds.stream().forEach(i -> {
                message.addRegId(i.getRegId());
            });

            log.info("Alert will post to GCM in a while, Message :{}", message);
            new Thread(() -> Post2GCM.post(apiKey, message)).start();

        }
        catch (Exception e)
        {
            log.error("Error occurred in Generating Alerts,", e);
        }
    }

}
