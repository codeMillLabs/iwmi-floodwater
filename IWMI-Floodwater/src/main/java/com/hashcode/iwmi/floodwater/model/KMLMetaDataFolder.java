/*
 * FILENAME
 *     KMLMetaDataFolderr.java
 *
 * FILE LOCATION
 *     $Source$
 *
 * VERSION
 *     $Id$
 *         @version       $Revision$
 *         Check-Out Tag: $Name$
 *         Locked By:     $Lockers$
 *
 * FORMATTING NOTES
 *     * Lines should be limited to 78 characters.
 *     * Files should contain no tabs.
 *     * Indent code using four-character increments.
 *
 * COPYRIGHT
 *     Copyright (C) 2013 hashCode Solutions Pty. Ltd. All rights reserved.
 *     This software is the confidential and proprietary information of
 *     hashCode Solutions ("Confidential Information"). You shall not
 *     disclose such Confidential Information and shall use it only in
 *     accordance with the terms of the licence agreement you entered into
 *     with hashCode Solutions.
 */

package com.hashcode.iwmi.floodwater.model;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

//
// IMPORTS
// NOTE: Import specific classes without using wildcards.
//

/**
 * <p>
 * KMLMetaDataFolder to hold the KML meta data related to single map information set
 * </p>
 * 
 *
 * @author Amila Silva
 * @email amilasilva88@gmail.com
 * 
 **/
public class KMLMetaDataFolder implements Serializable
{

    private static final long serialVersionUID = 4372600435027399439L;

    private String id;
    private LocalDateTime lastModifiedDate;
    private List<KMLFileMetaData> fileMetaData = new ArrayList<>();

    /**
     * <p>
     * Getter for id.
     * </p>
     * 
     * @return the id
     */
    public String getId()
    {
        return id;
    }

    /**
     * <p>
     * Setting value for id.
     * </p>
     * 
     * @param id
     *            the id to set
     */
    public void setId(String id)
    {
        this.id = id;
    }

    /**
     * <p>
     * Getter for lastModifiedDate.
     * </p>
     * 
     * @return the lastModifiedDate
     */
    public LocalDateTime getLastModifiedDate()
    {
        return lastModifiedDate;
    }

    /**
     * <p>
     * Setting value for lastModifiedDate.
     * </p>
     * 
     * @param lastModifiedDate
     *            the lastModifiedDate to set
     */
    public void setLastModifiedDate(LocalDateTime lastModifiedDate)
    {
        this.lastModifiedDate = lastModifiedDate;
    }

    /**
     * <p>
     * Getter for fileMetaData.
     * </p>
     * 
     * @return the fileMetaData
     */
    public List<KMLFileMetaData> getFileMetaData()
    {
        return fileMetaData;
    }

    /**
     * <p>
     * Setting value for fileMetaData.
     * </p>
     * 
     * @param fileMetaData
     *            the fileMetaData to set
     */
    public void setFileMetaData(List<KMLFileMetaData> fileMetaData)
    {
        this.fileMetaData = fileMetaData;
    }

    /**
     * {@inheritDoc}
     *
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString()
    {
        return String.format("KMLMetaDataFolderr [id=%s, lastModifiedDate=%s, fileMetaData=%s]", id, lastModifiedDate,
            fileMetaData.size());
    }

}
