/*
 * FILENAME
 *     ActualWaterLevelDaoImpl.java
 *
 * FILE LOCATION
 *     $Source$
 *
 * VERSION
 *     $Id$
 *         @version       $Revision$
 *         Check-Out Tag: $Name$
 *         Locked By:     $Lockers$
 *
 * FORMATTING NOTES
 *     * Lines should be limited to 78 characters.
 *     * Files should contain no tabs.
 *     * Indent code using four-character increments.
 *
 * COPYRIGHT
 *     Copyright (C) 2013 hashCode Solutions Pty. Ltd. All rights reserved.
 *     This software is the confidential and proprietary information of
 *     hashCode Solutions ("Confidential Information"). You shall not
 *     disclose such Confidential Information and shall use it only in
 *     accordance with the terms of the licence agreement you entered into
 *     with hashCode Solutions.
 */

package com.hashcode.iwmi.floodwater.dao.impl;

import java.time.LocalDate;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.NoResultException;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import com.hashcode.iwmi.floodwater.dao.ActualWaterLevelDao;
import com.hashcode.iwmi.floodwater.domain.ActualWaterLevel;
import com.hashcode.iwmi.floodwater.domain.WaterLevelMeasuringPoint;
import com.hashcode.iwmi.floodwater.model.AnnualWaterLevel;

//
// IMPORTS
// NOTE: Import specific classes without using wildcards.
//

/**
 * <p>
 * Actual Water Level Dao implementation.
 * </p>
 *
 * @author Manuja
 *
 * @version $Id$
 */
@Transactional
@Repository
public class ActualWaterLevelDaoImpl extends GenericDaoImpl<ActualWaterLevel, Long> implements ActualWaterLevelDao
{
    private static final Logger log = LoggerFactory.getLogger(ActualWaterLevelDaoImpl.class);

    /**
     * {@inheritDoc}
     *
     * @see ActualWaterLevelDao#getActualWaterLevels(LocalDate, LocalDate)
     */
    @Override
    public List<ActualWaterLevel> getActualWaterLevels(String name, LocalDate fromDate, LocalDate toDate)
    {
        log.info("Get actual water levels for Name : {}, From Date : {}, To Date : {}", name, fromDate, toDate);

        String queryText =
            "SELECT wl FROM " + ActualWaterLevel.class.getName() + " wl INNER JOIN wl.measuringPoint mp "
                + " WHERE mp.name like :name AND wl.date >= :fromDate AND wl.date < :toDate ORDER BY wl.date ASC ";

        TypedQuery<ActualWaterLevel> query = entityManager.createQuery(queryText, ActualWaterLevel.class);
        query.setParameter("name", name);
        query.setParameter("fromDate", fromDate);
        query.setParameter("toDate", toDate);

        List<ActualWaterLevel> waterData = query.getResultList();
        log.debug("Get Actual water levels, [ Results :{}]", waterData.size());
        return waterData;
    }

    /**
     * {@inheritDoc}
     *
     * @see ActualWaterLevelDao#getActualWaterLevels(LocalDate, LocalDate)
     */
    @Override
    public List<ActualWaterLevel> getActualWaterLevels(Long id, LocalDate fromDate, LocalDate toDate)
    {
        log.info("Get actual water levels for id : {}, From Date : {}, To Date : {}", id, fromDate, toDate);

        String queryText =
            "SELECT wl FROM " + ActualWaterLevel.class.getName() + " wl INNER JOIN wl.measuringPoint mp "
                + " WHERE wl.date >= :fromDate AND wl.date <= :toDate ";

        if (id > 0)
            queryText += " AND mp.id= :id ";

        queryText += "ORDER BY wl.date ASC ";

        TypedQuery<ActualWaterLevel> query = entityManager.createQuery(queryText, ActualWaterLevel.class);

        if (id > 0)
            query.setParameter("id", id);

        query.setParameter("fromDate", fromDate);
        query.setParameter("toDate", toDate);

        List<ActualWaterLevel> waterData = query.getResultList();
        log.debug("Get Actual water levels, [ Results :{}]", waterData.size());
        return waterData;
    }

    /**
     * {@inheritDoc}
     *
     * @see ActualWaterLevelDao#getActualWaterLevel(java.time.LocalDate)
     */
    @Override
    public ActualWaterLevel getActualWaterLevel(LocalDate date)
    {
        try
        {
            log.info("Get predicted water level for : {}", date);

            String queryText = "SELECT wl FROM " + ActualWaterLevel.class.getName() + " wl WHERE wl.date = :date";

            TypedQuery<ActualWaterLevel> query = entityManager.createQuery(queryText, ActualWaterLevel.class);
            query.setParameter("date", date);
            query.setMaxResults(1);
            return query.getSingleResult();
        }
        catch (NoResultException e)
        {
            return null;
        }
    }

    /**
     * {@inheritDoc}
     *
     * @see ActualWaterLevelDao#getAnnualAvgWaterLevelToDate(java.time.LocalDate)
     */
    @Override
    public Map<Integer, AnnualWaterLevel> getAnnualAvgWaterLevelToDate(LocalDate startDate,
        WaterLevelMeasuringPoint point)
    {
        Map<Integer, AnnualWaterLevel> dataMap = new HashMap<Integer, AnnualWaterLevel>();
        try
        {
            log.info("Get Annual average water level from : {}", startDate);

            String queryText =
                "SELECT wl.measuringPoint.name, YEAR(wl.date), AVG(wl.waterLevel) FROM "
                    + ActualWaterLevel.class.getName()
                    + " wl WHERE wl.date >= :date AND wl.measuringPoint.id = :pointId GROUP BY YEAR(wl.date) ORDER BY YEAR(wl.date)";

            Query query = entityManager.createQuery(queryText);
            query.setParameter("date", startDate);
            query.setParameter("pointId", point.getId());

            @SuppressWarnings("rawtypes")
            List results = query.getResultList();

            for (Object result : results)
            {
                Object[] values = (Object[]) result;
                String name = (String) values[0];
                int year = (int) values[1];
                double value = (double) values[2];
                AnnualWaterLevel waterLevel = new AnnualWaterLevel(name, year, value);

                dataMap.put(year, waterLevel);
            }
            log.info("Get Annual average water level, Results : {}", results.size());
            return dataMap;
        }
        catch (Exception e)
        {
            log.error("Error occurred in get actual water levels,", e);
            return dataMap;
        }
    }

}
