/*
 * FILENAME
 *     GeoCoordinate.java
 *
 * FILE LOCATION
 *     $Source$
 *
 * VERSION
 *     $Id$
 *         @version       $Revision$
 *         Check-Out Tag: $Name$
 *         Locked By:     $Lockers$
 *
 * FORMATTING NOTES
 *     * Lines should be limited to 78 characters.
 *     * Files should contain no tabs.
 *     * Indent code using four-character increments.
 *
 * COPYRIGHT
 *     Copyright (C) 2014 hashCode Solutions Pty. Ltd. All rights reserved.
 *     This software is the confidential and proprietary information of
 *     hashCode Solutions ("Confidential Information"). You shall not
 *     disclose such Confidential Information and shall use it only in
 *     accordance with the terms of the licence agreement you entered into
 *     with hashCode Solutions.
 */

package com.hashcode.iwmi.floodwater.domain;

import javax.persistence.Column;
import javax.persistence.Embeddable;

import org.codehaus.jackson.annotate.JsonProperty;

//
// IMPORTS
// NOTE: Import specific classes without using wildcards.
//

/**
 * <p>
 * GEO Coordinates model object.
 * </p>
 *
 * @author Amila Silva
 * @email amilasilva88@gmail.com
 **/
@Embeddable
public class GeoCoordinate
{
    @JsonProperty("lat")
    @Column(name = "latitude")
    private double lat;

    @JsonProperty("lng")
    @Column(name = "longitude")
    private double lng;

    /**
     * <p>
     * Constructor for GEO coordinates.
     * </p>
     *
     * @param latitude
     *            latitude
     * @param longitude
     *            longitude
     *
     */
    public GeoCoordinate(double latitude, double longitude)
    {
        super();
        this.lat = latitude;
        this.lng = longitude;
    }

    /**
     * <p>
     * Constructor for GEO coordinates.
     * </p>
     *
     * @param latitude
     *            latitude
     * @param longitude
     *            longitude
     *
     */
    public GeoCoordinate()
    {
        super();
    }

    /**
     * <p>
     * Getter for lat.
     * </p>
     * 
     * @return the lat
     */
    public double getLat()
    {
        return lat;
    }

    /**
     * <p>
     * Setting value for lat.
     * </p>
     * 
     * @param lat
     *            the lat to set
     */
    public void setLat(double lat)
    {
        this.lat = lat;
    }

    /**
     * <p>
     * Getter for lng.
     * </p>
     * 
     * @return the lng
     */
    public double getLng()
    {
        return lng;
    }

    /**
     * <p>
     * Setting value for lng.
     * </p>
     * 
     * @param lng
     *            the lng to set
     */
    public void setLng(double lng)
    {
        this.lng = lng;
    }

    /**
     * {@inheritDoc}
     *
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString()
    {
        return String.format("GeoCoordinate [lat=%s, lng=%s]", lat, lng);
    }

}
