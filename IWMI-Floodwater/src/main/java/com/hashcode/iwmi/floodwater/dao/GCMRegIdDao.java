/*
 * Created on Nov 11, 2014
 * 
 * All sources, binaries and HTML pages (C) copyright 2014 by NextLabs, Inc.,
 * San Mateo CA, Ownership remains with NextLabs, Inc., All rights reserved
 * worldwide.
 *
 */
package com.hashcode.iwmi.floodwater.dao;

import com.hashcode.iwmi.floodwater.domain.GCMRegId;

/**
 * <p>
 *  DAO for Google cloud registration
 * </p>
 *
 *
 * @author Amila Silva
 * @email  amilasilva88@gmail.com
 */
public interface GCMRegIdDao  extends GenericDao<GCMRegId, Long>
{

}
