/*
 * FILENAME
 *     BaseModel.java
 *
 * FILE LOCATION
 *     $Source$
 *
 * VERSION
 *     $Id$
 *         @version       $Revision$
 *         Check-Out Tag: $Name$
 *         Locked By:     $Lockers$
 *
 * FORMATTING NOTES
 *     * Lines should be limited to 78 characters.
 *     * Files should contain no tabs.
 *     * Indent code using four-character increments.
 *
 * COPYRIGHT
 *     Copyright (C) 2014 hashCode Solutions Pty. Ltd. All rights reserved.
 *     This software is the confidential and proprietary information of
 *     hashCode Solutions ("Confidential Information"). You shall not
 *     disclose such Confidential Information and shall use it only in
 *     accordance with the terms of the licence agreement you entered into
 *     with hashCode Solutions.
 */

package com.hashcode.iwmi.floodwater.domain;

import java.io.Serializable;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.MappedSuperclass;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Version;

import com.hashcode.iwmi.floodwater.util.LocalDateTimePersistenceConverter;

//
// IMPORTS
// NOTE: Import specific classes without using wildcards.
//

/**
 * <p>
 * Base model for all the domain classes.
 * </p>
 *
 * @author Amila Silva
 * @email amilasilva88@gmail.com
 * 
 **/
@MappedSuperclass
public abstract class BaseModel implements Serializable
{

    private static final long serialVersionUID = 6281509266782825890L;

    @Version
    private Long version;

    @Convert(converter = LocalDateTimePersistenceConverter.class)
    @Column(name = "last_updated_date")
    private LocalDateTime lastUpdatedDate;

    
    @Convert(converter = LocalDateTimePersistenceConverter.class)
    @Column(name = "created_date")
    private LocalDateTime createdDate;
    
    /**
     * <p>
     * Getter for id.
     * </p>
     * 
     * @return the id
     */
    public abstract Long getId();

    /**
     * <p>
     * Getter for version.
     * </p>
     * 
     * @return the version
     */
    public Long getVersion()
    {
        return version;
    }

    /**
     * <p>
     * Getter for lastUpdatedDate.
     * </p>
     * 
     * @return the lastUpdatedDate
     */
    public LocalDateTime getLastUpdatedDate()
    {
        return lastUpdatedDate;
    }

    /**
     * <p>
     * Setting value for lastUpdatedDate.
     * </p>
     * 
     * @param lastUpdatedDate
     *            the lastUpdatedDate to set
     */
    public void setLastUpdatedDate(LocalDateTime lastUpdatedDate)
    {
        this.lastUpdatedDate = lastUpdatedDate;
    }

    /**
     * <p>
     * Getter for createdDate.
     * </p>
     * 
     * @return the createdDate
     */
    public LocalDateTime getCreatedDate()
    {
        return createdDate;
    }

    /**
     * <p>
     * Setting value for createdDate.
     * </p>
     * 
     * @param createdDate
     *            the createdDate to set
     */
    public void setCreatedDate(LocalDateTime createdDate)
    {
        this.createdDate = createdDate;
    }

    @PrePersist 
    public void prePersist() {
        this.createdDate = LocalDateTime.now();
        this.lastUpdatedDate = LocalDateTime.now();
    }
    
    @PreUpdate 
    public void preUpdate() {
        this.lastUpdatedDate = LocalDateTime.now();
    }
}
