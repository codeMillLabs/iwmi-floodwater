/*
 * FILENAME
 *     MapDatar.java
 *
 * FILE LOCATION
 *     $Source$
 *
 * VERSION
 *     $Id$
 *         @version       $Revision$
 *         Check-Out Tag: $Name$
 *         Locked By:     $Lockers$
 *
 * FORMATTING NOTES
 *     * Lines should be limited to 78 characters.
 *     * Files should contain no tabs.
 *     * Indent code using four-character increments.
 *
 * COPYRIGHT
 *     Copyright (C) 2014 hashCode Solutions Pty. Ltd. All rights reserved.
 *     This software is the confidential and proprietary information of
 *     hashCode Solutions ("Confidential Information"). You shall not
 *     disclose such Confidential Information and shall use it only in
 *     accordance with the terms of the licence agreement you entered into
 *     with hashCode Solutions.
 */

package com.hashcode.iwmi.floodwater.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.codehaus.jackson.annotate.JsonProperty;

import com.hashcode.iwmi.floodwater.domain.GeoCoordinate;

//
// IMPORTS
// NOTE: Import specific classes without using wildcards.
//

/**
 * <p>
 * KML mapping data stored on this model.
 * </p>
 * 
 *
 * @author Amila Silva
 * @email amilasilva88@gmail.com
 * 
 **/
public class MapData implements Serializable
{

    private static final long serialVersionUID = 3918320742229802653L;

    private long id;
    
    @JsonProperty("map_name")
    private String mapName;

    @JsonProperty("routes")
    private List<RoutePlacemark> routePlacemarks = new ArrayList<>();
    
    @JsonProperty("points")
    private List<PointPlacemark> pointPlacemarks = new ArrayList<>();

    @JsonProperty("zoom_level")
    private int zoomLevel; // from property by Map name
    
    @JsonProperty("cam_position")
    private GeoCoordinate cameraPosition; // from property by Map name

    /**
     * <p>
     * Add Map data Route place mark.
     * </p>
     *
     * @param route
     *            {@link RoutePlacemark}
     *
     */
    public void addRoute(RoutePlacemark route)
    {
        getRoutePlacemarks().add(route);
    }

    /**
     * <p>
     * Add Map data Point place mark.
     * </p>
     *
     * @param route
     *            {@link PointPlacemark}
     *
     */
    public void addPoint(PointPlacemark point)
    {
        getPointPlacemarks().add(point);
    }

    /**
     * <p>
     * Getter for id.
     * </p>
     * 
     * @return the id
     */
    public long getId()
    {
        return id;
    }

    /**
     * <p>
     * Setting value for id.
     * </p>
     * 
     * @param id
     *            the id to set
     */
    public void setId(long id)
    {
        this.id = id;
    }

    /**
     * <p>
     * Getter for mapName.
     * </p>
     * 
     * @return the mapName
     */
    public String getMapName()
    {
        return mapName;
    }

    /**
     * <p>
     * Setting value for mapName.
     * </p>
     * 
     * @param mapName
     *            the mapName to set
     */
    public void setMapName(String mapName)
    {
        this.mapName = mapName;
    }

    /**
     * <p>
     * Getter for routePlacemarks.
     * </p>
     * 
     * @return the routePlacemarks
     */
    public List<RoutePlacemark> getRoutePlacemarks()
    {
        return routePlacemarks;
    }

    /**
     * <p>
     * Setting value for routePlacemarks.
     * </p>
     * 
     * @param routePlacemarks
     *            the routePlacemarks to set
     */
    public void setRoutePlacemarks(List<RoutePlacemark> routePlacemarks)
    {
        this.routePlacemarks = routePlacemarks;
    }

    /**
     * <p>
     * Getter for pointPlacemarks.
     * </p>
     * 
     * @return the pointPlacemarks
     */
    public List<PointPlacemark> getPointPlacemarks()
    {
        return pointPlacemarks;
    }

    /**
     * <p>
     * Setting value for pointPlacemarks.
     * </p>
     * 
     * @param pointPlacemarks
     *            the pointPlacemarks to set
     */
    public void setPointPlacemarks(List<PointPlacemark> pointPlacemarks)
    {
        this.pointPlacemarks = pointPlacemarks;
    }

    /**
     * <p>
     * Getter for zoomLevel.
     * </p>
     * 
     * @return the zoomLevel
     */
    public int getZoomLevel()
    {
        return zoomLevel;
    }

    /**
     * <p>
     * Setting value for zoomLevel.
     * </p>
     * 
     * @param zoomLevel
     *            the zoomLevel to set
     */
    public void setZoomLevel(int zoomLevel)
    {
        this.zoomLevel = zoomLevel;
    }

    /**
     * <p>
     * Getter for cameraPosition.
     * </p>
     * 
     * @return the cameraPosition
     */
    public GeoCoordinate getCameraPosition()
    {
        return cameraPosition;
    }

    /**
     * <p>
     * Setting value for cameraPosition.
     * </p>
     * 
     * @param cameraPosition
     *            the cameraPosition to set
     */
    public void setCameraPosition(GeoCoordinate cameraPosition)
    {
        this.cameraPosition = cameraPosition;
    }

}
