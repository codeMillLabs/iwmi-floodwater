/*
 * FILENAME
 *     KMLParseException.java
 *
 * FILE LOCATION
 *     $Source$
 *
 * VERSION
 *     $Id$
 *         @version       $Revision$
 *         Check-Out Tag: $Name$
 *         Locked By:     $Lockers$
 *
 * FORMATTING NOTES
 *     * Lines should be limited to 78 characters.
 *     * Files should contain no tabs.
 *     * Indent code using four-character increments.
 *
 * COPYRIGHT
 *     Copyright (C) 2014 hashCode Solutions Pty. Ltd. All rights reserved.
 *     This software is the confidential and proprietary information of
 *     hashCode Solutions ("Confidential Information"). You shall not
 *     disclose such Confidential Information and shall use it only in
 *     accordance with the terms of the licence agreement you entered into
 *     with hashCode Solutions.
 */

package com.hashcode.iwmi.floodwater.exception;

//
// IMPORTS
// NOTE: Import specific classes without using wildcards.
//

/**
 * <p>
 * KML parse exception for any exception related to KML parsing and processing.
 * </p>
 *
 * @author Amila Silva
 * @email amilasilva88@gmail.com
 **/
public class KMLParseException extends RuntimeException
{

    private static final long serialVersionUID = 4106364196796739819L;

    public KMLParseException()
    {
        super();
    }

    public KMLParseException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace)
    {
        super(message, cause, enableSuppression, writableStackTrace);
    }

    public KMLParseException(String message, Throwable cause)
    {
        super(message, cause);
    }

    public KMLParseException(String message)
    {
        super(message);
    }

    public KMLParseException(Throwable cause)
    {
        super(cause);
    }

}
