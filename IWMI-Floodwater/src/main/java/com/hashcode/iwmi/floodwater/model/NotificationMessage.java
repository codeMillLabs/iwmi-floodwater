/*
 * Created on Nov 11, 2014
 * 
 * All sources, binaries and HTML pages (C) copyright 2014 by NextLabs, Inc.,
 * San Mateo CA, Ownership remains with NextLabs, Inc., All rights reserved
 * worldwide.
 *
 */
package com.hashcode.iwmi.floodwater.model;

import java.io.Serializable;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * NotificationMessage
 * </p>
 *
 *
 * @author Amila Silva
 *
 */
public class NotificationMessage implements Serializable
{
    private static final long serialVersionUID = -1581942303046742423L;

    public static final String ALERT_TYPE = "ALERT";
    public static final String SYNC_MAP = "SYNC_MAP";
    
    private List<String> registration_ids;
    private Map<String, String> data;

    
    public void addRegId(String regId)
    {
        getRegistration_ids().add(regId);
    }

    public void createData(String type, String title, String message)
    {
        getData().put("message_type", type);
        getData().put("title", title);
        getData().put("message", message);
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString()
    {
        return String.format("NotificationMessage [ data=%s]", data);
    }

    /**
     * <p>
     * Getter method for registration_ids
     * </p>
     * 
     * @return the registration_ids
     */
    public List<String> getRegistration_ids()
    {
        if (registration_ids == null)
            registration_ids = new LinkedList<String>();
        return registration_ids;
    }

    /**
     * <p>
     * Setter method for registration_ids
     * </p>
     *
     * @param registration_ids
     *            the registration_ids to set
     */
    public void setRegistration_ids(List<String> registration_ids)
    {
        this.registration_ids = registration_ids;
    }

    /**
     * <p>
     * Getter method for data
     * </p>
     * 
     * @return the data
     */
    public Map<String, String> getData()
    {
        if (data == null)
            data = new HashMap<String, String>();
        return data;
    }

    /**
     * <p>
     * Setter method for data
     * </p>
     *
     * @param data
     *            the data to set
     */
    public void setData(Map<String, String> data)
    {
        this.data = data;
    }

}
