/*
 * FILENAME
 *     WaterLevelMeasuringPointDao.java
 *
 * FILE LOCATION
 *     $Source$
 *
 * VERSION
 *     $Id$
 *         @version       $Revision$
 *         Check-Out Tag: $Name$
 *         Locked By:     $Lockers$
 *
 * FORMATTING NOTES
 *     * Lines should be limited to 78 characters.
 *     * Files should contain no tabs.
 *     * Indent code using four-character increments.
 *
 * COPYRIGHT
 *     Copyright (C) 2013 hashCode Solutions Pty. Ltd. All rights reserved.
 *     This software is the confidential and proprietary information of
 *     hashCode Solutions ("Confidential Information"). You shall not
 *     disclose such Confidential Information and shall use it only in
 *     accordance with the terms of the licence agreement you entered into
 *     with hashCode Solutions.
 */

package com.hashcode.iwmi.floodwater.dao;

import com.hashcode.iwmi.floodwater.domain.WaterLevelMeasuringPoint;

//
// IMPORTS
// NOTE: Import specific classes without using wildcards.
//

/**
 * <p>
 * WaterLevelMeasuringPointDao interface.
 * </p>
 *
 * @author Amila Silva
 * @email  amilasilva88@gmail.com
 */
public interface WaterLevelMeasuringPointDao extends GenericDao<WaterLevelMeasuringPoint, Long>
{

    /**
     * <p>
     * Get the measuring point by name.
     * </p>
     *
     * @param name
     *            name of the point
     * @return {@link WaterLevelMeasuringPoint}
     *
     */
    WaterLevelMeasuringPoint getMeasuringPointByName(String name);
}
