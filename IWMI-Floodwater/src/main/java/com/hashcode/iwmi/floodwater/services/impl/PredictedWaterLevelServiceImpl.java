/*
 * FILENAME
 *     PredictedWaterLevelServiceImpl.java
 *
 * FILE LOCATION
 *     $Source$
 *
 * VERSION
 *     $Id$
 *         @version       $Revision$
 *         Check-Out Tag: $Name$
 *         Locked By:     $Lockers$
 *
 * FORMATTING NOTES
 *     * Lines should be limited to 78 characters.
 *     * Files should contain no tabs.
 *     * Indent code using four-character increments.
 *
 * COPYRIGHT
 *     Copyright (C) 2013 hashCode Solutions Pty. Ltd. All rights reserved.
 *     This software is the confidential and proprietary information of
 *     hashCode Solutions ("Confidential Information"). You shall not
 *     disclose such Confidential Information and shall use it only in
 *     accordance with the terms of the licence agreement you entered into
 *     with hashCode Solutions.
 */

package com.hashcode.iwmi.floodwater.services.impl;

import static com.hashcode.iwmi.floodwater.util.FloodwaterUtils.getDurationInDays;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.function.Function;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hashcode.iwmi.floodwater.dao.PredictedWaterLevelDao;
import com.hashcode.iwmi.floodwater.domain.PredictedWaterLevel;
import com.hashcode.iwmi.floodwater.services.PredictedWaterLevelService;

//
// IMPORTS
// NOTE: Import specific classes without using wildcards.
//

/**
 * <p>
 * PredictedWaterLevelService implementation.
 * </p>
 *
 * @author Manuja
 *
 * @version $Id$
 */
@Service
public class PredictedWaterLevelServiceImpl implements PredictedWaterLevelService
{
    private static final Logger log = LoggerFactory.getLogger(PredictedWaterLevelServiceImpl.class);
    
    @Autowired
    private PredictedWaterLevelDao predictedWaterLevelDao;
    
    /**
     * {@inheritDoc}
     *
     * @see PredictedWaterLevelService#getPredictedWaterLevels(String, LocalDate, int)
     */
    @Override
    public List<PredictedWaterLevel> getPredictedWaterLevels(String name, LocalDate date, int period)
    {
        return predictedWaterLevelDao.getPredictedWaterLevels(name, date, date.plusDays(period));
    }
    
    /**
     * <p>
     * Get actual water levels for given id and date period.
     * </p>
     *
     * @param id
     *            measuring point id
     * @param fromDate
     *            water level from date
     * @param toDate
     *            water level to date
     * 
     * @return list of actual water data levels
     */
    @Override
    public  List<PredictedWaterLevel> getPredictedWaterLevels(Long id, LocalDate fromDate, LocalDate toDate)
    {
        return predictedWaterLevelDao.getPredictedWaterLevels(id, fromDate, toDate);
    }

    /**
     * {@inheritDoc}
     *
     * @see com.hashcode.iwmi.floodwater.services.PredictedWaterLevelService#getPredictedWaterLevelsMap(java.lang.String, java.time.LocalDate, int)
     */
    @Override
    public Map<LocalDate, Double> getPredictedWaterLevelsMap(String name, LocalDate date, int period, boolean isPast)
    {
        Map<LocalDate, Double> waterLevelMap = new TreeMap<>();
        List<LocalDate> datesInBetween = null;
        
        List<PredictedWaterLevel> predictedWaterLevels = new ArrayList<>();
        if(isPast) {
            predictedWaterLevels =  predictedWaterLevelDao.getPredictedWaterLevels(name, date.minusDays(period), date.plusDays(1));
            datesInBetween = getDurationInDays(date.minusDays(period), date.plusDays(1));
        } else {
            predictedWaterLevels = predictedWaterLevelDao.getPredictedWaterLevels(name, date.plusDays(1), date.plusDays(period + 1));
            datesInBetween = getDurationInDays(date.plusDays(1), date.plusDays(period + 1));
        }

        Map<LocalDate, PredictedWaterLevel> waterLevelByDatesMap =
            predictedWaterLevels.stream().collect(Collectors.toMap(PredictedWaterLevel::getDate, Function.identity()));

        log.debug("Total no of days during the dates : {}.", datesInBetween.size());
        
        datesInBetween.stream().forEach((thisDate) -> {
            if (waterLevelByDatesMap.containsKey(thisDate))
            {
                waterLevelMap.put(thisDate, waterLevelByDatesMap.get(thisDate).getWaterLevel());
            } else 
                waterLevelMap.put(thisDate, 0.0D);
        });
        log.info("Predicted water level Map, Name : {}, Data size :{}", name, waterLevelMap.size());
        return waterLevelMap;
    }
    
    

}
