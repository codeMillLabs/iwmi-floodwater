/*
 * FILENAME
 *     UserDaoImpl.java
 *
 * FILE LOCATION
 *     $Source$
 *
 * VERSION
 *     $Id$
 *         @version       $Revision$
 *         Check-Out Tag: $Name$
 *         Locked By:     $Lockers$
 *
 * FORMATTING NOTES
 *     * Lines should be limited to 78 characters.
 *     * Files should contain no tabs.
 *     * Indent code using four-character increments.
 *
 * COPYRIGHT
 *     Copyright (C) 2013 hashCode Solutions Pty. Ltd. All rights reserved.
 *     This software is the confidential and proprietary information of
 *     hashCode Solutions ("Confidential Information"). You shall not
 *     disclose such Confidential Information and shall use it only in
 *     accordance with the terms of the licence agreement you entered into
 *     with hashCode Solutions.
 */

package com.hashcode.iwmi.floodwater.dao.impl;

import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;
import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import com.hashcode.iwmi.floodwater.dao.UserDao;
import com.hashcode.iwmi.floodwater.domain.User;

//
// IMPORTS
// NOTE: Import specific classes without using wildcards.
//

/**
 * <p>
 * UserDao implementation.
 * </p>
 *
 * @author Manuja
 *
 * @version $Id$
 */
@Transactional
@Repository
public class UserDaoImpl extends GenericDaoImpl<User, Long> implements UserDao
{
    private static final Logger log = LoggerFactory.getLogger(UserDaoImpl.class);

    /**
     * {@inheritDoc}
     *
     * @see UserDao#authenticateUser(String, String)
     */
    @Override
    public User authenticateUser(final String username, final String password)
    {
        try
        {
            log.info("User authentication request received, Username : {}");

            String queryText =
                "SELECT user FROM " + User.class.getName()
                    + " user WHERE user.email= :username AND user.password= :password";

            TypedQuery<User> query = entityManager.createQuery(queryText, User.class);
            query.setParameter("username", username);
            query.setParameter("password", password);
            query.setMaxResults(1);
            return query.getSingleResult();
        }
        catch (NoResultException e)
        {
            log.warn("Unauthroized user access attempt");
            return null;
        }
    }

    /**
     * {@inheritDoc}
     *
     * @see UserDao#getUser(java.lang.String)
     */
    @Override
    public User getUser(String username)
    {
        try
        {
            log.info("User authentication request received, Username : {}");

            String queryText = "SELECT user FROM " + User.class.getName() + " user WHERE user.email= :username";

            TypedQuery<User> query = entityManager.createQuery(queryText, User.class);
            query.setParameter("username", username);
            query.setMaxResults(1);
            return query.getSingleResult();
        }
        catch (NoResultException e)
        {
            log.warn("Unauthroized user access attempt");
            return null;
        }
    }
}
