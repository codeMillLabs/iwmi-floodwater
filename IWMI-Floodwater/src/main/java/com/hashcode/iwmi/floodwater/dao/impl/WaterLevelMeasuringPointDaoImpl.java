/*
 * FILENAME
 *     WaterLevelMeasuringPointDaoImpl.java
 *
 * FILE LOCATION
 *     $Source$
 *
 * VERSION
 *     $Id$
 *         @version       $Revision$
 *         Check-Out Tag: $Name$
 *         Locked By:     $Lockers$
 *
 * FORMATTING NOTES
 *     * Lines should be limited to 78 characters.
 *     * Files should contain no tabs.
 *     * Indent code using four-character increments.
 *
 * COPYRIGHT
 *     Copyright (C) 2013 hashCode Solutions Pty. Ltd. All rights reserved.
 *     This software is the confidential and proprietary information of
 *     hashCode Solutions ("Confidential Information"). You shall not
 *     disclose such Confidential Information and shall use it only in
 *     accordance with the terms of the licence agreement you entered into
 *     with hashCode Solutions.
 */

package com.hashcode.iwmi.floodwater.dao.impl;

import javax.persistence.TypedQuery;
import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import com.hashcode.iwmi.floodwater.dao.WaterLevelMeasuringPointDao;
import com.hashcode.iwmi.floodwater.domain.WaterLevelMeasuringPoint;

//
// IMPORTS
// NOTE: Import specific classes without using wildcards.
//

/**
 * <p>
 * WaterLevelMeasuringPointDao implementation.
 * </p>
 *
 * @author Manuja
 *
 * @version $Id$
 */
@Transactional
@Repository
public class WaterLevelMeasuringPointDaoImpl extends GenericDaoImpl<WaterLevelMeasuringPoint, Long> implements
    WaterLevelMeasuringPointDao
{
    private static final Logger log = LoggerFactory.getLogger(WaterLevelMeasuringPointDaoImpl.class);

    /**
     * {@inheritDoc}
     *
     * @see com.hashcode.iwmi.floodwater.dao.WaterLevelMeasuringPointDao#getMeasuringPointByName(java.lang.String)
     */
    @Override
    public WaterLevelMeasuringPoint getMeasuringPointByName(String name)
    {
        log.info("Get Water level measuring point by name :{}", name);
        String queryText = "SELECT p FROM " + WaterLevelMeasuringPoint.class.getName() + " p WHERE p.name = :name ";
        
        TypedQuery<WaterLevelMeasuringPoint> query = entityManager.createQuery(queryText, WaterLevelMeasuringPoint.class);
        query.setParameter("name", name);
        WaterLevelMeasuringPoint point = query.getSingleResult();
        
        log.debug("Water level measuring point by name :{}, value:{}", name, point);
        return point;
    }
    
}