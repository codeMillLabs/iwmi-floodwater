/*
 * FILENAME
 *     WaterLevel.java
 *
 * FILE LOCATION
 *     $Source$
 *
 * VERSION
 *     $Id$
 *         @version       $Revision$
 *         Check-Out Tag: $Name$
 *         Locked By:     $Lockers$
 *
 * FORMATTING NOTES
 *     * Lines should be limited to 78 characters.
 *     * Files should contain no tabs.
 *     * Indent code using four-character increments.
 *
 * COPYRIGHT
 *     Copyright (C) 2014 hashCode Solutions Pty. Ltd. All rights reserved.
 *     This software is the confidential and proprietary information of
 *     hashCode Solutions ("Confidential Information"). You shall not
 *     disclose such Confidential Information and shall use it only in
 *     accordance with the terms of the licence agreement you entered into
 *     with hashCode Solutions.
 */
package com.hashcode.iwmi.floodwater.domain;

import java.time.LocalDate;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;
import javax.persistence.PrimaryKeyJoinColumn;

import com.hashcode.iwmi.floodwater.util.LocalDatePersistenceConverter;

/**
 * <p>
 * Abstract WaterLevel domain class.
 * </p>
 *
 *
 * @author Amila Silva
 * @email amilasilva88@gmail.com
 *
 */
@MappedSuperclass
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
public abstract class WaterLevel extends BaseModel
{

    private static final long serialVersionUID = 7859026272963257068L;

    @Column(name = "user_id")
    protected String userId;

    @Column(name = "comment")
    protected String comment;

    @Column(name = "water_level")
    protected double waterLevel;

    @ManyToOne(cascade = CascadeType.ALL)
    @PrimaryKeyJoinColumn
    private WaterLevelMeasuringPoint measuringPoint;

    @Column(name = "meassured_date")
    @Convert(converter = LocalDatePersistenceConverter.class)
    protected LocalDate date;

    /**
     * <p>
     * Getter for userId.
     * </p>
     * 
     * @return the userId
     */
    public String getUserId()
    {
        return userId;
    }

    /**
     * <p>
     * Setting value for userId.
     * </p>
     * 
     * @param userId
     *            the userId to set
     */
    public void setUserId(String userId)
    {
        this.userId = userId;
    }

    /**
     * <p>
     * Getter for comment.
     * </p>
     * 
     * @return the comment
     */
    public String getComment()
    {
        return comment;
    }

    /**
     * <p>
     * Setting value for comment.
     * </p>
     * 
     * @param comment
     *            the comment to set
     */
    public void setComment(String comment)
    {
        this.comment = comment;
    }

    /**
     * <p>
     * Getter for waterLevel.
     * </p>
     * 
     * @return the waterLevel
     */
    public double getWaterLevel()
    {
        return waterLevel;
    }

    /**
     * <p>
     * Setting value for waterLevel.
     * </p>
     * 
     * @param waterLevel
     *            the waterLevel to set
     */
    public void setWaterLevel(double waterLevel)
    {
        this.waterLevel = waterLevel;
    }

    /**
     * <p>
     * Getter for measuringPoint.
     * </p>
     * 
     * @return the measuringPoint
     */
    public WaterLevelMeasuringPoint getMeasuringPoint()
    {
        return measuringPoint;
    }

    /**
     * <p>
     * Setting value for measuringPoint.
     * </p>
     * 
     * @param measuringPoint
     *            the measuringPoint to set
     */
    public void setMeasuringPoint(WaterLevelMeasuringPoint measuringPoint)
    {
        this.measuringPoint = measuringPoint;
    }

    /**
     * <p>
     * Getter for date.
     * </p>
     * 
     * @return the date
     */
    public LocalDate getDate()
    {
        return date;
    }

    /**
     * <p>
     * Setting value for date.
     * </p>
     * 
     * @param date
     *            the date to set
     */
    public void setDate(LocalDate date)
    {
        this.date = date;
    }

}
