/*
 * FILENAME
 *     KMLFileUtil.java
 *
 * FILE LOCATION
 *     $Source$
 *
 * VERSION
 *     $Id$
 *         @version       $Revision$
 *         Check-Out Tag: $Name$
 *         Locked By:     $Lockers$
 *
 * FORMATTING NOTES
 *     * Lines should be limited to 78 characters.
 *     * Files should contain no tabs.
 *     * Indent code using four-character increments.
 *
 * COPYRIGHT
 *     Copyright (C) 2014 hashCode Solutions Pty. Ltd. All rights reserved.
 *     This software is the confidential and proprietary information of
 *     hashCode Solutions ("Confidential Information"). You shall not
 *     disclose such Confidential Information and shall use it only in
 *     accordance with the terms of the licence agreement you entered into
 *     with hashCode Solutions.
 */

package com.hashcode.iwmi.floodwater.util;

import static com.hashcode.iwmi.floodwater.util.FloodwaterUtils.getLastModifiedDate;
import static com.hashcode.iwmi.floodwater.util.FloodwaterUtils.isModified;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.codehaus.jackson.map.ObjectMapper;
import org.json.JSONArray;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.hashcode.iwmi.floodwater.exception.KMLParseException;
import com.hashcode.iwmi.floodwater.kml.KMLFileParser;
import com.hashcode.iwmi.floodwater.model.KMLFileMetaData;
import com.hashcode.iwmi.floodwater.model.KMLMetaDataFolder;
import com.hashcode.iwmi.floodwater.model.MapData;

/**
 * <p>
 * KMLFileUtil
 * </p>
 *
 *
 * @author Amila Silva
 * @email amilasilva88@gmail.com
 */
public final class KMLFileUtil
{
    private static final Logger log = LoggerFactory.getLogger(KMLFileUtil.class);

    public static KMLMetaDataFolder loadMetaData(String filePath) throws KMLParseException
    {
        log.debug("Load KML metadata from {} ", filePath);
        try
        {
            KMLMetaDataFolder folder = new KMLMetaDataFolder();
            folder.setId("IWMI-KML Folder");
            folder.setLastModifiedDate(getLastModifiedDate(filePath));
            folder.setFileMetaData(new ArrayList<KMLFileMetaData>());

            String metaDataStr = new String(Files.readAllBytes(Paths.get(filePath)));

            ObjectMapper mapper = new ObjectMapper();
            JSONArray jon = new JSONArray(metaDataStr);

            for (int i = 0; i < jon.length(); i++)
            {
                folder.getFileMetaData().add(mapper.readValue(jon.get(i).toString().getBytes(), KMLFileMetaData.class));
            }

            log.info("Load KML metadata successfully, [ Folder: {} ]", folder);
            return folder;
        }
        catch (Exception e)
        {
            throw new KMLParseException("Error occurred in load meta data, ", e);
        }
    }

    public static void main(String a[]) throws IOException
    {

        //		List<KMLFileMetaData> metaData = new ArrayList<>();
        //		
        //		
        //		KMLFileMetaData file1 = new KMLFileMetaData();
        //		file1.setId("V-1");
        //		file1.setName("Niger Revier Map");
        //		file1.setMapZoomLevel(1);
        //		file1.setCamPosition(new GeoCoordinate(6.756340837897481, 7.734294142843669));
        //		file1.getKmlFiles().add("C:\\Users\\asilva\\Downloads\\Niger_JA2_VS.kml");
        //		file1.getKmlFiles().add("C:\\Users\\asilva\\Downloads\\Niger_JA2_VS.kml");
        //		
        //		metaData.add(file1);
        //		metaData.add(file1);
        //		metaData.add(file1);
        //		
        //		ObjectMapper mapper = new ObjectMapper();
        //		mapper.writeValue(System.out, metaData);
        //		JSONArray jon = new JSONArray(metaData);
        LocalDate myDate = LocalDate.of(2015, 1, 4);
        long milis = myDate.atStartOfDay().atZone(ZoneId.systemDefault()).toInstant().toEpochMilli();
        System.out.println("-----------------------------");
        System.out.println("------------------- TIME STAMP : "  + Calendar.getInstance().getTimeInMillis());
        System.out.println("------------------- TIME STAMP 2 : "  + milis);
        System.out.println("-----------------------------");

//        KMLMetaDataFolder folder = loadMetaData("C:\\Users\\Amila Silva\\git\\iwmi-floodwater\\IWMI-Floodwater\\src\\main\\resources\\kml_props.meta");
//
//        KMLFileParser parser = new KMLFileParser();
//        List<MapData> mapDataList = parser.parseMultiKML(folder.getFileMetaData());
//
//        System.out.println("::: MAP DATA LIST :" + mapDataList);
//        System.out.println("::: MAP DATA LIST :" + mapDataList.size());
//
//        LocalDateTime dateTime1 = getLastModifiedDate("C:\\Users\\asilva\\Downloads\\kml_props.meta");
//        LocalDateTime dateTime2 = getLastModifiedDate("C:\\Users\\asilva\\Downloads\\Niger_JA2_VS.kml");
//        
//        System.out.println(":  :" + dateTime1.format(DateTimeFormatter.ofPattern("YYYY-MMM-dd")));
//
//        System.out.println(":::::::  DATE TIME : " + isModified(dateTime1, dateTime2));
//        System.out.println(":::::::  DATE TIME : " + isModified(dateTime2, dateTime1));

    }

}
