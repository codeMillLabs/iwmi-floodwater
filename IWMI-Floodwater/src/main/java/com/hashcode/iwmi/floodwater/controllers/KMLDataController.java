/*
 * FILENAME
 *     KMLDataController.java
 *
 * FILE LOCATION
 *     $Source$
 *
 * VERSION
 *     $Id$
 *         @version       $Revision$
 *         Check-Out Tag: $Name$
 *         Locked By:     $Lockers$
 *
 * FORMATTING NOTES
 *     * Lines should be limited to 78 characters.
 *     * Files should contain no tabs.
 *     * Indent code using four-character increments.
 *
 * COPYRIGHT
 *     Copyright (C) 2014 hashCode Solutions Pty. Ltd. All rights reserved.
 *     This software is the confidential and proprietary information of
 *     hashCode Solutions ("Confidential Information"). You shall not
 *     disclose such Confidential Information and shall use it only in
 *     accordance with the terms of the licence agreement you entered into
 *     with hashCode Solutions.
 */

package com.hashcode.iwmi.floodwater.controllers;

import static com.hashcode.iwmi.floodwater.util.FloodwaterUtils.createResponse;
import static com.hashcode.iwmi.floodwater.util.FloodwaterUtils.getLastModifiedDate;
import static com.hashcode.iwmi.floodwater.util.FloodwaterUtils.getResponceDataMap;
import static com.hashcode.iwmi.floodwater.util.FloodwaterUtils.isModified;

import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.hashcode.iwmi.floodwater.dao.GCMRegIdDao;
import com.hashcode.iwmi.floodwater.dao.WaterLevelMeasuringPointDao;
import com.hashcode.iwmi.floodwater.domain.GCMRegId;
import com.hashcode.iwmi.floodwater.domain.PredictedWaterLevel;
import com.hashcode.iwmi.floodwater.domain.WaterLevelMeasuringPoint;
import com.hashcode.iwmi.floodwater.exception.KMLParseException;
import com.hashcode.iwmi.floodwater.kml.KMLFileParser;
import com.hashcode.iwmi.floodwater.model.KMLMetaDataFolder;
import com.hashcode.iwmi.floodwater.model.MapData;
import com.hashcode.iwmi.floodwater.services.ActualWaterLevelService;
import com.hashcode.iwmi.floodwater.services.PredictedWaterLevelService;
import com.hashcode.iwmi.floodwater.util.FloodwaterUtils;
import com.hashcode.iwmi.floodwater.util.KMLFileUtil;
import com.hashcode.iwmi.floodwater.util.ResponseStatus;

//
// IMPORTS
// NOTE: Import specific classes without using wildcards.
//

/**
 * <p>
 * This controller will handle the REST calls to KML data.
 * </p>
 * 
 *
 * @author Amila Silva
 * @email amilasilva88@gmail.com
 **/
@Controller
@RequestMapping("/kml")
public class KMLDataController
{
    private static final Logger log = LoggerFactory.getLogger(KMLDataController.class);

    @Value("${kml.meta.prop.filepath}")
    private String kmlMetaFile;

    @Value("${actual.data.period.in.days}")
    private int actualDataPeriodInDays = 30;

    @Value("${predicted.data.period.in.days}")
    private int predictedDataPeriodInDays = 5;
    
    @Value("${flood.water.alert.threshold}")
    private double thresholdFloodLevel = 100.0;
    
    @Value("${flood.water.alert.moderate}")
    private double moderateFloodLevel = 100.0;

    @Autowired
    private KMLFileParser kmlParser;

    @Autowired
    private ActualWaterLevelService actualWaterLevelService;

    @Autowired
    private PredictedWaterLevelService predictedWaterLevelService;
    
    @Autowired
    private WaterLevelMeasuringPointDao waterLevelMeasuringPointDao;
    
    @Autowired
    private GCMRegIdDao gcmRegIdDao;

    /**
     * <p>
     * Load map data if modified.
     * </p>
     *
     * @param lastModifiedTime
     *            last modified time
     * @return
     *
     */
    @RequestMapping(value = "/loadIfModified/{lastModifiedTime}/{id}", method = {
        RequestMethod.GET, RequestMethod.POST
    })
    @ResponseBody
    public String loadIfModified(@PathVariable("lastModifiedTime") long lastModifiedTime,
        @PathVariable("id") String mapId)
    {
        try
        {
            log.info("isRecentlyModified [ lastModified date : {}, Map : {} ]", lastModifiedTime, mapId);
            LocalDateTime kmlMetaLastModified = getLastModifiedDate(kmlMetaFile);
            LocalDateTime cacheLastModified =
                Instant.ofEpochMilli(lastModifiedTime).atZone(ZoneId.systemDefault()).toLocalDateTime();

            boolean isModified = isModified(cacheLastModified, kmlMetaLastModified);
            log.info("isRecentlyModified [ Modified : {} ]", isModified);

            Map<String, Object> params = FloodwaterUtils.getResponceDataMap();
            params.put("modified", isModified);

            if (isModified)
            {
                KMLMetaDataFolder metaFolder = KMLFileUtil.loadMetaData(kmlMetaFile);
                log.debug("KML meta data folder loaded : {}", metaFolder);

                List<MapData> mapDataList = kmlParser.parseMultiKML(metaFolder.getFileMetaData());

                params.put("last_modified_date", metaFolder.getLastModifiedDate().atZone(ZoneId.systemDefault())
                    .toInstant().toEpochMilli());
                params.put("map_data_size", mapDataList.size());

                if (!mapDataList.isEmpty())
                {
                    JSONArray dataArry = new JSONArray(mapDataList);
                    params.put("map_data", dataArry);
                }
            }
            JSONObject jsonReponse = createResponse(ResponseStatus.SUCCESS, params);
            log.debug("JSON Response for loadIfModified to be send, Response : {}", jsonReponse);
            return jsonReponse.toString();
        }
        catch (Exception e)
        {
            Map<String, Object> params = FloodwaterUtils.getResponceDataMap();
            params.put("error", e.getLocalizedMessage());
            return createResponse(ResponseStatus.FAILED, params).toString();
        }
    }

    @RequestMapping(value = "/modified/{lastModifiedTime}", method = {
        RequestMethod.GET, RequestMethod.POST
    })
    @ResponseBody
    public String isRecentlyModified(@PathVariable("lastModifiedTime") long lastModifiedTime)
    {
        log.info("isRecentlyModified [ lastModified date : {} ]", lastModifiedTime);
        LocalDateTime kmlMetaLastModified = getLastModifiedDate(kmlMetaFile);
        LocalDateTime cacheLastModified =
            Instant.ofEpochMilli(lastModifiedTime).atZone(ZoneId.systemDefault()).toLocalDateTime();

        boolean isModified = isModified(cacheLastModified, kmlMetaLastModified);

        log.info("isRecentlyModified [ Modified : {} ]", isModified);

        Map<String, Object> params = FloodwaterUtils.getResponceDataMap();
        params.put("modified", isModified);

        return createResponse(ResponseStatus.SUCCESS, params).toString();
    }

    @RequestMapping(value = "/kmlMapData/{id}", method = {
        RequestMethod.GET, RequestMethod.POST
    })
    @ResponseBody
    public String getKMLMapData(@PathVariable("id") String mapId)
    {
        log.info("Request to fetch KML Map data from server [ Map id : {}, Meta file : {}]", mapId, kmlMetaFile);
        try
        {
            KMLMetaDataFolder metaFolder = KMLFileUtil.loadMetaData(kmlMetaFile);
            log.debug("KML meta data folder loaded : {}", metaFolder);
            List<MapData> mapDataList = kmlParser.parseMultiKML(metaFolder.getFileMetaData());

            Map<String, Object> params = FloodwaterUtils.getResponceDataMap();
            params.put("mapdata_size", mapDataList.size());

            JSONObject jsonReponse = createResponse(ResponseStatus.SUCCESS, null);
            jsonReponse.put("map_data", mapDataList);

            return jsonReponse.toString();
        }
        catch (KMLParseException e)
        {
            Map<String, Object> params = FloodwaterUtils.getResponceDataMap();
            params.put("error", e.getLocalizedMessage());
            return createResponse(ResponseStatus.FAILED, params).toString();
        }
    }

    @RequestMapping(value = "/waterLevel/summary", method = RequestMethod.POST)
	@ResponseBody
	public String getWaterLevelSummary(@RequestBody String request) {
		try {
			JSONObject summaryReq = new JSONObject(request);
			LocalDate queryDate = LocalDate.parse(summaryReq.getString("date"));
			String locationId = summaryReq.getString("location_id");
			int actualDays = summaryReq.has("no_past_days") ? summaryReq
					.getInt("no_past_days") : actualDataPeriodInDays;
			boolean dateInMilis = summaryReq.has("date_in_milis") ? summaryReq.getBoolean("date_in_milis") : false;

			log.info(
					"Request to fetch water level summary for [ Location Id: {}, Request Date : {}]",
					locationId, queryDate);

//			Map<LocalDate, Double> actualWaterLevelDataMap = actualWaterLevelService
//					.getActualWaterLevelsMap(locationId, queryDate, actualDays);
			
			Map<LocalDate, Double> actualWaterLevelDataMap = predictedWaterLevelService
              .getPredictedWaterLevelsMap(locationId, queryDate, actualDays, true); // past days + today
			
			Map<LocalDate, Double> predictedWaterLevelDataMap = predictedWaterLevelService
					.getPredictedWaterLevelsMap(locationId, queryDate,
							predictedDataPeriodInDays, false);

			JSONArray actualData = getWaterLevelData(queryDate,
					actualWaterLevelDataMap, dateInMilis);
			
			JSONObject todayData = new JSONObject()
					.put("label", (dateInMilis) ? queryDate.atStartOfDay().atZone(ZoneId.systemDefault()).toInstant().toEpochMilli() : queryDate.toString())
					.put("value",
							actualWaterLevelDataMap.containsKey(queryDate) ? actualWaterLevelDataMap
									.get(queryDate) : new Double(0.0));
			JSONArray predictedData = getWaterLevelData(queryDate,
					predictedWaterLevelDataMap, dateInMilis);
			
			WaterLevelMeasuringPoint pointMeta = waterLevelMeasuringPointDao.getMeasuringPointByName(locationId);

			Map<String, Object> params = getResponceDataMap();
			params.put("query_date", queryDate);
			params.put("location_name", locationId);
			params.put("moderate_value", pointMeta.getModerateLevelThreshold());
			params.put("threshold_value", pointMeta.getRiskLevelThreshold());
			params.put("actual_data", actualData);
			params.put("today", todayData);
			params.put("forcasting_data", predictedData);

			JSONObject response = createResponse(ResponseStatus.SUCCESS, params);
			log.debug("JSON Response for Water Level Summary  : {}", response);
			return response.toString();
		} catch (KMLParseException e) {
			Map<String, Object> params = FloodwaterUtils.getResponceDataMap();
			params.put("error", e.getLocalizedMessage());
			return createResponse(ResponseStatus.FAILED, params).toString();
		}
	}
    
    @RequestMapping(value = "/waterLevel/archive", method = RequestMethod.POST)
	@ResponseBody
	public String getWaterLevelArchiveData(@RequestBody String request) {
		try {
			JSONObject summaryReq = new JSONObject(request);
			LocalDate queryDate = LocalDate.parse(summaryReq.getString("date"));
			String locationId = summaryReq.getString("location_id");
			int actualDays = summaryReq.has("no_past_days") ? summaryReq
					.getInt("no_past_days") : actualDataPeriodInDays;
			boolean dateInMilis = summaryReq.has("date_in_milis") ? summaryReq.getBoolean("date_in_milis") : false;

			log.info(
					"Request to fetch water level archive for [ Location Id: {}, Request Date : {}]",
					locationId, queryDate);

			Map<LocalDate, Double> actualWaterLevelDataMap = actualWaterLevelService
					.getActualWaterLevelsMap(locationId, queryDate, actualDays);
			
//			Map<LocalDate, Double> predictedWaterLevelDataMap = predictedWaterLevelService
//					.getPredictedWaterLevelsMap(locationId, queryDate,
//							predictedDataPeriodInDays, false);

			JSONArray actualData = getWaterLevelData(queryDate,
					actualWaterLevelDataMap, dateInMilis);
			
			JSONObject todayData = new JSONObject()
					.put("label", (dateInMilis) ? queryDate.atStartOfDay().atZone(ZoneId.systemDefault()).toInstant().toEpochMilli() : queryDate.toString())
					.put("value",
							actualWaterLevelDataMap.containsKey(queryDate) ? actualWaterLevelDataMap
									.get(queryDate) : new Double(0.0));
			JSONArray predictedData = new JSONArray();
//			JSONArray predictedData = getWaterLevelData(queryDate,
//					predictedWaterLevelDataMap, dateInMilis);
			
			WaterLevelMeasuringPoint pointMeta = waterLevelMeasuringPointDao.getMeasuringPointByName(locationId);

			Map<String, Object> params = getResponceDataMap();
			params.put("query_date", queryDate);
			params.put("location_name", locationId);
			params.put("moderate_value", pointMeta.getModerateLevelThreshold());
			params.put("threshold_value", pointMeta.getRiskLevelThreshold());
			params.put("actual_data", actualData);
			params.put("today", todayData);
			params.put("forcasting_data", predictedData);

			JSONObject response = createResponse(ResponseStatus.SUCCESS, params);
			log.debug("JSON Response for Water Level archive  : {}", response);
			return response.toString();
		} catch (KMLParseException e) {
			Map<String, Object> params = FloodwaterUtils.getResponceDataMap();
			params.put("error", e.getLocalizedMessage());
			return createResponse(ResponseStatus.FAILED, params).toString();
		}
	}

    /**
     * <p>
     * Client to check the server status.
     * </p>
     *
     * @param request
     * @return JSON string with the status
     */
    @RequestMapping(value = "/statusCheck", method = {
        RequestMethod.GET, RequestMethod.POST, RequestMethod.PUT
    })
    @ResponseBody
    public String statusCheck(@RequestBody String request)
    {
        JSONObject response = createResponse(ResponseStatus.SUCCESS, null);
        log.debug(" Status Check  : {}", response);
        return response.toString();
    }
    
    
    /**
     * <p>
     * Save GCM registration id.
     * </p>
     *
     * @param request
     * @return JSON string with the status
     */
    @RequestMapping(value = "/saveRegId", method = {
        RequestMethod.GET, RequestMethod.POST, RequestMethod.PUT
    })
    @ResponseBody
    public String saveRegId(@RequestBody String request)
    {
        JSONObject reqObj = new JSONObject(request);
        
        String regIdStr = reqObj.getString("reg_id");
        GCMRegId regId = new GCMRegId();
        regId.setRegId(regIdStr);
        
        gcmRegIdDao.create(regId);
        log.info("New GCM Registration Id saved, Id : {}", regId);
        
        Map<String, Object> resData = new HashMap<String, Object>();
        resData.put("reg_id", regIdStr);
        
        JSONObject response = createResponse(ResponseStatus.SUCCESS, resData);
        return response.toString();
    }
    
    /**
     * <p>
     * Load Data collection Points data.
     * </p>
     *
     * @param lastModifiedTime
     *            last modified time
     * @return
     *
     */
    @RequestMapping(value = "/loadMarkers/{date}", method = {
        RequestMethod.GET, RequestMethod.POST
    })
    @ResponseBody
    public String loadMarkers(@PathVariable("date") long date)
    {
        try
        {
            log.info("Load Markers [ Date : {}]", date);
            LocalDate queryDate =  Instant.ofEpochMilli(date).atZone(ZoneId.systemDefault()).toLocalDate();

            JSONArray dataArray = new JSONArray();
            List<WaterLevelMeasuringPoint> measuringPoints = waterLevelMeasuringPointDao.findAll();
            
            measuringPoints.stream().forEach(n -> {
            	List<PredictedWaterLevel> waterLevels = predictedWaterLevelService.getPredictedWaterLevels(n.getId(), queryDate, queryDate.plusDays(1));
                log.info("Water Level details :" + waterLevels.size());
                
                if(waterLevels.size() > 0){
                	PredictedWaterLevel waterLevel = waterLevels.stream().findFirst().get();
                    JSONObject pointValue = new JSONObject();
                    pointValue.put("id", n.getId());
                    pointValue.put("name", n.getName());
                    pointValue.put("latitude", n.getGeoCoordinate().getLat());
                    pointValue.put("longitude", n.getGeoCoordinate().getLng());
                    pointValue.put("water_level", waterLevel.getWaterLevel());
                    pointValue.put("is_risk", (n.getRiskLevelThreshold() <= waterLevel.getWaterLevel()) ? true : false);
                    pointValue.put("is_moderate", (n.getModerateLevelThreshold() <= waterLevel.getWaterLevel() && n.getRiskLevelThreshold() > waterLevel.getWaterLevel()) ? true : false);
                    
                    dataArray.put(pointValue);
                }
            });
            
            Map<String, Object> params = FloodwaterUtils.getResponceDataMap();
            params.put("data", dataArray);

            JSONObject jsonReponse = createResponse(ResponseStatus.SUCCESS, params);
            log.debug("JSON Response for loadMarkers to be send, Response : {}", jsonReponse);
            return jsonReponse.toString();
        }
        catch (Exception e)
        {
            Map<String, Object> params = FloodwaterUtils.getResponceDataMap();
            params.put("error", e.getLocalizedMessage());
            return createResponse(ResponseStatus.FAILED, params).toString();
        }
    }


    
    
    private JSONArray getWaterLevelData(LocalDate queryDate, Map<LocalDate, Double> waterLevelData, boolean dateInMilis)
    {
        JSONArray jsonArr = new JSONArray();
        waterLevelData.forEach((date, level) -> {
            if (!date.isEqual(queryDate))
            {
                JSONObject waterLvl = new JSONObject();
                if (dateInMilis)
                {
                    long milis = date.atStartOfDay().atZone(ZoneId.systemDefault()).toInstant().toEpochMilli();
                    waterLvl.put("label", milis);
                }
                else
                {
                    waterLvl.put("label", date.format(DateTimeFormatter.ofPattern("dd-MMM-yyyy")));
                }

                waterLvl.put("value", level);

                jsonArr.put(waterLvl);
            }
        });

        return jsonArr;
    }
}
