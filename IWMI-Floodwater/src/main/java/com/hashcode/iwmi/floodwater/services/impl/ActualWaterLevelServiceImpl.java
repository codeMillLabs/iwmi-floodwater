/*
 * FILENAME
 *     ActualWaterLevelServiceImpl.java
 *
 * FILE LOCATION
 *     $Source$
 *
 * VERSION
 *     $Id$
 *         @version       $Revision$
 *         Check-Out Tag: $Name$
 *         Locked By:     $Lockers$
 *
 * FORMATTING NOTES
 *     * Lines should be limited to 78 characters.
 *     * Files should contain no tabs.
 *     * Indent code using four-character increments.
 *
 * COPYRIGHT
 *     Copyright (C) 2013 hashCode Solutions Pty. Ltd. All rights reserved.
 *     This software is the confidential and proprietary information of
 *     hashCode Solutions ("Confidential Information"). You shall not
 *     disclose such Confidential Information and shall use it only in
 *     accordance with the terms of the licence agreement you entered into
 *     with hashCode Solutions.
 */

package com.hashcode.iwmi.floodwater.services.impl;

import static com.hashcode.iwmi.floodwater.util.FloodwaterUtils.getDurationInDays;

import java.time.LocalDate;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.function.Function;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hashcode.iwmi.floodwater.dao.ActualWaterLevelDao;
import com.hashcode.iwmi.floodwater.domain.ActualWaterLevel;
import com.hashcode.iwmi.floodwater.services.ActualWaterLevelService;

//
// IMPORTS
// NOTE: Import specific classes without using wildcards.
//

/**
 * <p>
 * ActualWaterLevelService Implementation.
 * </p>
 *
 * @author Manuja
 *
 * @version $Id$
 */
@Service
public class ActualWaterLevelServiceImpl implements ActualWaterLevelService
{

    private static final Logger log = LoggerFactory.getLogger(ActualWaterLevelServiceImpl.class);

    @Autowired
    private ActualWaterLevelDao actualWaterLevelDao;

    /**
     * {@inheritDoc}
     *
     * @see ActualWaterLevelService#getActualWaterLevels(String, LocalDate, int)
     */
    @Override
    public List<ActualWaterLevel> getActualWaterLevels(String name, LocalDate date, int period)
    {
        return actualWaterLevelDao.getActualWaterLevels(name, date.minusDays(period), date);
    }

    /**
     * <p>
     * Get actual water levels for given id and date period.
     * </p>
     *
     * @param id
     *            measuring point id
     * @param fromDate
     *            water level from date
     * @param toDate
     *            water level to date
     * 
     * @return list of actual water data levels
     */
    @Override
    public List<ActualWaterLevel> getActualWaterLevels(Long id, LocalDate fromDate, LocalDate toDate)
    {
        return actualWaterLevelDao.getActualWaterLevels(id, fromDate, toDate);
    }

    @Override
    public Map<LocalDate, Double> getActualWaterLevelsMap(String name, LocalDate date, int period)
    {
        Map<LocalDate, Double> waterLevelMap = new TreeMap<>();

        List<ActualWaterLevel> waterLevels =
            actualWaterLevelDao.getActualWaterLevels(name, date.minusDays(period), date.plusDays(1));

        Map<LocalDate, ActualWaterLevel> waterLevelByDatesMap =
            waterLevels.stream().collect(Collectors.toMap(ActualWaterLevel::getDate, Function.identity()));

        List<LocalDate> datesInBetween = getDurationInDays(date.minusDays(period), date.plusDays(1));

        log.debug("Total no of days during the dates : {}.", datesInBetween.size());

        datesInBetween.stream().forEach((thisDate) -> {
            if (waterLevelByDatesMap.containsKey(thisDate))
            {
                waterLevelMap.put(thisDate, waterLevelByDatesMap.get(thisDate).getWaterLevel());
            }
            else
                waterLevelMap.put(thisDate, 0.0D);
        });

        log.info("Actual water level Map, Name : {}, Data size :{}", name, waterLevelMap.size());
        return waterLevelMap;
    }
}
