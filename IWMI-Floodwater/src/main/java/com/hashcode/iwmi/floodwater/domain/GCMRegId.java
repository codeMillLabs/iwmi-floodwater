/*
 * Created on Nov 11, 2014
 * 
 * All sources, binaries and HTML pages (C) copyright 2014 by NextLabs, Inc.,
 * San Mateo CA, Ownership remains with NextLabs, Inc., All rights reserved
 * worldwide.
 *
 */
package com.hashcode.iwmi.floodwater.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * <p>
 * Model to hold the Google cloud Registration Id
 * </p>
 *
 *
 * @author Amila Silva
 * @email amilasilva88@gmail.com
 */
@Entity
@Table(name = "GCM_REG_ID")
public class GCMRegId extends BaseModel
{

    private static final long serialVersionUID = 8442656366236523850L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "reg_id", length = 4100)
    private String regId;

    /*
     * (non-Javadoc)
     * 
     * @see com.hashcode.iwmi.floodwater.domain.BaseModel#getId()
     */
    @Override
    public Long getId()
    {
        return id;
    }

    /**
     * <p>
     * Getter method for regId
     * </p>
     * 
     * @return the regId
     */
    public String getRegId()
    {
        return regId;
    }

    /**
     * <p>
     * Setter method for regId
     * </p>
     *
     * @param regId
     *            the regId to set
     */
    public void setRegId(String regId)
    {
        this.regId = regId;
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString()
    {
        return String.format("GCMRegId [id=%s, regId=%s]", id, regId);
    }

}
