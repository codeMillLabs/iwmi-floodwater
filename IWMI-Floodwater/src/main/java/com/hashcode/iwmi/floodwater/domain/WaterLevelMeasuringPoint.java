/*
 * FILENAME
 *     WaterLevelMeasuringPoint.java
 *
 * FILE LOCATION
 *     $Source$
 *
 * VERSION
 *     $Id$
 *         @version       $Revision$
 *         Check-Out Tag: $Name$
 *         Locked By:     $Lockers$
 *
 * FORMATTING NOTES
 *     * Lines should be limited to 78 characters.
 *     * Files should contain no tabs.
 *     * Indent code using four-character increments.
 *
 * COPYRIGHT
 *     Copyright (C) 2014 hashCode Solutions Pty. Ltd. All rights reserved.
 *     This software is the confidential and proprietary information of
 *     hashCode Solutions ("Confidential Information"). You shall not
 *     disclose such Confidential Information and shall use it only in
 *     accordance with the terms of the licence agreement you entered into
 *     with hashCode Solutions.
 */

package com.hashcode.iwmi.floodwater.domain;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinTable;
import javax.persistence.OneToMany;
import javax.persistence.Table;

//
// IMPORTS
// NOTE: Import specific classes without using wildcards.
//

/**
 * <p>
 * Water level measuring point domain object.
 * </p>
 * 
 * @author Amila Silva
 * @email amilasilva88@gmail.com
 **/
@Entity
@Table(name = "WATER_LEVEL_MEASURING_POINT")
public class WaterLevelMeasuringPoint extends BaseModel
{

    private static final long serialVersionUID = 1992795008668224238L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "measuring_point", unique = true)
    private String name;

    @Embedded
    private GeoCoordinate geoCoordinate;

    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinTable(name = "ACTUAL_WATER_LVL_READING_BY_POINT")
    private List<ActualWaterLevel> actualReadings = new ArrayList<>();

    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinTable(name = "PREDICTION_WATER_LVL_READING_BY_POINT")
    private List<PredictedWaterLevel> predictionReadings = new ArrayList<PredictedWaterLevel>();

    @Column(name = "moderate_level")
    private double moderateLevelThreshold;

    @Column(name = "risk_level")
    private double riskLevelThreshold;

    /**
     * <p>
     * Setting value for id.
     * </p>
     * 
     * @param id
     *            the id to set
     */
    public void setId(Long id)
    {
        this.id = id;
    }

    /**
     * {@inheritDoc}
     *
     * @see com.hashcode.iwmi.floodwater.domain.BaseModel#getId()
     */
    @Override
    public Long getId()
    {
        return id;
    }

    /**
     * <p>
     * Add actual water level reading to measuring point.
     * </p>
     *
     * @param actualWaterLevel
     *            actualWaterLevel
     *
     */
    public void addActualReading(ActualWaterLevel actualWaterLevel)
    {
        getActualReadings().add(actualWaterLevel);
        actualWaterLevel.setMeasuringPoint(this);
    }

    /**
     * <p>
     * Remove actual water level reading to measuring point.
     * </p>
     *
     * @param actualWaterLevel
     *            actualWaterLevel
     */
    public void removeActualReading(ActualWaterLevel actualWaterLevel)
    {
        getActualReadings().remove(actualWaterLevel);
        actualWaterLevel.setMeasuringPoint(null);
    }

    /**
     * <p>
     * Add predicted water level reading to measuring point.
     * </p>
     *
     * @param predictedWaterLevel
     *            predictedWaterLevel
     *
     */
    public void addPredictedReading(PredictedWaterLevel predictedWaterLevel)
    {
        getPredictionReadings().add(predictedWaterLevel);
        predictedWaterLevel.setMeasuringPoint(this);
    }

    /**
     * <p>
     * Remove predicted water level reading to measuring point.
     * </p>
     *
     * @param predictedWaterLevel
     *            predictedWaterLevel
     *
     */
    public void removePredictedReading(PredictedWaterLevel predictedWaterLevel)
    {
        getPredictionReadings().remove(predictedWaterLevel);
        predictedWaterLevel.setMeasuringPoint(null);
    }

    /**
     * <p>
     * Getter for name.
     * </p>
     * 
     * @return the name
     */
    public String getName()
    {
        return name;
    }

    /**
     * <p>
     * Setting value for name.
     * </p>
     * 
     * @param name
     *            the name to set
     */
    public void setName(String name)
    {
        this.name = name;
    }

    /**
     * <p>
     * Getter for geoCoordinate.
     * </p>
     * 
     * @return the geoCoordinate
     */
    public GeoCoordinate getGeoCoordinate()
    {
        return geoCoordinate;
    }

    /**
     * <p>
     * Setting value for geoCoordinate.
     * </p>
     * 
     * @param geoCoordinate
     *            the geoCoordinate to set
     */
    public void setGeoCoordinate(GeoCoordinate geoCoordinate)
    {
        this.geoCoordinate = geoCoordinate;
    }

    /**
     * <p>
     * Getter for actualReadings.
     * </p>
     * 
     * @return the actualReadings
     */
    public List<ActualWaterLevel> getActualReadings()
    {
        return actualReadings;
    }

    /**
     * <p>
     * Setting value for actualReadings.
     * </p>
     * 
     * @param actualReadings
     *            the actualReadings to set
     */
    public void setActualReadings(List<ActualWaterLevel> actualReadings)
    {
        this.actualReadings = actualReadings;
    }

    /**
     * <p>
     * Getter for predictionReadings.
     * </p>
     * 
     * @return the predictionReadings
     */
    public List<PredictedWaterLevel> getPredictionReadings()
    {
        return predictionReadings;
    }

    /**
     * <p>
     * Setting value for predictionReadings.
     * </p>
     * 
     * @param predictionReadings
     *            the predictionReadings to set
     */
    public void setPredictionReadings(List<PredictedWaterLevel> predictionReadings)
    {
        this.predictionReadings = predictionReadings;
    }

    /**
     * <p>
     * Getter for moderateLevelThreshold.
     * </p>
     * 
     * @return the moderateLevelThreshold
     */
    public double getModerateLevelThreshold()
    {
        return moderateLevelThreshold;
    }

    /**
     * <p>
     * Setting value for moderateLevelThreshold.
     * </p>
     * 
     * @param moderateLevelThreshold
     *            the moderateLevelThreshold to set
     */
    public void setModerateLevelThreshold(double moderateLevelThreshold)
    {
        this.moderateLevelThreshold = moderateLevelThreshold;
    }

    /**
     * <p>
     * Getter for riskLevelThreshold.
     * </p>
     * 
     * @return the riskLevelThreshold
     */
    public double getRiskLevelThreshold()
    {
        return riskLevelThreshold;
    }

    /**
     * <p>
     * Setting value for riskLevelThreshold.
     * </p>
     * 
     * @param riskLevelThreshold
     *            the riskLevelThreshold to set
     */
    public void setRiskLevelThreshold(double riskLevelThreshold)
    {
        this.riskLevelThreshold = riskLevelThreshold;
    }

    /**
     * {@inheritDoc}
     *
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString()
    {
        return String
            .format(
                "WaterLevelMeasuringPoint [id=%s, name=%s, geoCoordinate=%s, actualReadings=%s, predictionReadings=%s, moderateLevelThreshold=%s, riskLevelThreshold=%s]",
                id, name, geoCoordinate, actualReadings, predictionReadings, moderateLevelThreshold, riskLevelThreshold);
    }

}
