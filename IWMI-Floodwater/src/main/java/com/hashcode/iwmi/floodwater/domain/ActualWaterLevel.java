/*
 * FILENAME
 *     ActualWaterLevel.java
 *
 * FILE LOCATION
 *     $Source$
 *
 * VERSION
 *     $Id$
 *         @version       $Revision$
 *         Check-Out Tag: $Name$
 *         Locked By:     $Lockers$
 *
 * FORMATTING NOTES
 *     * Lines should be limited to 78 characters.
 *     * Files should contain no tabs.
 *     * Indent code using four-character increments.
 *
 * COPYRIGHT
 *     Copyright (C) 2014 hashCode Solutions Pty. Ltd. All rights reserved.
 *     This software is the confidential and proprietary information of
 *     hashCode Solutions ("Confidential Information"). You shall not
 *     disclose such Confidential Information and shall use it only in
 *     accordance with the terms of the licence agreement you entered into
 *     with hashCode Solutions.
 */
package com.hashcode.iwmi.floodwater.domain;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * <p>
 * Domain class for Actual Water Level
 * </p>
 *
 *
 * @author Amila Silva
 * @email amilasilva88@gmail.com
 *
 */
@Entity
@Table(name = "ACTUAL_WATER_LEVEL")
public class ActualWaterLevel extends WaterLevel
{

    private static final long serialVersionUID = -6379882402451980317L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    /**
     * 
     * {@inheritDoc}
     *
     * @see com.hashcode.iwmi.floodwater.domain.BaseModel#getId()
     */
    @Override
    public Long getId()
    {
        return id;
    }

    /**
     * {@inheritDoc}
     *
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString()
    {
        return String.format("ActualWaterLevel [id=%s, getWaterLevel()=%s, getMeasuringPoint()=%s, getDate()=%s]", id,
            getWaterLevel(), getMeasuringPoint(), getDate());
    }

}
