/*
 * FILENAME
 *     AuthenticationFilter.java
 *
 * FILE LOCATION
 *     $Source$
 *
 * VERSION
 *     $Id$
 *         @version       $Revision$
 *         Check-Out Tag: $Name$
 *         Locked By:     $Lockers$
 *
 * FORMATTING NOTES
 *     * Lines should be limited to 78 characters.
 *     * Files should contain no tabs.
 *     * Indent code using four-character increments.
 *
 * COPYRIGHT
 *     Copyright (C) 2014 hashCode Solutions Pty. Ltd. All rights reserved.
 *     This software is the confidential and proprietary information of
 *     hashCode Solutions ("Confidential Information"). You shall not
 *     disclose such Confidential Information and shall use it only in
 *     accordance with the terms of the licence agreement you entered into
 *     with hashCode Solutions.
 */

package com.hashcode.iwmi.floodwater.filter;



import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


//
// IMPORTS
// NOTE: Import specific classes without using wildcards.
//

/**
 * <p>
 * AuthenticationFilter filter for web requests.
 * </p>
 *
 *
 * @author Amila Silva
 *
 * @version $Id$
 **/
@WebFilter("/*")
public class AuthenticationFilter implements Filter
{
    private static final Logger log = LoggerFactory.getLogger(AuthenticationFilter.class);
    private static final List<String> skipContentList = Arrays.asList("/kml/", "/authenticate", "/login",
        "/javascript/", "/images/", "signin.jsp", "index.jsp");
    
    private ServletContext context;
    
    /**
     * {@inheritDoc}
     *
     * @see javax.servlet.Filter#doFilter(javax.servlet.ServletRequest, javax.servlet.ServletResponse, javax.servlet.FilterChain)
     */
    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException,
        ServletException
    {
        HttpServletRequest req = (HttpServletRequest) request;
        HttpServletResponse res = (HttpServletResponse) response;
         
        String uri = req.getRequestURI();
         
        HttpSession session = req.getSession(false);
         
        if(hasSessionOrToken(session) && skipCheck(uri)){
            log.info("Unauthorized access request, URL : {}", uri);
            res.sendRedirect("./signin.jsp");
        }else{
            // pass the request along the filter chain
            chain.doFilter(request, response);
        }
    }

    private boolean skipCheck(String uri) {
       return !skipContentList.stream().anyMatch((n) -> uri.contains(n));
    }
   
    private boolean hasSessionOrToken(HttpSession session)
    {
        return session == null || session.getAttribute("TOKEN") == null;
    }

    /**
     * {@inheritDoc}
     *
     * @see javax.servlet.Filter#init(javax.servlet.FilterConfig)
     */
    @Override
    public void init(FilterConfig config) throws ServletException
    {
        this.context = config.getServletContext();
        this.context.log("AuthenticationFilter initialized");
    }
    
    /**
     * {@inheritDoc}
     *
     * @see javax.servlet.Filter#destroy()
     */
    @Override
    public void destroy()
    {

    }

}
