/*
 * FILENAME
 *     ResponseStatus.java
 *
 * FILE LOCATION
 *     $Source$
 *
 * VERSION
 *     $Id$
 *         @version       $Revision$
 *         Check-Out Tag: $Name$
 *         Locked By:     $Lockers$
 *
 * FORMATTING NOTES
 *     * Lines should be limited to 78 characters.
 *     * Files should contain no tabs.
 *     * Indent code using four-character increments.
 *
 * COPYRIGHT
 *     Copyright (C) 2014 hashCode Solutions Pty. Ltd. All rights reserved.
 *     This software is the confidential and proprietary information of
 *     hashCode Solutions ("Confidential Information"). You shall not
 *     disclose such Confidential Information and shall use it only in
 *     accordance with the terms of the licence agreement you entered into
 *     with hashCode Solutions.
 */

package com.hashcode.iwmi.floodwater.util;

//
// IMPORTS
// NOTE: Import specific classes without using wildcards.
//
/**
 * 
 * <p>
 * Response status manage from here.
 * </p>
 * 
 *
 * @author Amila Silva
 * @email amilasilva88@gmail.com
 *
 */
public enum ResponseStatus
{
    SUCCESS("1000", "Succes"),
    FAILED("5000", "Failed"),
    ZERO_DATA("2001", "No data available");

    private String code;
    private String msg;

    /**
     * <p>
     * Constructor for ResponseStatus.
     * </p>
     *
     * @param code
     * @param msg
     *
     */
    private ResponseStatus(String code, String msg)
    {
        this.code = code;
        this.msg = msg;
    }

    /**
     * <p>
     * Getter for code.
     * </p>
     * 
     * @return the code
     */
    public String getCode()
    {
        return code;
    }

    /**
     * <p>
     * Getter for msg.
     * </p>
     * 
     * @return the msg
     */
    public String getMsg()
    {
        return msg;
    }

}
