/*
 * FILENAME
 *     AuthenticationController.java
 *
 * FILE LOCATION
 *     $Source$
 *
 * VERSION
 *     $Id$
 *         @version       $Revision$
 *         Check-Out Tag: $Name$
 *         Locked By:     $Lockers$
 *
 * FORMATTING NOTES
 *     * Lines should be limited to 78 characters.
 *     * Files should contain no tabs.
 *     * Indent code using four-character increments.
 *
 * COPYRIGHT
 *     Copyright (C) 2014 hashCode Solutions Pty. Ltd. All rights reserved.
 *     This software is the confidential and proprietary information of
 *     hashCode Solutions ("Confidential Information"). You shall not
 *     disclose such Confidential Information and shall use it only in
 *     accordance with the terms of the licence agreement you entered into
 *     with hashCode Solutions.
 */

package com.hashcode.iwmi.floodwater.controllers;

import java.security.NoSuchAlgorithmException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.hashcode.iwmi.floodwater.domain.User;
import com.hashcode.iwmi.floodwater.services.UserService;
import com.hashcode.iwmi.floodwater.util.ResponseStatus;

//
// IMPORTS
// NOTE: Import specific classes without using wildcards.
//

/**
 * <p>
 * Controller to handle the user Authentication.
 * </p>
 *
 *
 * @author Amila Silva
 *
 * @version $Id$
 **/
@Controller
public class AuthenticationController {
	private static final Logger log = LoggerFactory
			.getLogger(AuthenticationController.class);
	public static final String TOKEN = "TOKEN";
	public static final String CURRENT_USER = "CURRENT_USER";
	public static final String USER_NAME = "USER_NAME";

	@Autowired
	private UserService userService;

	@RequestMapping(value = "/login", method = { RequestMethod.GET })
	public String authenticateLogin(HttpServletRequest request, Model model) {
		return "signin";
	}

	@RequestMapping(value = "/authenticate", method = { RequestMethod.POST,
			RequestMethod.PUT })
	@ResponseBody
	public String authenticateLogin(HttpServletRequest request,
			@RequestBody String requestBody, Model model)
			throws NoSuchAlgorithmException {
		log.info("Request came to authenticate login :" + requestBody);
		JSONObject reqJson = new JSONObject(requestBody);
		String email = reqJson.getString("email");
		String password = reqJson.getString("password");

		User user = userService.authenticateUser(email, password);

		JSONObject responseObj = new JSONObject();

		if (user != null) {

			HttpSession session = request.getSession(true);
			session.setAttribute(TOKEN, email);
			session.setAttribute(CURRENT_USER, user);
			session.setAttribute(USER_NAME, user.getName());

			responseObj.put("status", ResponseStatus.SUCCESS.getCode());
			responseObj.put("message", ResponseStatus.SUCCESS.getMsg());
			responseObj.put("token", email);
			log.info("User login successfull, [ email :{} ]", email);

		} else {
			responseObj.put("status", ResponseStatus.FAILED.getCode());
			responseObj.put("message", ResponseStatus.FAILED.getMsg());
			log.info("User login failed, [ email :{} ]", email);
		}
		return responseObj.toString();
	}

	@RequestMapping(value = "/index", method = { RequestMethod.GET,
			RequestMethod.POST })
	public String index(HttpServletRequest request, Model model) {
		return "index";
	}

	@RequestMapping(value = "/", method = { RequestMethod.GET,
			RequestMethod.POST })
	public String defaultHome(HttpServletRequest request, Model model) {
		return "index";
	}

	@RequestMapping(value = "/logout", method = { RequestMethod.GET })
	@ResponseBody
	public String logout(HttpServletRequest request,
			HttpServletResponse response, Model model) {
		log.info("Request came to logout....");

		// clear the session
		request.getSession(false).invalidate();

		JSONObject responseObj = new JSONObject();
		responseObj.put("status", ResponseStatus.SUCCESS.getCode());
		responseObj.put("message", ResponseStatus.SUCCESS.getMsg());

		return responseObj.toString();
	}

}
