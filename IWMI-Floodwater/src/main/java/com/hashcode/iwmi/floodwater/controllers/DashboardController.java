/*
 * FILENAME
 *     DashboardController.java
 *
 * FILE LOCATION
 *     $Source$
 *
 * VERSION
 *     $Id$
 *         @version       $Revision$
 *         Check-Out Tag: $Name$
 *         Locked By:     $Lockers$
 *
 * FORMATTING NOTES
 *     * Lines should be limited to 78 characters.
 *     * Files should contain no tabs.
 *     * Indent code using four-character increments.
 *
 * COPYRIGHT
 *     Copyright (C) 2014 hashCode Solutions Pty. Ltd. All rights reserved.
 *     This software is the confidential and proprietary information of
 *     hashCode Solutions ("Confidential Information"). You shall not
 *     disclose such Confidential Information and shall use it only in
 *     accordance with the terms of the licence agreement you entered into
 *     with hashCode Solutions.
 */

package com.hashcode.iwmi.floodwater.controllers;

import static com.hashcode.iwmi.floodwater.util.FloodwaterUtils.getDurationInDays;

import java.time.LocalDate;
import java.time.Period;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

import javax.transaction.Transactional;

import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.hashcode.iwmi.floodwater.dao.ActualWaterLevelDao;
import com.hashcode.iwmi.floodwater.dao.PredictedWaterLevelDao;
import com.hashcode.iwmi.floodwater.dao.WaterLevelMeasuringPointDao;
import com.hashcode.iwmi.floodwater.domain.ActualWaterLevel;
import com.hashcode.iwmi.floodwater.domain.PredictedWaterLevel;
import com.hashcode.iwmi.floodwater.domain.WaterLevelMeasuringPoint;
import com.hashcode.iwmi.floodwater.model.AnnualWaterLevel;

//
// IMPORTS
// NOTE: Import specific classes without using wildcards.
//

/**
 * <p>
 * Dashboard functions handles here.
 * </p>
 *
 * @author Amila Silva
 * @email amilasilva88@gmail.com
 **/
@Controller
@Transactional
public class DashboardController
{
    private static final Logger log = LoggerFactory.getLogger(DashboardController.class);

    @Autowired
    private ActualWaterLevelDao actualWaterLevelDao;

    @Autowired
    private PredictedWaterLevelDao predictedWaterLevelDao;

    @Autowired
    private WaterLevelMeasuringPointDao measuringPointsDao;

    /**
     * <p>
     * Fetch data for today's water level chart.
     * </p>
     *
     * @return data string in JSON format
     */
    @RequestMapping(value = "/todayWaterLevels", method = {
        RequestMethod.POST
    })
    @ResponseBody
    public String todayWaterLevels(@RequestBody String request)
    {
        log.info("Request came to fetch today's water levels, Request : {}", request);

        JSONObject requestJSON = new JSONObject(request);
        LocalDate today = LocalDate.parse(requestJSON.getString("date"));

        Collection<ActualWaterLevel> actualWaterLevels =
            actualWaterLevelDao.getActualWaterLevels("%", today, today.plusDays(1));

        JSONObject chartData = new JSONObject();
        JSONArray dataArr = new JSONArray();
        chartData.put("key", "");
        chartData.put("values", dataArr);

        actualWaterLevels.stream()
            .sorted((l1, l2) -> l1.getMeasuringPoint().getName().compareTo(l2.getMeasuringPoint().getName()))
            .forEach((n) -> {
                JSONObject data = new JSONObject();
                data.put("key", n.getMeasuringPoint().getName());
                data.put("value", n.getWaterLevel());

                dataArr.put(data);
            });

        JSONArray chartDataArr = new JSONArray();
        chartDataArr.put(chartData);
        log.info("Today Water Level chart data populated, Response : {}", chartDataArr.toString());
        return chartDataArr.toString();
    }

    /**
     * <p>
     * Fetch data for tomorrows's water level chart.
     * </p>
     *
     * @return data string in JSON format
     */
    @RequestMapping(value = "/tomorrowWaterLevels", method = {
        RequestMethod.POST
    })
    @ResponseBody
    public String tomorrowWaterLevels(@RequestBody String request)
    {
        log.info("Request came to fetch tomorrows's water levels, Request : {}", request);

        JSONObject requestJSON = new JSONObject(request);
        LocalDate date = LocalDate.parse(requestJSON.getString("date"));

        Collection<PredictedWaterLevel> predictedWaterLevels =
            predictedWaterLevelDao.getPredictedWaterLevels("%", date.plusDays(1), date.plusDays(2));

        JSONObject chartData = new JSONObject();
        JSONArray dataArr = new JSONArray();
        chartData.put("key", "");
        chartData.put("values", dataArr);

        predictedWaterLevels.stream()
            .sorted((l1, l2) -> l1.getMeasuringPoint().getName().compareTo(l2.getMeasuringPoint().getName()))
            .forEach((n) -> {
                JSONObject data = new JSONObject();
                data.put("key", n.getMeasuringPoint().getName());
                data.put("value", n.getWaterLevel());

                dataArr.put(data);
            });

        JSONArray chartDataArr = new JSONArray();
        chartDataArr.put(chartData);
        log.info("Tomorrow's Water Level chart data populated, Response : {}", chartDataArr.toString());
        return chartDataArr.toString();
    }

    // /**
    // * <p>
    // * Fetch data for last 30 days water level chart.
    // * </p>
    // *
    // * @return data string in JSON format
    // */
    // @RequestMapping(value = "/last30DaysWaterLevels", method = {
    // RequestMethod.POST })
    // @ResponseBody
    // public String last30DaysWaterLevels(@RequestBody String request) {
    // log.info("Request came to fetch today's water levels, Request : {}",
    // request);
    //
    // JSONObject requestJSON = new JSONObject(request);
    // LocalDate today = LocalDate.parse(requestJSON.getString("date"));
    //
    // List<WaterLevelMeasuringPoint> measuringPoints = measuringPointsDao
    // .findAll();
    //
    // JSONArray chartDataArr = new JSONArray();
    //
    // measuringPoints
    // .stream()
    // .forEach(
    // (point) -> {
    // Collection<ActualWaterLevel> actualWaterLevels =
    // populateMissingEntriesForActualTrend(
    // point.getName(), today.minusDays(30),
    // today.plusDays(1));
    //
    // JSONObject chartData = new JSONObject();
    // JSONArray dataArr = new JSONArray();
    // chartData.put("key", point.getName());
    // chartData.put("values", dataArr);
    //
    // actualWaterLevels
    // .stream()
    // .sorted((l1, l2) -> l1.getDate().compareTo(
    // l2.getDate()))
    // .forEach(
    // (level) -> {
    // long milis = level
    // .getDate()
    // .atStartOfDay()
    // .atZone(ZoneId
    // .systemDefault())
    // .toInstant()
    // .toEpochMilli();
    // JSONArray valueArr = new JSONArray();
    // valueArr.put(milis);
    // valueArr.put(level
    // .getWaterLevel());
    // dataArr.put(valueArr);
    // });
    //
    // chartDataArr.put(chartData);
    // });
    // log.info(
    // "Last 30 days Water Level chart data populated, Response : {}",
    // chartDataArr.length());
    // return chartDataArr.toString();
    // }

    // /**
    // * <p>
    // * Fetch data for Next 5 Days water level chart.
    // * </p>
    // *
    // * @return data string in JSON format
    // */
    // @RequestMapping(value = "/next5DaysWaterLevels", method = {
    // RequestMethod.POST })
    // @ResponseBody
    // public String next5DaysWaterLevels(@RequestBody String request) {
    // log.info("Request came to fetch today's water levels, Request : {}",
    // request);
    //
    // JSONObject requestJSON = new JSONObject(request);
    // LocalDate today = LocalDate.parse(requestJSON.getString("date"));
    //
    // List<WaterLevelMeasuringPoint> measuringPoints = measuringPointsDao
    // .findAll();
    //
    // JSONArray chartDataArr = new JSONArray();
    //
    // measuringPoints
    // .stream()
    // .forEach(
    // (point) -> {
    // Collection<PredictedWaterLevel> predictedWaterLevels =
    // populateMissingEntriesForPredictedTrend(
    // point.getName(), today.plusDays(1),
    // today.plusDays(6));
    //
    // JSONObject chartData = new JSONObject();
    // JSONArray dataArr = new JSONArray();
    // chartData.put("key", point.getName());
    // chartData.put("values", dataArr);
    //
    // predictedWaterLevels
    // .stream()
    // .sorted((l1, l2) -> l1.getDate().compareTo(
    // l2.getDate()))
    // .forEach(
    // (level) -> {
    // long milis = level
    // .getDate()
    // .atStartOfDay()
    // .atZone(ZoneId
    // .systemDefault())
    // .toInstant()
    // .toEpochMilli();
    // JSONArray valueArr = new JSONArray();
    // valueArr.put(milis);
    // valueArr.put(level
    // .getWaterLevel());
    // dataArr.put(valueArr);
    // });
    //
    // chartDataArr.put(chartData);
    // });
    // log.info("Next 5 Water Level chart data populated, Response : {}",
    // chartDataArr.length());
    // return chartDataArr.toString();
    // }

    /**
     * <p>
     * Fetch data for last 30 days water level chart.
     * </p>
     *
     * @return data string in JSON format
     */
    @RequestMapping(value = "/last30DaysWaterLevels", method = {
        RequestMethod.POST
    })
    @ResponseBody
    public String last30DaysWaterLevels(@RequestBody String request)
    {
        log.info("Request came to fetch today's water levels, Request : {}", request);

        DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern("MM-dd");

        JSONObject requestJSON = new JSONObject(request);
        LocalDate today = LocalDate.parse(requestJSON.getString("date"));

        JSONArray columnsArr = new JSONArray();

        JSONArray xAxisTickersArr = new JSONArray();
        xAxisTickersArr.put("x");

        for (int i = -30; i < 1; i++)
        {
            xAxisTickersArr.put(today.plusDays(i).format(dateFormatter));
        }
        columnsArr.put(xAxisTickersArr);

        List<WaterLevelMeasuringPoint> measuringPoints = measuringPointsDao.findAll();

        measuringPoints.stream().forEach(
            (point) -> {
                Collection<ActualWaterLevel> actualWaterLevels =
                    populateMissingEntriesForActualTrend(point.getName(), today.minusDays(30), today.plusDays(1));

                JSONArray seriesDataArr = new JSONArray();
                seriesDataArr.put(point.getName());

                actualWaterLevels.stream().sorted((l1, l2) -> l1.getDate().compareTo(l2.getDate()))
                    .forEach((level) -> {
                        seriesDataArr.put(level.getWaterLevel());
                    });

                columnsArr.put(seriesDataArr);
            });
        log.debug("Last 30 days Water Level chart data populated, Response : {}", columnsArr.length());
        return columnsArr.toString();
    }

    /**
     * <p>
     * Fetch data for Next 5 Days water level chart.
     * </p>
     *
     * @return data string in JSON format
     */
    @RequestMapping(value = "/next5DaysWaterLevels", method = {
        RequestMethod.POST
    })
    @ResponseBody
    public String next5DaysWaterLevels(@RequestBody String request)
    {
        log.info("Request came to fetch today's water levels, Request : {}", request);
        DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern("MM-dd");

        JSONObject requestJSON = new JSONObject(request);
        LocalDate today = LocalDate.parse(requestJSON.getString("date"));

        List<WaterLevelMeasuringPoint> measuringPoints = measuringPointsDao.findAll();

        JSONArray columnsArr = new JSONArray();

        JSONArray xAxisTickersArr = new JSONArray();
        xAxisTickersArr.put("x");

        for (int i = 1; i < 6; i++)
        {
            xAxisTickersArr.put(today.plusDays(i).format(dateFormatter));
        }
        columnsArr.put(xAxisTickersArr);

        measuringPoints.stream().forEach(
            (point) -> {
                Collection<PredictedWaterLevel> predictedWaterLevels =
                    populateMissingEntriesForPredictedTrend(point.getName(), today.plusDays(1), today.plusDays(6));

                JSONArray seriesDataArr = new JSONArray();
                seriesDataArr.put(point.getName());

                predictedWaterLevels.stream().sorted((l1, l2) -> l1.getDate().compareTo(l2.getDate()))
                    .forEach((level) -> {
                        seriesDataArr.put(level.getWaterLevel());
                    });

                columnsArr.put(seriesDataArr);
            });
        log.debug("Next 5 Water Level chart data populated, Response : {}", columnsArr.length());
        return columnsArr.toString();
    }

    /**
     * <p>
     * Fetch data for full water level history.
     * </p>
     *
     * @return data string in JSON format
     */
    @RequestMapping(value = "/annualWaterDataHistory", method = {
        RequestMethod.POST
    })
    @ResponseBody
    public String annualWaterDataHistory(@RequestBody String request)
    {
        log.info("Request came to fetch annual water level history, Request : {}", request);

        JSONObject requestJSON = new JSONObject(request);
        LocalDate today = LocalDate.parse(requestJSON.getString("date"));

        List<WaterLevelMeasuringPoint> measuringPoints = measuringPointsDao.findAll();

        JSONArray columnsArr = new JSONArray();

        LocalDate startYear = LocalDate.of(2002, 1, 1);
        Period period = Period.between(startYear, today);
        int years = period.getYears() + 1;

        JSONArray xAxisTickersArr = new JSONArray();
        xAxisTickersArr.put("x");

        for (int i = 0; i <= years; i++)
        {
            xAxisTickersArr.put(startYear.getYear() + i);
        }
        columnsArr.put(xAxisTickersArr);

        measuringPoints.stream().forEach((point) -> {
            List<AnnualWaterLevel> pointsAnnualData = populateMissingYearsData(point, startYear, years);

            JSONArray seriesDataArr = new JSONArray();
            seriesDataArr.put(point.getName());

            JSONObject chartData = new JSONObject();
            JSONArray dataArr = new JSONArray();
            chartData.put("key", point.getName());
            chartData.put("values", dataArr);

            pointsAnnualData.stream().forEach(n -> {
                seriesDataArr.put(n.getWaterLevel());
            });

            columnsArr.put(seriesDataArr);
        });
        log.info("Annuall Water Level chart data populated, Response : {}", columnsArr.length());

        return columnsArr.toString();
    }

    private List<AnnualWaterLevel> populateMissingYearsData(WaterLevelMeasuringPoint point, LocalDate startYear,
        int noOfYears)
    {

        List<AnnualWaterLevel> annualAvgData = new ArrayList<AnnualWaterLevel>();
        Map<Integer, AnnualWaterLevel> pointsAnnualData =
            actualWaterLevelDao.getAnnualAvgWaterLevelToDate(startYear, point);

        for (int i = 0; i < noOfYears; i++)
        {
            int thisYear = startYear.plusYears(i).getYear();
            AnnualWaterLevel annualAvg =
                pointsAnnualData.getOrDefault(thisYear, new AnnualWaterLevel(point.getName(), thisYear, 0.0d));

            annualAvgData.add(annualAvg);
        }

        return annualAvgData;
    }

    private Collection<PredictedWaterLevel> populateMissingEntriesForPredictedTrend(String pointName,
        LocalDate fromTime, LocalDate toTime)
    {
        List<PredictedWaterLevel> waterLevels =
            predictedWaterLevelDao.getPredictedWaterLevels(pointName, fromTime, toTime);
        WaterLevelMeasuringPoint measuringPoint = measuringPointsDao.getMeasuringPointByName(pointName);

        Map<LocalDate, PredictedWaterLevel> waterLevelByDatesMap =
            waterLevels.stream().collect(Collectors.toMap(PredictedWaterLevel::getDate, Function.identity()));

        List<LocalDate> datesInBetween = getDurationInDays(fromTime, toTime);

        log.debug("Total no of days during the dates : {}.", datesInBetween.size());

        datesInBetween.stream().forEach((thisDate) -> {
            if (!waterLevelByDatesMap.containsKey(thisDate))
            {
                PredictedWaterLevel waterLvl = new PredictedWaterLevel();
                waterLvl.setDate(thisDate);
                waterLvl.setMeasuringPoint(measuringPoint);
                waterLvl.setWaterLevel(0.0);

                waterLevelByDatesMap.put(thisDate, waterLvl);
            }
        });

        return waterLevelByDatesMap.values();
    }

    private Collection<ActualWaterLevel> populateMissingEntriesForActualTrend(String pointName, LocalDate fromTime,
        LocalDate toTime)
    {
        log.info("Actual Trend Dates : From Date :{}, TO Date :{}.", fromTime, toTime);

        List<ActualWaterLevel> waterLevels = actualWaterLevelDao.getActualWaterLevels(pointName, fromTime, toTime);
        WaterLevelMeasuringPoint measuringPoint = measuringPointsDao.getMeasuringPointByName(pointName);

        Map<LocalDate, ActualWaterLevel> waterLevelByDatesMap =
            waterLevels.stream().collect(Collectors.toMap(ActualWaterLevel::getDate, Function.identity()));

        List<LocalDate> datesInBetween = getDurationInDays(fromTime, toTime);

        log.debug("Total no of days during the dates : {}.", datesInBetween.size());

        datesInBetween.stream().forEach((thisDate) -> {
            if (!waterLevelByDatesMap.containsKey(thisDate))
            {
                ActualWaterLevel waterLvl = new ActualWaterLevel();
                waterLvl.setDate(thisDate);
                waterLvl.setMeasuringPoint(measuringPoint);
                waterLvl.setWaterLevel(0.0);

                waterLevelByDatesMap.put(thisDate, waterLvl);
            }
        });

        return waterLevelByDatesMap.values();
    }

}
