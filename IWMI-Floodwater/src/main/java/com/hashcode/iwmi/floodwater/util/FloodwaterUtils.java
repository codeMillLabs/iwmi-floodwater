/*
 * FILENAME
 *     FloodwaterUtils.java
 *
 * FILE LOCATION
 *     $Source$
 *
 * VERSION
 *     $Id$
 *         @version       $Revision$
 *         Check-Out Tag: $Name$
 *         Locked By:     $Lockers$
 *
 * FORMATTING NOTES
 *     * Lines should be limited to 78 characters.
 *     * Files should contain no tabs.
 *     * Indent code using four-character increments.
 *
 * COPYRIGHT
 *     Copyright (C) 2014 hashCode Solutions Pty. Ltd. All rights reserved.
 *     This software is the confidential and proprietary information of
 *     hashCode Solutions ("Confidential Information"). You shall not
 *     disclose such Confidential Information and shall use it only in
 *     accordance with the terms of the licence agreement you entered into
 *     with hashCode Solutions.
 */

package com.hashcode.iwmi.floodwater.util;

import java.io.File;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

//
// IMPORTS
// NOTE: Import specific classes without using wildcards.
//

/**
 * <p>
 * Utility class for Application wide utilities.
 * </p>
 *
 * @author Amila Silva
 * @email amilasilva88@gmail.com
 **/
public final class FloodwaterUtils
{
    private static final Logger log = LoggerFactory.getLogger(FloodwaterUtils.class);
    private static final String ENCRYPTION_ALGORITHM = "MD5";

    /**
     * <p>
     * Get Last modified date from a given file.
     * </p>
     *
     * @param filePath
     *            file path
     * @return last modified date
     *
     */
    public static LocalDateTime getLastModifiedDate(String filePath)
    {
        try
        {
            File file = new File(filePath);
            LocalDateTime dateTime =
                Instant.ofEpochMilli(file.lastModified()).atZone(ZoneId.systemDefault()).toLocalDateTime();
            return dateTime;
        }
        catch (Throwable e)
        {
            log.warn("Error occurred in reading the lastmodified date from file", e);
            return LocalDateTime.now(ZoneId.systemDefault());
        }
    }

    /**
     * <p>
     * Check file is modified recently.
     * </p>
     *
     * @param cachedMetaDateTime
     *            in memory data last modified date time
     * @param fileLastModifiedDateTime
     *            actual file last modified data time
     * @return
     *
     */
    public static boolean isModified(LocalDateTime cachedMetaDateTime, LocalDateTime fileLastModifiedDateTime)
    {
        return cachedMetaDateTime.isBefore(fileLastModifiedDateTime);
    }

    /**
     * <p>
     * Check given string is not null or empty.
     * </p>
     * 
     *
     * @param value
     *            value to check empty or null
     * @return false if empty or null, otherwise true
     *
     */
    public static boolean isNotNullOrEmpty(String value)
    {
        return (value == null || value.isEmpty()) ? false : true;
    }

    /**
     * 
     * <p>
     * Create a JSON response from given parameters.
     * </p>
     *
     * @param status
     *            status
     * @param values
     *            map of values
     * @return {@link JSONObject}
     *
     */
    public static JSONObject createResponse(ResponseStatus status, Map<String, Object> values)
    {
        JSONObject jsonResponse = new JSONObject();
        jsonResponse.put("status", status.getCode());
        jsonResponse.put("message", status.getMsg());

        if (values != null)
            values.forEach((key, value) -> {
                jsonResponse.put(key, value);
            });
        return jsonResponse;
    }

    /**
     * <p>
     * Get responce data map.
     * </p>
     *
     *
     * @return Map<String, Object>
     *
     */
    public static Map<String, Object> getResponceDataMap()
    {
        Map<String, Object> params = new HashMap<>();
        return params;
    }

    /**
     * <p>
     * Get Period between given dates.
     * </p>
     *
     *
     * @param fromDate
     *            fromDate
     * @param toDate
     *            toDate
     * @return Period
     *
     */
    public static List<LocalDate> getDurationInDays(LocalDate fromDate, LocalDate toDate)
    {
        List<LocalDate> dates = new ArrayList<>();
        long days = ChronoUnit.DAYS.between(fromDate, toDate);

        for (int i = 0; i < days; i++)
        {
            dates.add(fromDate.plusDays(i));
        }

        return dates;
    }

    /**
     * Encrypt password.
     * 
     * @param password
     *            password to encrypt
     * 
     * @return encrypted password
     * 
     * @throws NoSuchAlgorithmException
     *             throws when no such algorithm
     */
    public static String encrypt(final String password) throws NoSuchAlgorithmException
    {
        MessageDigest md = MessageDigest.getInstance(ENCRYPTION_ALGORITHM);
        md.update(password.getBytes());

        byte[] byteData = md.digest();

        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < byteData.length; i++)
        {
            sb.append(Integer.toString((byteData[i] & 0xff) + 0x100, 16).substring(1));
        }

        StringBuffer hexString = new StringBuffer();
        for (int i = 0; i < byteData.length; i++)
        {
            String hex = Integer.toHexString(0xff & byteData[i]);
            if (hex.length() == 1)
                hexString.append('0');
            hexString.append(hex);
        }

        return hexString.toString();
    }

}
