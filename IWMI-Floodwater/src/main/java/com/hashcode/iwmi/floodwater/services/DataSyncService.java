/*
 * FILENAME
 *     DataSyncService.java
 *
 * FILE LOCATION
 *     $Source$
 *
 * VERSION
 *     $Id$
 *         @version       $Revision$
 *         Check-Out Tag: $Name$
 *         Locked By:     $Lockers$
 *
 * FORMATTING NOTES
 *     * Lines should be limited to 78 characters.
 *     * Files should contain no tabs.
 *     * Indent code using four-character increments.
 *
 * COPYRIGHT
 *     Copyright (C) 2014 hashCode Solutions Pty. Ltd. All rights reserved.
 *     This software is the confidential and proprietary information of
 *     hashCode Solutions ("Confidential Information"). You shall not
 *     disclose such Confidential Information and shall use it only in
 *     accordance with the terms of the licence agreement you entered into
 *     with hashCode Solutions.
 */

package com.hashcode.iwmi.floodwater.services;

//
// IMPORTS
// NOTE: Import specific classes without using wildcards.
//

/**
 * <p>
 *  Map data Sync service. This service will check for any modifications on KML meta data
 *  file. If any changes found it will broadcast the message to clients to update the map data.
 * </p>
 *
 * @author Amila Silva
 * @email  amilasilva88@gmail.com
 * @version $Id$
 **/
public interface DataSyncService
{

    /**
     * <p>
     * Scheduled task to check the the KML meta data file modification.
     * </p>
     */
    void checkForDataModification();
}
