/**
 * 
 */
package com.hashcode.iwmi.floodwater.model;

/**
 * <p>
 * Annual water level model class
 * </p>
 * 
 * @author asilva
 * @email amila@hashcodesys.com
 * 
 */
public class AnnualWaterLevel {

	private String pointName;
	private int year;
	private double waterLevel;

	/**
	 * Construtor
	 * 
	 * @param pointName
	 * @param year
	 * @param waterLevel
	 */
	public AnnualWaterLevel(String pointName, int year, double waterLevel) {
		this.pointName = pointName;
		this.year = year;
		this.waterLevel = waterLevel;
	}

	/**
	 * @return the pointName
	 */
	public String getPointName() {
		return pointName;
	}

	/**
	 * @param pointName
	 *            the pointName to set
	 */
	public void setPointName(String pointName) {
		this.pointName = pointName;
	}

	/**
	 * @return the year
	 */
	public int getYear() {
		return year;
	}

	/**
	 * @param year
	 *            the year to set
	 */
	public void setYear(int year) {
		this.year = year;
	}

	/**
	 * @return the waterLevel
	 */
	public double getWaterLevel() {
		return waterLevel;
	}

	/**
	 * @param waterLevel
	 *            the waterLevel to set
	 */
	public void setWaterLevel(double waterLevel) {
		this.waterLevel = waterLevel;
	}

}
