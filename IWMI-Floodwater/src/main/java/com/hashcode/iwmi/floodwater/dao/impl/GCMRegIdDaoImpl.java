/*
 * Created on Nov 11, 2014
 * 
 * All sources, binaries and HTML pages (C) copyright 2014 by NextLabs, Inc.,
 * San Mateo CA, Ownership remains with NextLabs, Inc., All rights reserved
 * worldwide.
 *
 */
package com.hashcode.iwmi.floodwater.dao.impl;

import javax.transaction.Transactional;

import org.springframework.stereotype.Repository;

import com.hashcode.iwmi.floodwater.dao.GCMRegIdDao;
import com.hashcode.iwmi.floodwater.domain.GCMRegId;

/**
 * <p>
 * Implementation of the GCM Registration DAO
 * </p>
 *
 *
 * @author Amila Silva
 * @email amilasilva88@gmail.com
 *
 */
@Transactional
@Repository
public class GCMRegIdDaoImpl extends GenericDaoImpl<GCMRegId, Long> implements GCMRegIdDao
{

}
