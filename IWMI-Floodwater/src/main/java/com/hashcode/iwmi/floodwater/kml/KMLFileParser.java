/*
 * FILENAME
 *     KMLFileParser.java
 *
 * FILE LOCATION
 *     $Source$
 *
 * VERSION
 *     $Id$
 *         @version       $Revision$
 *         Check-Out Tag: $Name$
 *         Locked By:     $Lockers$
 *
 * FORMATTING NOTES
 *     * Lines should be limited to 78 characters.
 *     * Files should contain no tabs.
 *     * Indent code using four-character increments.
 *
 * COPYRIGHT
 *     Copyright (C) 2014 hashCode Solutions Pty. Ltd. All rights reserved.
 *     This software is the confidential and proprietary information of
 *     hashCode Solutions ("Confidential Information"). You shall not
 *     disclose such Confidential Information and shall use it only in
 *     accordance with the terms of the licence agreement you entered into
 *     with hashCode Solutions.
 */

package com.hashcode.iwmi.floodwater.kml;

import java.io.FileInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.hashcode.iwmi.floodwater.domain.GeoCoordinate;
import com.hashcode.iwmi.floodwater.exception.KMLParseException;
import com.hashcode.iwmi.floodwater.model.KMLFileMetaData;
import com.hashcode.iwmi.floodwater.model.MapData;
import com.hashcode.iwmi.floodwater.model.PointPlacemark;
import com.hashcode.iwmi.floodwater.model.RoutePlacemark;

//
// IMPORTS
// NOTE: Import specific classes without using wildcards.
//

/**
 * <p>
 * KML File parser class which will read the KML file and parse the routes and placemarks.
 * </p>
 * 
 *
 * @author Amila Silva
 * @email amilasilva88@gmail.com
 **/
@Service
public class KMLFileParser
{
    protected static final String FOLDER = "Folder";
    protected static final String PLACEMARK = "Placemark";
    protected static final String NAME = "name";
    protected static final String DESCRIPTION = "description";
    protected static final String LINE_STRING = "LineString";
    protected static final String POINT = "Point";
    protected static final String COORDINATES = "coordinates";

    private static final Logger log = LoggerFactory.getLogger(KMLFileParser.class);

    /**
     * <p>
     * Parse KML metadata in to MAP data.
     * 
     * This will extract the KML files from each KML meta data and process a Map.
     * </p>
     *
     * @param kmlFileMetaData
     *            List of KML file meta data
     * @return list of {@link MapData}
     */
    public List<MapData> parseMultiKML(List<KMLFileMetaData> kmlFileMetaDataList) throws KMLParseException
    {
        log.debug("load multiple KML files to process, [ No of KML meta data : {}],", kmlFileMetaDataList.size());
        List<MapData> maps = new ArrayList<>();

        kmlFileMetaDataList.stream().forEach((kml) -> {
            MapData map = new MapData();
            maps.add(map);

            map.setId(System.currentTimeMillis());
            map.setMapName(kml.getName());
            map.setZoomLevel(kml.getMapZoomLevel());
            map.setCameraPosition(kml.getCamPosition());

            try
            {
                map = parseKML(kml.getKmlFiles(), map);
            }
            catch (Exception e)
            {
                throw new KMLParseException("Error occurred in parseMultiKML ", e);
            }
        });

        log.info("Multiple KML metadata process successfully. [ No of Map data : {} ]", maps.size());
        return maps;
    }

    /**
     * <p>
     * Parse KML files in to MapData.
     * </p>
     *
     * @param kmlFiles
     *            list of KML files
     * @return {@link MapData}
     */
    protected MapData parseKML(List<String> kmlFiles, MapData kmlMapData) throws KMLParseException
    {
        log.debug("Parse KML files, [ No of KML files : {}],", kmlFiles.size());

        kmlFiles.stream().forEach((file) -> {
            try
            {
                Document kmlDocument = parseKMLDocument(file);
                processDocument(kmlDocument, kmlMapData);
            }
            catch (Exception e)
            {
                throw new KMLParseException("Error occurred in parseKML ", e);
            }

        });

        log.info("{} KML files in {} parsed successfully.", kmlFiles.size(), kmlMapData.getMapName());
        return kmlMapData;
    }

    /**
     * <p>
     * Process KML document to extract contents. Paths, Points,
     * </p>
     *
     * @param kmlDocument
     *            kml document
     * @param mapData
     *            Map data
     * @return processed with map data
     */
    protected MapData processDocument(Document kmlDocument, MapData mapData) throws KMLParseException
    {
        if (kmlDocument == null)
        {
            log.warn("No KML document found to process");
            return mapData;
        }

        NodeList placeMarkers = kmlDocument.getElementsByTagName(PLACEMARK);

        for (int i = 0; i < placeMarkers.getLength(); i++)
        {

            Element placemark = (Element) placeMarkers.item(i);

            if (placemark != null)
            {

                String name = getElementValueByName(placemark, NAME);
                String description = getElementValueByName(placemark, DESCRIPTION);
                Element lineString = (Element) placemark.getElementsByTagName(LINE_STRING).item(0);
                Element pointEle = (Element) placemark.getElementsByTagName(POINT).item(0);

                if (lineString != null)
                {

                    RoutePlacemark route = new RoutePlacemark();
                    route.setId(name + "_" + i);
                    route.setName(name);
                    route.setDescription(description);

                    String coordinates = getElementValueByName(lineString, COORDINATES);
                    String[] coords = coordinates.split("\\ ");

                    for (int k = 0; k < coords.length; k++)
                    {
                        if (coords[k].split("\\,").length == 3)
                        {
                            String[] coord = coords[k].split("\\,");
                            double lng = Double.parseDouble(coord[0]);
                            double lat = Double.parseDouble(coord[1]);
                            route.getCoordinates().add(new GeoCoordinate(lat, lng));
                        }
                    }

                    mapData.addRoute(route);

                }
                else if (pointEle != null)
                {

                    PointPlacemark point = new PointPlacemark();
                    point.setId(name + "_" + i);
                    point.setName(name);
                    point.setDescription(description);

                    String coordinates = getElementValueByName(pointEle, COORDINATES);
                    String[] coords = coordinates.split("\\,");

                    if (coords.length == 3)
                    {
                        double lng = Double.parseDouble(coords[0]);
                        double lat = Double.parseDouble(coords[1]);
                        point.setPosition(new GeoCoordinate(lat, lng));
                    }
                    mapData.addPoint(point);
                }
            }
        }
        log.debug("successfully process KML document, Name of the file : {} ", mapData.getMapName());
        return mapData;
    }

    /**
     * <p>
     * Get KML document from the KML file
     * </p>
     *
     * @param kmlFile
     * @return
     */
    protected Document parseKMLDocument(String kmlFile) throws KMLParseException
    {
        try
        {
            Document document = getDocument(kmlFile);
            return document;

        }
        catch (Exception e)
        {
            throw new KMLParseException("Error occurred in parseKMLDocument from KML file", e);
        }
    }

    private Document getDocument(String kmlFile) throws KMLParseException
    {
        InputStream inputStream;
        try
        {
            inputStream = new FileInputStream(kmlFile);

            DocumentBuilder docBuilder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
            Document document = docBuilder.parse(inputStream);
            return document;
        }
        catch (Exception e)
        {
            throw new KMLParseException("Error occurred in getDocument from KML file", e);
        }
    }

    private String getElementValueByName(Element element, String name)
    {
        Node node = element.getElementsByTagName(name).item(0);
        return (node != null) ? node.getTextContent() : "";
    }
}
