/*
 * FILENAME
 *     PredictedWaterLevelDao.java
 *
 * FILE LOCATION
 *     $Source$
 *
 * VERSION
 *     $Id$
 *         @version       $Revision$
 *         Check-Out Tag: $Name$
 *         Locked By:     $Lockers$
 *
 * FORMATTING NOTES
 *     * Lines should be limited to 78 characters.
 *     * Files should contain no tabs.
 *     * Indent code using four-character increments.
 *
 * COPYRIGHT
 *     Copyright (C) 2013 hashCode Solutions Pty. Ltd. All rights reserved.
 *     This software is the confidential and proprietary information of
 *     hashCode Solutions ("Confidential Information"). You shall not
 *     disclose such Confidential Information and shall use it only in
 *     accordance with the terms of the licence agreement you entered into
 *     with hashCode Solutions.
 */

package com.hashcode.iwmi.floodwater.dao;

import java.time.LocalDate;
import java.util.List;

import com.hashcode.iwmi.floodwater.domain.PredictedWaterLevel;

//
// IMPORTS
// NOTE: Import specific classes without using wildcards.
//

/**
 * <p>
 * Predicted Water Level Dao interface.
 * </p>
 *
 * @author Manuja
 *
 * @version $Id$
 */
public interface PredictedWaterLevelDao extends GenericDao<PredictedWaterLevel, Long>
{
    /**
     * <p>
     * Get predicted water levels for given name and date period.
     * </p>
     *
     * @param name measuring point name
     * @param fromDate water level from date
     * @param toDate water level to date
     * 
     * @return list of predicted water data levels
     */
    List<PredictedWaterLevel> getPredictedWaterLevels(String name, LocalDate fromDate, LocalDate toDate);
    
    /**
     * <p>
     * Get predicted water levels for given id and date period.
     * </p>
     *
     * @param name measuring point id
     * @param fromDate water level from date
     * @param toDate water level to date
     * 
     * @return list of predicted water data levels
     */
    List<PredictedWaterLevel> getPredictedWaterLevels(Long id, LocalDate fromDate, LocalDate toDate);
    
    /**
     * <p>
     * Get predicted water level for given date.
     * </p>
     *
     * @param date date
     * 
     * @return predicted water level instance
     */
    PredictedWaterLevel getPredictedWaterLevel(LocalDate date);
    
    /**
     * <p>
     * Identify the measuring points to generate risk alerts.
     * </p>
     *
     * @param date date to check the risk area
     * @return measuring points which are under risk
     *
     */
    List<Object[]> findPointsToAlert(LocalDate date);
}
