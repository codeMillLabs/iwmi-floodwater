<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html lang="en">

<jsp:include page="layout/header.jsp" />
<body>

	<div id="wrapper">
		<jsp:include page="layout/menu.jsp" />

		<!-- Page Content -->
		<div id="page-wrapper">
			<div class="row">
				<div class="col-lg-12">
					<h2 class="page-header">Add User</h2>
				</div>
				<!-- /.col-lg-12 -->
			</div>
			<div class="row">
				<div class="col-lg-12">
					<jsp:include page="content/AddUser-body.jsp" />
				</div>
				<!-- /.col-lg-12 -->
			</div>
			<!-- /.row -->
		</div>
		<!-- /#page-wrapper -->

	</div>
	<!-- /#wrapper -->

	<jsp:include page="layout/footer.jsp" />

	<script type="text/javascript">
		$(document).ready(function() {
			validateAndSubmitForm();
		});

		function validateAndSubmitForm() {
			$('#addUserForm')
					.bootstrapValidator(
							{
								feedbackIcons : {
									valid : 'glyphicon glyphicon-ok',
									invalid : 'glyphicon glyphicon-remove',
									validating : 'glyphicon glyphicon-refresh'
								},
								fields : {
									email : {
										validators : {
											notEmpty : {
												message : 'The email address is required and cannot be empty'
											},
											emailAddress : {
												message : 'The email address is not a valid'
											},
											callback : {
												callback : function(value,
														validator, $field) {
													var reqData = {
														email : $("#email")
																.val()
													}
													var response = saveUser(
															'./checkEmail',
															reqData);
													if (response.status == 'false') {
														return {
															valid : false,
															message : 'Email address already taken'
														};
													}

													return true;
												}
											}
										}
									},
									password : {
										validators : {
											notEmpty : {
												message : 'The password is required and cannot be empty'
											},
											stringLength : {
												min : 6,
												message : 'The password must have at least 6 characters'
											},
											identical : {
												field : 'confirmPassword',
												message : 'The password and its confirm are not the same'
											}
										}
									},
									confirmPassword : {
										validators : {
											notEmpty : {
												message : 'The password is required and cannot be empty'
											},
											stringLength : {
												min : 6,
												message : 'The password must have at least 6 characters'
											},
											identical : {
												field : 'password',
												message : 'The password and its confirm are not the same'
											}
										}
									},
									firstName : {
										validators : {
											notEmpty : {
												message : 'First name is required'
											}
										}
									},
									lastName : {
										validators : {
											notEmpty : {
												message : 'Last name is required'
											}
										}

									}
								}
							}).on(
							'success.form.bv',
							function(e) {
								// Prevent form submission
								e.preventDefault();

								var email = $("#email").val();
								var firstName = $("#firstName").val();
								var lastName = $("#lastName").val();
								var password = $("#password").val();

								//save form
								var reqData = {
									email : email,
									firstName : firstName,
									lastName : lastName,
									password : password
								};

								console.log("User data to save :"
										+ JSON.stringify(reqData));

								var response = saveUser('./saveUserForm',
										reqData);
							});
		}

		function saveUser(posturl, postData) {
			var responseData;

			$.ajax({
				type : 'POST',
				contentType : "application/json; charset=utf-8",
				dataType : "json",
				url : posturl,
				data : JSON.stringify(postData),
				success : function(data) {
					responseData = data;
				},
				error : function(xhr, status) {
					window.location = "";
				},
				async : false
			});

			return responseData;
		}
	</script>

</body>
</html>
