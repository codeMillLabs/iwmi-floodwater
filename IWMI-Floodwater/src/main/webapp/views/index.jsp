<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html lang="en">

<jsp:include page="layout/header.jsp" />
<body>

	<div id="wrapper">
		<jsp:include page="layout/menu.jsp" />

		<!-- Page Content -->
		<div id="page-wrapper">
			<div class="row">
				<div class="col-lg-12">
					<h2 class="page-header">Dashboard</h2>
				</div>
				<!-- /.col-lg-12 -->
			</div>
			<div class="row">
				<div class="col-lg-12">
					<jsp:include page="content/index-body.jsp" />
				</div>
				<!-- /.col-lg-12 -->
			</div>
			<!-- /.row -->
		</div>
		<!-- /#page-wrapper -->

	</div>
	<!-- /#wrapper -->

	<jsp:include page="layout/footer.jsp" />

	<script type="text/javascript">
		$(document).ready(
				function() {
					var requestData = {
						date : todayDate()
					};
					var todayWaterLevelData = fetchDashBoardWaterLevels(
							"./todayWaterLevels", requestData);
					var tomorrowWaterLevelData = fetchDashBoardWaterLevels(
							"./tomorrowWaterLevels", requestData);
					var last30DaysWaterLevelData = fetchDashBoardWaterLevels(
							"./last30DaysWaterLevels", requestData);
					var next5DaysWaterLevelData = fetchDashBoardWaterLevels(
							"./next5DaysWaterLevels", requestData);
					var annualWaterDataHistoryData = fetchDashBoardWaterLevels(
							"./annualWaterDataHistory", requestData);

					discreteBarChart("todayWaterLevel_Chart",
							todayWaterLevelData);
					discreteBarChart("tomorrowWaterLevel_Chart",
							tomorrowWaterLevelData);
					timeSeriesLineChart("last30Days_Chart",
							'%m-%d', 30, last30DaysWaterLevelData);
					timeSeriesLineChart("next5Days_Chart",
							'%m-%d', 5, next5DaysWaterLevelData); 
				 	multiLineChart_c3("archive_Chart",
							annualWaterDataHistoryData);
					
					/* stackedAreaChart("last30Days_Chart",
							last30DaysWaterLevelData, "", "");
					stackedAreaChart("next5Days_Chart",
							next5DaysWaterLevelData, "", "");
					stackedAreaChartWithNoXAxisFormat("archive_Chart",
							annualWaterDataHistoryData, "", "");  */
				});

		function fetchDashBoardWaterLevels(posturl, postData) {
			var responseData;

			$.ajax({
				type : 'POST',
				contentType : "application/json; charset=utf-8",
				dataType : "json",
				url : posturl,
				data : JSON.stringify(postData),
				success : function(data) {
					responseData = data;
				},
				error : function(xhr, status) {
					//alert("Something went wrong, System requires re-login");
					//window.location = "./login";
				},
				async : false
			});
			return responseData;
		}
	</script>



</body>
</html>
