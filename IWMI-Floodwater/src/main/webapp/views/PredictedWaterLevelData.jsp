<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html lang="en">

<jsp:include page="layout/header.jsp" />
<body>

	<div id="wrapper">
		<jsp:include page="layout/menu.jsp" />

		<!-- Page Content -->
		<div id="page-wrapper">
			<div class="row">
				<div class="col-lg-12">
					<h2 class="page-header">Forecasted Water Levels</h2>
				</div>
				<!-- /.col-lg-12 -->
			</div>
			<div class="row">
				<div class="col-lg-12">
					<jsp:include page="content/PredictedWaterLevelData-body.jsp" />
				</div>
				<!-- /.col-lg-12 -->
			</div>
			<!-- /.row -->
		</div>
		<!-- /#page-wrapper -->

	</div>
	<!-- /#wrapper -->

	<jsp:include page="layout/footer.jsp" />
<div data-alerts="alerts" data-titles="{'warning': '<em>Warning!</em>'}" data-ids="myid" data-fade="3000"></div>
<button id="warn-me" class="btn">Click to see a warning alert</button>
	<script type="text/javascript">
		$(document).ready(function() {

			$('.input-group.date').datepicker({
				format : "yyyy-mm-dd",
				todayBtn : "linked",
				autoclose : true,
				todayHighlight : true,
			});

			$('.input-group.date').datepicker('setDate', new Date());

			var reqData = {
				measurePointId : "-1",
				fromDate : $("#fromDate").val(),
				toDate : $("#toDate").val()
			};

			renderDataTable('./searchForecastedLevels', reqData);

			$("form#forecastedWaterLevelForm").submit(function (e) {     			 
     			e.preventDefault();
     			e.stopPropagation();
     			validateAndSubmitForm();
			});
			
			
			$("#forecastedWaterLevelForm").bind("reset", function() {
				resetForm();
				return false;
			});
			
			$("[id^=waterLevel]").keydown(function (e) {
		        // Allow: backspace, delete, tab, escape, enter and .
		        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
		             // Allow: Ctrl+A
		            (e.keyCode == 65 && e.ctrlKey === true) || 
		             // Allow: home, end, left, right, down, up
		            (e.keyCode >= 35 && e.keyCode <= 40)) {
		                 // let it happen, don't do anything
		                 return;
		        }
		        // Ensure that it is a number and stop the keypress
		        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
		            e.preventDefault();
		        }
		    });
			
			$("[id^=measuringPoint]").change(function(){
			    $("[id^=measuringPoint]").val( $(this).val());
			});
		});
		
		function resetForm()
		{
			$('.input-group.date').datepicker('setDate', new Date());
			$("[id^=waterLevel]").val("");
			$("[id^=comment]").val("");
		}

		$("#searchBtn").click(function() {

			var measurePointId = $("#sMeasuringPoint").val();
			var fromDate = $("#fromDate").val();
			var toDate = $("#toDate").val();

			var reqData = {
				measurePointId : measurePointId,
				fromDate : fromDate,
				toDate : toDate
			};


			renderDataTable('./searchForecastedLevels', reqData);

		});

		function renderDataTable(url, postData) {
			
			var response = postRequest(url, postData);
			var dataSet = prepareTableData(response, './deleteForecastedLevels', './searchForecastedLevels', './loadForecastedLevelForm');

			$('#forecasted-water-data-table').DataTable({
				"bAutoWidth" : false,
				"bJQueryUI" : true,
				"aaData" : dataSet.rows,
				"bDestroy" : true,
				"aoColumns" : [ {
					"sTitle" : "Date",
					"mData" : "date",
					"sWidth" : "10%"
				}, {
					"sTitle" : "Measuring Point",
					"mData" : "measuringPoint",
					"sWidth" : "30%"
				}, {
					"sTitle" : "Water Level",
					"mData" : "level",
					"sWidth" : "20%"
				}, {
					"sTitle" : "Comment",
					"mData" : "comment",
					"sWidth" : "30%"
				}, {
					"sTitle" : "Action",
					"mData" : "action",
					"sWidth" : "10%"
				} ]
			});
		}
		
		
		function validateAndSubmitForm() {
			
			var result = "";
			var measurePointId = $("#measuringPointCommon").val();
			var comment = $("#commentCommon").val();
			
			for (i = 0; i < 10; i++) {
	
					var recordDate = $("#recordDate" + i).val();
					var waterLevel = $("#waterLevel" + i).val();
	
					if (recordDate != "" && waterLevel != "") {
	
						var reqData = {
							measurePointId : measurePointId,
							recordDate : recordDate
						};
	
						var response = saveWaterLevelData(
								'./checkPredictedWaterLevel', reqData);
						if (response.status == 'false') {
							result = result + 'Water level already saved for point ['
									+ measurePointId + '] and date [' + recordDate
									+ '] <br/>';
						} else {
							//save form
							var reqData = {
								measurePointId : measurePointId,
								recordDate : recordDate,
								comment : comment,
								waterLevel : waterLevel
							};
	
							// Use Ajax to submit form data
							var response = saveWaterLevelData(
									'./saveForecastedLevelForm', reqData);
							
							result = result + 'Water level successfully saved for point ['
									+ measurePointId + '] and date [' + recordDate
									+ '] <br/>';
						}
					}
				}
			
			 	resetForm();
			
			    if(result == "")
			    {
			    	result = "No water data is saved. Please note all the fields are required";
			    	$('.top-right').notify({
				        message: { html: result},  type: 'warning', fadeOut: {
				            enabled: false,
				          }, onClosed: function () {
				        	  $('#newWaterLevelModal').modal('toggle');
				          }
				     }).show();
			    }
			    else
			    {
			    	$('.top-right').notify({
				        message: { html: result}, fadeOut: {
				            enabled: false,
				          }, onClosed: function () {
				        	  $('#newWaterLevelModal').modal('toggle');
				          }
				     }).show();
			    }
		}

		function saveWaterLevelData(posturl, postData) {
			var responseData;

			$.ajax({
				type : 'POST',
				contentType : "application/json; charset=utf-8",
				dataType : "json",
				url : posturl,
				data : JSON.stringify(postData),
				success : function(data) {
					responseData = data;
				},
				error : function(xhr, status) {
					//alert("Something went wrong, System requires re-login");
					window.location = "";
				},
				async : false
			});

			return responseData;
		}
	</script>

</body>
</html>
