<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<div class="row">
	<div class="col-lg-12">
		<div class="panel panel-default">
			<div class="panel-heading">Add Water Level Measuring Point</div>

			<div class="panel-body">
				<div class="row">
					<div class="col-lg-12">
						<form role="form" id="addMeasuringPointForm">
							<div class="row">
								<div class="col-lg-4">
									<div class="form-group">
										<label>Measuring Point</label>
										<input type="text" class="form-control" id="measuringName"
												name="measuringName">
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-lg-8">
									<div class="form-group">
										<label>Coordinates</label>
										<div class="form-inline">
											<input type="text" class="form-control"
												placeholder="Latitude" id="latitude" name="latitude">
											<input type="text" class="form-control"
												placeholder="Longitute" id="longitute" name="longitute">
										</div>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-lg-4">
									<div class="form-group">
										<label>Moderate Water Level</label>
										<input type="text" class="form-control" id="moderateLevelThreshold"
												name="moderateLevelThreshold">
									</div>
								</div>
								<div class="col-lg-4">
									<div class="form-group">
										<label>Risk Water Level</label>
										<input type="text" class="form-control" id="riskLevelThreshold"
												name="riskLevelThreshold">
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-lg-11">
									<button type="submit" id="searchBtn" class="btn btn-default">Save</button>
									<button type="reset" class="btn btn-default">Clear</button>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="row">
	<div class="col-lg-12">
		<div class="panel panel-default">
			<div class="panel-heading">Water Level Measuring Points</div>
			<div class="panel-body">
				<div class="row">
					<div class="col-lg-12">
						<div class="table-responsive" id="actual-water-data-table-div">
							<table class="table table-striped table-bordered table-hover"
								id="water-measuring-table">
								<thead>
									<tr>
										<th>Measuring Point</th>
										<th>Coordinates (Lat/Long)</th>
										<th>Moderate Water Level</th>
										<th>Risk Water Level</th>
									</tr>
								</thead>
							</table>
						</div>
						<!-- /.table-responsive -->
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

