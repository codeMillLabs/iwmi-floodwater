<div class="row">
	<div class="col-lg-6">
		<div class="panel panel-default">
			<div class="panel-heading">Today's Water Levels</div>

			<div class="panel-body">
				<div class="row">
					<div class="col-lg-12">
					    <svg id="todayWaterLevel_Chart" style="height:450px;"></svg>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="col-lg-6">
		<div class="panel panel-default">
			<div class="panel-heading">Tomorrow's Water Levels</div>

			<div class="panel-body">
				<div class="row">
					<div class="col-lg-12">
					   <svg id="tomorrowWaterLevel_Chart" style="height:450px;"></svg>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="row">
	<div class="col-lg-6">
		<div class="panel panel-default">
			<div class="panel-heading">Last 30 days Water Levels</div>

			<div class="panel-body">
				<div class="row">
					<div class="col-lg-12">
					    <div id="last30Days_Chart" style="height:450px;"></div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="col-lg-6">
		<div class="panel panel-default">
			<div class="panel-heading">Next 5 days Water Levels</div>

			<div class="panel-body">
				<div class="row">
					<div class="col-lg-12">
					    <div id="next5Days_Chart" style="height:450px;"></div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="row">
	<div class="col-lg-12">
		<div class="panel panel-default">
			<div class="panel-heading">Archives</div>

			<div class="panel-body">
				<div class="row">
					<div class="col-lg-12">
					    <div id="archive_Chart" style="height:450px;"></div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

