<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<div class="panel panel-default">
	<div class="panel-heading">User</div>

	<div class="panel-body">
		<div class="row">
			<div class="col-lg-4">
				<form role="form" id="addUserForm">
					<div class="form-group">
						<label>Email Address</label>
						<input class="form-control" placeholder="E-mail" name="email" id="email" type="email" autofocus>
					</div>
					<div class="form-group">
						<label>First Name</label>
						<input class="form-control" placeholder="First Name" id="firstName" name="firstName" />
					</div>
					<div class="form-group">
						<label>Last Name</label>
						<input class="form-control" placeholder="Last Name" id="lastName" name="firstName" />
					</div>
					<div class="form-group">
						<label>Password</label>
                        <input class="form-control" placeholder="Password" name="password" id="password" type="password" value="">
					</div>
					<div class="form-group">
						<label>Confirm Password</label>
                        <input class="form-control" placeholder="Confirm Password" name="confirmPassword" type="password" value="">
					</div>
					<button type="submit" id="submitBtn" class="btn btn-default">Save</button>
					<button type="reset" class="btn btn-default">Clear</button>
				</form>
			</div>
		</div>
	</div>
</div>
