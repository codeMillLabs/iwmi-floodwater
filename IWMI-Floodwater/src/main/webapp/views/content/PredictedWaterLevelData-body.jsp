<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<div>
	<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#newWaterLevelModal">
  		New Water Level
	</button>
</div><br/>
<div class='notifications top-right'></div>
<!-- Modal -->
<div class="modal fade" id="newWaterLevelModal" tabindex="-1" role="dialog" aria-labelledby="newWaterLevelModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="newWaterLevelModalLabel">Forecasted water level</h4>
      </div>
			<div class="modal-body">
				<form role="form" id="forecastedWaterLevelForm" name="forecastedWaterLevelForm">
					<div class="col-lg-4">
						<div class="form-group">
							<label>Measuring Point</label> <select class="form-control"
								id="measuringPointCommon" name="measuringPointCommon">
								<c:forEach items="${measuring_points}" var="point">
									<option value="${point.id}">${point.name}</option>
								</c:forEach>
							</select>
						</div>
					</div>
					<div style="clear: both;"></div>
				
					<table class="table table-striped table-bordered table-hover"
						id="add=forecasted-water-data-table">
						<thead>
							<tr>
								<th>Date</th>
								<th>Water Level</th>
							</tr>
						</thead>
					
					<tbody>
					<c:forEach begin="0" end="10" varStatus="loop">
					<tr>
					<td width="25%">
					<div class="form-group">
						<div class="input-group date">
							<input type="text" class="form-control" id="recordDate${loop.index}"
								name="recordDate${loop.index}"><span class="input-group-addon"><i
								class="fa fa-calendar fa-fw"></i></span>
						</div>
					</div>
					</td>
					<td width="15%">
					<div class="form-group">
						<input class="form-control"
							id="waterLevel${loop.index}" name="waterLevel${loop.index}" />
					</div>
					</td>
					<tr>
					</c:forEach>
					</tbody>
					</table>
					<div class="col-lg-10">
						<label>Comment</label><div class="form-group">
							<textarea class="form-control" rows="1" id="commentCommon"
								name="commentCommon"></textarea>
						</div>
					</div>
					<div style="clear: both;"></div>
					<button type="submit" id="submitBtn" class="btn btn-primary">Save</button>
					<button type="reset" class="btn btn-default">Clear</button>
				</form>
			</div>
			<div class="modal-footer">
      </div>
    </div>
  </div>
</div>

<div class="panel panel-default">
	<div class="panel-heading">Search Data</div>

	<div class="panel-body">
		<div class="row">
			<div class="col-lg-12">
				<form role="form" id="searchForecastedWaterLevels">
					<div class="row">
						<div class="col-lg-2">
							<div class="form-group">
								<label>From Date</label>
								<div class="input-group date">
									<input type="text" class="form-control" id="fromDate"
										name="fromDate"><span class="input-group-addon"><i
										class="fa fa-calendar fa-fw"></i></span>
								</div>
							</div>
						</div>
						<div class="col-lg-2">
							<div class="form-group">
								<label>To Date</label>
								<div class="input-group date">
									<input type="text" class="form-control" id="toDate"
										name="toDate"><span class="input-group-addon"><i
										class="fa fa-calendar fa-fw"></i></span>
								</div>
							</div>
						</div>
						<div class="col-lg-2">
							<div class="form-group">
								<label>Measuring Point</label> <select class="form-control"
									id="sMeasuringPoint" name="sMeasuringPoint">
									<option value="-1"></option>
									<c:forEach items="${measuring_points}" var="point">
										<option value="${point.id}">${point.name}</option>
									</c:forEach>
								</select>
							</div>
						</div>
						<div class="col-lg-2">
							<div class="search-button-layout">
								<button type="button" id="searchBtn" class="btn btn-default">Search</button>
								<button type="reset" class="btn btn-default">Clear</button>
							</div>
						</div>
					</div>
				</form>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-12">
			  <div class="panel-body"><hr/></div>
			</div>
		</div>	
		<div class="row">
			<div class="col-lg-12">
				<div class="table-responsive" id="forecasted-water-data-table-div">
					<table class="table table-striped table-bordered table-hover"
						id="forecasted-water-data-table">
						<thead>
							<tr>
								<th>Date</th>
								<th>Measuring Point</th>
								<th>Water Level</th>
								<th>Comment</th>
								<th>Action</th>
							</tr>
						</thead>
					</table>
				</div>
				<!-- /.table-responsive -->
			</div>
		</div>
	</div>
</div>
