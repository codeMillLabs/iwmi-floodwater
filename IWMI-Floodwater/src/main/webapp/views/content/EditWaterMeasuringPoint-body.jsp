<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<div class="row">
	<div class="col-lg-12">
		<div class="panel panel-default">
			<div class="panel-heading">Add Water Level Measuring Point</div>

			<div class="panel-body">
				<div class="row">
					<div class="col-lg-12">
						<form role="form" id="addMeasuringPointEidtForm">
							<input type="hidden" class="form-control" id="pointId"
												name="pointId" value="${point.id}">
						
							<div class="row">
								<div class="col-lg-4">
									<div class="form-group">
										<label>Measuring Point</label>
										<input type="text" class="form-control" id="measuringName"
												name="measuringName" value="${point.name}">
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-lg-6">
									<div class="form-group">
										<label>Coordinates</label>
										<div class="form-inline">
											<input type="text" class="form-control"
												placeholder="Latitude" id="latitude" name="latitude" value="${point.geoCoordinate.lat}">
											<input type="text" class="form-control"
												placeholder="Longitute" id="longitute" name="longitute" value="${point.geoCoordinate.lng}">
										</div>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-lg-4">
									<div class="form-group">
										<label>Moderate Water Level</label>
										<input type="text" class="form-control" id="moderateLevelThreshold"
												name="moderateLevelThreshold" value="${point.moderateLevelThreshold}">
									</div>
								</div>
								<div class="col-lg-4">
									<div class="form-group">
										<label>Risk Water Level</label>
										<input type="text" class="form-control" id="riskLevelThreshold"
												name="riskLevelThreshold" value="${point.riskLevelThreshold}">
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-lg-11">
									<button type="submit" id="searchBtn" class="btn btn-default">Save</button>
									<button type="reset" class="btn btn-default">Clear</button>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>