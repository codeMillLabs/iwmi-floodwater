<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<div class="panel panel-default">
	<div class="panel-heading">Form</div>

	<div class="panel-body">
		<div class="row">
			<div class="col-lg-4">
				<c:if test="${successMsg != null}">
					<div id="formSucessAlert"
						class="alert alert-success alert-dismissable">
						<button type="button" class="close" data-dismiss="alert"
							aria-hidden="true">&times;</button>
						<a href="#" class="alert-link">Success!</a> <span id="successMsg"><c:out
								value="${successMsg}" /></span>.
					</div>
				</c:if>
				<c:if test="${errorMsg != null}">
					<div id="formErrorAlert"
						class="alert alert-danger alert-dismissable">
						<button type="button" class="close" data-dismiss="alert"
							aria-hidden="true">&times;</button>
						<a href="#" class="alert-link">Error!</a> <span id="errorMsg"><c:out
								value="${errorMsg}" /></span>
					</div>
				</c:if>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-4">
				<form role="form" id="actualWaterLevelForm">
					<input type="hidden" id="waterLevelId"
								name="waterLevelId" value="${actualWaterLevel.id}">
					
					<div class="form-group">
						<label>Measuring Point</label> <select class="form-control"
							id="measuringPoint" name="measuringPoint">
							<c:forEach items="${measuring_points}" var="point">
								<c:choose>
									<c:when
										test="${actualWaterLevel.measuringPoint.id eq point.id}">
										<option value="${point.id}" selected="selected">${point.name}</option>
									</c:when>
									<c:otherwise>
										<option value="${point.id}">${point.name}</option>
									</c:otherwise>
								</c:choose>
							</c:forEach>
						</select>
					</div>
					<div class="form-group">
						<label>Date</label>
						<div class="input-group date">
							<input type="text" class="form-control" id="recordDate"
								name="recordDate" value="${actualWaterLevel.date}"><span class="input-group-addon"><i class="fa fa-calendar fa-fw"></i></span>
						</div>
					</div>
					<div class="form-group">
						<label>Comment</label>
						<textarea class="form-control" rows="3" id="comment"
							name="comment">${actualWaterLevel.comment}</textarea>
					</div>
					<div class="form-group">
						<label>Water Level</label> <input class="form-control"
							id="waterLevel" name="waterLevel" value="${actualWaterLevel.waterLevel}"/>
					</div>
					<button type="submit" id="submitBtn" class="btn btn-default">Save</button>
					<button type="reset" class="btn btn-default">Clear</button>
				</form>
			</div>
		</div>
	</div>
</div>