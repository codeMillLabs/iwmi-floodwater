<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html lang="en">

<jsp:include page="layout/header.jsp" />
<body>

	<div id="wrapper">
		<jsp:include page="layout/menu.jsp" />

		<!-- Page Content -->
		<div id="page-wrapper">
			<div class="row">
				<div class="col-lg-12">
					<h2 class="page-header">Edit Forecasted Water Levels</h2>
				</div>
				<!-- /.col-lg-12 -->
			</div>
			<div class="row">
				<div class="col-lg-12">
					<jsp:include page="content/AddPredictedWaterLevel-body.jsp" />
				</div>
				<!-- /.col-lg-12 -->
			</div>
			<!-- /.row -->
		</div>
		<!-- /#page-wrapper -->

	</div>
	<!-- /#wrapper -->

	<jsp:include page="layout/footer.jsp" />

	<script type="text/javascript">
		$(document).ready(function() {

			$('.input-group.date').datepicker({
				format : "yyyy-mm-dd",
				todayBtn : "linked",
				autoclose : true,
				todayHighlight : true
			});

			validateAndSubmitForm();
		});

		function validateAndSubmitForm() {
			$('#predictedWaterLevelForm')
					.bootstrapValidator(
							{
								feedbackIcons : {
									valid : 'glyphicon glyphicon-ok',
									invalid : 'glyphicon glyphicon-remove',
									validating : 'glyphicon glyphicon-refresh'
								},
								fields : {
									measuringPoint : {
										validators : {
											notEmpty : {
												message : 'Measuring Point is required'
											},
											callback : {
												callback : function(value,
														validator, $field) {
													var reqData = {
														measurePointId : $(
																"#measuringPoint")
																.val(),
														recordDate : $(
																"#recordDate")
																.val(),
														id : $(
																"#waterLevelId")
																.val()
													};
													var response = saveWaterLevelData(
															'./checkActualWaterLevelWithId',
															reqData);
													if (response.status == 'false') {
														return {
															valid : false,
															message : 'Water levels already saved for measuring point'
														};
													}

													return true;
												}
											}
										}
									},
									recordDate : {
										validators : {
											notEmpty : {
												message : 'Date is required'
											},
										}
									},
									waterLevel : {
										validators : {
											notEmpty : {
												message : 'Water Level is required'
											},
											numeric : {
												message : 'Value should be numeric'
											}
										}
									}
								}
							})
					.on(
							'success.form.bv',
							function(e) {
								// Prevent form submission
								e.preventDefault();
								var waterLevelId = $("#waterLevelId").val();
								var measurePointId = $("#measuringPoint").val();
								var recordDate = $("#recordDate").val();
								var comment = $("#comment").val();
								var waterLevel = $("#waterLevel").val();

								//save form
								var reqData = {
									waterLevelId : waterLevelId,
									measurePointId : measurePointId,
									recordDate : recordDate,
									comment : comment,
									waterLevel : waterLevel
								};

								// Use Ajax to submit form data
								var response = saveWaterLevelData(
										'./updateForcastedLevelForm', reqData);
							});
		}

		function saveWaterLevelData(posturl, postData) {
			var responseData;

			$.ajax({
				type : 'POST',
				contentType : "application/json; charset=utf-8",
				dataType : "json",
				url : posturl,
				data : JSON.stringify(postData),
				success : function(data) {
					responseData = data;
				},
				error : function(xhr, status) {
					//alert("Something went wrong, System requires re-login");
					window.location = "";
				},
				async : false
			});

			return responseData;
		}
	</script>

</body>
</html>
