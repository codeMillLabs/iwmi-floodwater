<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html lang="en">

<jsp:include page="layout/header.jsp" />
<body>

	<div id="wrapper">
		<jsp:include page="layout/menu.jsp" />

		<!-- Page Content -->
		<div id="page-wrapper">
			<div class="row">
				<div class="col-lg-12">
					<h2 class="page-header">Users</h2>
				</div>
				<!-- /.col-lg-12 -->
			</div>
			<div class="row">
				<div class="col-lg-12">
					<jsp:include page="content/UserData-body.jsp" />
				</div>
				<!-- /.col-lg-12 -->
			</div>
			<!-- /.row -->
		</div>
		<!-- /#page-wrapper -->

	</div>
	<!-- /#wrapper -->

	<jsp:include page="layout/footer.jsp" />

	<script type="text/javascript">
		$(document).ready(function() {

			var reqData = {};
			var response = postRequest('./searchUsers', reqData);

			var tableData = prepareUserData(response, './deleteUsers');
			renderDataTable(tableData);
		});

		$("#searchBtn").click(function() {

			var reqData = {};
			var response = postRequest('./searchUsers', reqData);

			var tableData = prepareUserData(response, './deleteUsers');
			renderDataTable(tableData);

		});

		function prepareUserData(response, postUrl) {
			var rows = {};
			var dataofTable = [];
			for (var i = 0; i < response.length; i++) {
				var responseRow = {
					email : response[i]['email'],
					firstName : response[i]['firstName'],
					lastName : response[i]['lastName'],
					action : '<a href="#" onClick="deleteUser('
							+ response[i]['id'] + ', \'' + postUrl
							+ '\')">Delete</a>'
				};
				dataofTable.push(responseRow);
			}
			rows["rows"] = dataofTable;
			return rows;
		}

		function renderDataTable(dataSet) {
			$('#predicted-water-data-table').DataTable({
				"bAutoWidth" : false,
				"bJQueryUI" : true,
				"bDestroy" : true,
				"aaData" : dataSet.rows,
				"aoColumns" : [ {
					"sTitle" : "Email",
					"mData" : "email",
					"sWidth" : "20%"
				}, {
					"sTitle" : "First Name",
					"mData" : "firstName",
					"sWidth" : "35%"
				}, {
					"sTitle" : "Last Name",
					"mData" : "lastName",
					"sWidth" : "35%"
				}, {
					"sTitle" : "Action",
					"mData" : "action",
					"sWidth" : "10%"
				} ]
			});
		}
		
		function deleteUser(idValue, postUrl) {
			bootbox.dialog({
				message : "Are you sure want to delete selected user ?",
				title : "Confirm",
				buttons : {
					success : {
						label : "Cancel",
						className : "btn-success",
						callback : function() {
						}
					},
					danger : {
						label : "Ok",
						className : "btn-danger",
						callback : function() {
							var reqData = {
								id : idValue
							};
							var response = postRequest(postUrl, reqData);
							var tableData = prepareUserData(response, postUrl);
							renderDataTable(tableData);
						}
					}
				}
			});
		}
	</script>

</body>
</html>