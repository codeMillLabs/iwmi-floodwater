<!DOCTYPE html>
<html lang="en">

<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="IWMI Flood water Information System">
<meta name="author" content="Amila Silva">

<link rel="icon" href="../images/favicon.ico">

<title>IWMI Flood Water Forecasting System</title>

<!-- Bootstrap Core CSS -->
<link href="javascript/bootstrap/css/bootstrap.min.css" rel="stylesheet">

<!-- Custom CSS -->
<link href="javascript/bootstrap/css/iwmi-floodwater-app.css"
	rel="stylesheet">

<!-- Custom Fonts -->
<link
	href="javascript/bootstrap/font-awesome-4.2.0/css/font-awesome.min.css"
	rel="stylesheet" type="text/css">

<!-- Validator CSS -->
<link rel="stylesheet"
	href="javascript/bootstrap/css/plugins/validator/bootstrapValidator.min.css" />

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>
<body>
	<div class="container">
		<div class="row">
			<div class="col-md-4 col-md-offset-4">
				<div class="login-panel panel panel-default">
					<div class="panel-heading">
						<h3 class="panel-title">Please Sign In</h3>
					</div>
					<div class="panel-body">
						<div id="formErrorAlert"
							class="alert alert-warning alert-dismissable"
							style='display: none;'>
							<button type="button" class="close" data-dismiss="alert"
								aria-hidden="true">&times;</button>
							<a href="#" class="alert-link">Login Failed!</a> <span
								id="errorMsg">Invalid e-mail or password</span>
						</div>
						<form role="form" action="" id="loginForm">
							<fieldset>
								<div class="form-group">
									<div class="col-md-12">
										<div id="errors"></div>
									</div>
								</div>
								<div class="form-group input-group">
									<span class="input-group-addon"><i
										class="fa fa-envelope-o fa-fw"></i></span> <input
										class="form-control" placeholder="E-mail" id="email"
										name="email" type="email">
								</div>
								<div class="form-group input-group">
									<span class="input-group-addon"><i
										class="fa fa-paw fa-fw"></i></span> <input class="form-control"
										placeholder="Password" id="password" name="password"
										type="password" value="">
								</div>
								<div class="checkbox">
									<label> <input name="remember_me" id="remember_me"
										type="checkbox" value="Remember Me">Remember Me
									</label>
								</div>
								<!-- Change this to a button or input when using this as a form -->
								<button class="btn btn-lg btn-success btn-block" type="submit">Sign
									in</button>
							</fieldset>
						</form>
					</div>
					<div>
						<span class="pull-right text-muted small" style="font-size: 10px;"><a
							href="http://www.hashcodesys.com" target="_blank">Powered By
								: hashCode Solutions</a></span>
					</div>
				</div>
			</div>
		</div>
	</div>

	<!-- jQuery Version 1.11.0 -->
	<script src="javascript/bootstrap/js/jquery-1.11.0.js"></script>

	<!-- Bootstrap Core JavaScript -->
	<script src="javascript/bootstrap/js/bootstrap.min.js"></script>

	<script type="text/javascript"
		src="javascript/bootstrap/js/plugins/validator/bootstrapValidator.min.js"></script>

	<script type="text/javascript">
		$(document).ready(function() {
			
			 if (localStorage.chkbx && localStorage.chkbx != '') {
	                $('#remember_me').attr('checked', 'checked');
	                $('#email').val(localStorage.email);
	                $('#password').val(localStorage.pass);
	            } else {
	                $('#remember_me').removeAttr('checked');
	                $('#email').val('');
	                $('#password').val('');
	            }

			validateAndSubmitForm();
		});

		function validateAndSubmitForm() {
			$('#formErrorAlert').hide();

			$('#loginForm')
					.bootstrapValidator(
							{
								container: '#errors', 
								feedbackIcons : {
									valid : '',
									invalid : '',
									validating : 'glyphicon glyphicon-refresh'
								},
								fields : {
									email : {
										validators : {
											notEmpty : {
												message : 'Email is required<br/>'
											},
											emailAddress : {
												message : ''
											}
										}
									},
									password : {
										validators : {
											notEmpty : {
												message : 'Password is required<br/>'
											}
										}
									}
								}
							}).on('success.form.bv', function(e) {
								// Prevent form submission
								e.preventDefault();
		
								var email = $('#email').val();
								var password = $('#password').val();
								
								var postData = {
									email : email,
									password : password,
								}
		
								authenticateLogin("./app/authenticate", postData);
					});
		}
		
		function rememberMe() {
			if ($('#remember_me').is(':checked')) {
                // save username and password
                localStorage.email = $('#email').val();
                localStorage.pass = $('#password').val();
                localStorage.chkbx = $('#remember_me').val();
            } else {
                localStorage.email = '';
                localStorage.pass = '';
                localStorage.chkbx = '';
            }
		}
		

		function authenticateLogin(posturl, postData) {
			var responseData;

			$.ajax({
				type : 'POST',
				contentType : "application/json; charset=utf-8",
				dataType : "json",
				url : posturl,
				data : JSON.stringify(postData),
				success : function(data) {
					resData = data;

					if (resData.status == '1000') {
					    rememberMe();
						
						window.location = "./app/index";
					} else {
						$('#formErrorAlert').show();
					}
				},
				error : function(xhr, status) {
					$('#formErrorAlert').show();
				},
				async : false
			});
			return responseData;
		}
	</script>

</body>

</html>
