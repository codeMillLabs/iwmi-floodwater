<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html lang="en">

<jsp:include page="layout/header.jsp" />
<body>

	<div id="wrapper">
		<jsp:include page="layout/menu.jsp" />

		<!-- Page Content -->
		<div id="page-wrapper">
			<div class="row">
				<div class="col-lg-12">
					<h2 class="page-header">Water Level Measuring Point</h2>
				</div>
				<!-- /.col-lg-12 -->
			</div>
			<div class="row">
				<div class="col-lg-12">
					<jsp:include page="content/AddWaterMeasuringPoint-body.jsp" />
				</div>
				<!-- /.col-lg-12 -->
			</div>
			<!-- /.row -->
		</div>
		<!-- /#page-wrapper -->

	</div>
	<!-- /#wrapper -->

	<jsp:include page="layout/footer.jsp" />

	<script type="text/javascript">
	$(document).ready(function() {
		
		validateAndSubmitForm();
		
		var waterMeasuringPoint = fetchWaterMeasuringPoint('./loadMeasuringPoints', '');
		renderDataTable(waterMeasuringPoint);
	});

	function validateAndSubmitForm() {
		$('#addMeasuringPointForm').bootstrapValidator({
			feedbackIcons : {
								valid : 'glyphicon glyphicon-ok',
								invalid : 'glyphicon glyphicon-remove',
								validating : 'glyphicon glyphicon-refresh'
							},
							fields : {
								measuringName : {
									validators : {
										notEmpty : {
											message : 'Measuring Point is required'
										}
									}
								},
								latitude : {
									validators : {
										notEmpty : {
											message : 'Latitude is required'
										},
										numeric : {
											message : 'Value should be numeric'
										}
									}
								},
								longitute : {
									validators : {
										notEmpty : {
											message : 'Longitute is required'
										},
										numeric : {
											message : 'Value should be numeric'
										}
									}
								},
								moderateLevelThreshold : {
									validators : {
										notEmpty : {
											message : 'Moderate level is required'
										},
										numeric : {
											message : 'Value should be numeric'
										}
									}
								},
								riskLevelThreshold : {
									validators : {
										notEmpty : {
											message : 'Risk level is required'
										},
										numeric : {
											message : 'Value should be numeric'
										}
									}
								}
							}
		}).on('success.form.bv',
				function(e) {
					// Prevent form submission
					e.preventDefault();

					var measuringName = $("#measuringName").val();
				    var latitude = $("#latitude").val();
				    var longitute = $("#longitute").val();
				    var moderateLevelThreshold = $("#moderateLevelThreshold").val();
				    var riskLevelThreshold = $("#riskLevelThreshold").val();

					//save form
					var reqData = {
							measuringName : measuringName,
							latitude : latitude,
							longitute : longitute,
							moderateLevelThreshold : moderateLevelThreshold,
							riskLevelThreshold : riskLevelThreshold,
					};

					console.log("Water level measuring point  :"
							+ JSON.stringify(reqData));

					var response = saveMeasuringPoint('./saveMeasuringPoint', reqData);
				});
	}
		

		function saveMeasuringPoint(posturl, postData) {
			var responseData;

			$.ajax({
				type : 'POST',
				contentType : "application/json; charset=utf-8",
				dataType : "json",
				url : posturl,
				data : JSON.stringify(postData),
				success : function(data) {
					responseData = data;
				},
				error : function(xhr, status) {
					window.location = "";
				},
				async : false
			});

			return responseData;
		}
		
		
		function renderDataTable(rows) {

			var rowsUpdated = {};
			var dataofTable = [];
			for (var i = 0; i < rows.length; i++) {
				var responseRow = {
					measuringPoint : rows[i]['measuringPoint'],
					coordinate : rows[i]['coordinate'],
					moderateLevelThreshold : rows[i]['moderateLevelThreshold'],
					riskLevelThreshold : rows[i]['riskLevelThreshold'],
					action :  '<a href="./loadMeasuringPointEditForm?pointId=' + rows[i]['id']
					+ '">Edit</a> &nbsp;'+ '<a href="#" onClick="deletePoint('
							+ rows[i]['id'] + ')">Delete</a>'
				};
				dataofTable.push(responseRow);
			}
			rowsUpdated["rows"] = dataofTable;

			$('#water-measuring-table').DataTable({
				"bAutoWidth" : false,
				"bJQueryUI" : true,
				"aaData" : rowsUpdated.rows,
				"bDestroy" : true,
				"aoColumns" : [ {
					"sTitle" : "Measuring Point",
					"mData" : "measuringPoint",
					"sWidth" : "10%"
				}, {
					"sTitle" : "Coordinates (Lat/Long)",
					"mData" : "coordinate",
					"sWidth" : "30%"
				}, {
					"sTitle" : "Moderate Water Level",
					"mData" : "moderateLevelThreshold",
					"sWidth" : "20%"
				}, {
					"sTitle" : "Risk Water Level",
					"mData" : "riskLevelThreshold",
					"sWidth" : "20%"
				}, {
					"sTitle" : "Action",
					"mData" : "action",
					"sWidth" : "20%"
				} ]
			});
		}

		function fetchWaterMeasuringPoint(posturl, postData) {
			var responseData;

			$
					.ajax({
						type : 'POST',
						contentType : "application/json; charset=utf-8",
						dataType : "json",
						url : posturl,
						data : JSON.stringify(postData),
						success : function(data) {
							responseData = data;
						},
						error : function(xhr, status) {
							alert("Something went wrong during the data loading for table, Please try again");
						},
						async : false
					});

			return responseData;
		}

		function deletePoint(idValue) {
			bootbox
					.dialog({
						message : "Are you sure want to delete selected measuring point ?",
						title : "Confirm",
						buttons : {
							success : {
								label : "Cancel",
								className : "btn-success",
								callback : function() {
								}
							},
							danger : {
								label : "Ok",
								className : "btn-danger",
								callback : function() {

									var reqData = {
										id : idValue
									};
									var response = postRequest('deletePoint',
											reqData);

									renderDataTable(response);
								}
							}
						}
					});
		}
	</script>

</body>
</html>
