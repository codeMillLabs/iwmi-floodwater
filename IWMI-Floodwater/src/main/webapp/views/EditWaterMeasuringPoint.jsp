<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html lang="en">

<jsp:include page="layout/header.jsp" />
<body>

	<div id="wrapper">
		<jsp:include page="layout/menu.jsp" />

		<!-- Page Content -->
		<div id="page-wrapper">
			<div class="row">
				<div class="col-lg-12">
					<h2 class="page-header">Edit Water Level Measuring Point</h2>
				</div>
				<!-- /.col-lg-12 -->
			</div>
			<div class="row">
				<div class="col-lg-12">
					<jsp:include page="content/EditWaterMeasuringPoint-body.jsp" />
				</div>
				<!-- /.col-lg-12 -->
			</div>
			<!-- /.row -->
		</div>
		<!-- /#page-wrapper -->

	</div>
	<!-- /#wrapper -->

	<jsp:include page="layout/footer.jsp" />

	<script type="text/javascript">
	$(document).ready(function() {
		validateAndSubmitForm();
	});

	function validateAndSubmitForm() {
		$('#addMeasuringPointEidtForm').bootstrapValidator({
			feedbackIcons : {
								valid : 'glyphicon glyphicon-ok',
								invalid : 'glyphicon glyphicon-remove',
								validating : 'glyphicon glyphicon-refresh'
							},
							fields : {
								measuringName : {
									validators : {
										notEmpty : {
											message : 'Measuring Point is required'
										}
									}
								},
								latitude : {
									validators : {
										notEmpty : {
											message : 'Latitude is required'
										},
										numeric : {
											message : 'Value should be numeric'
										}
									}
								},
								longitute : {
									validators : {
										notEmpty : {
											message : 'Longitute is required'
										},
										numeric : {
											message : 'Value should be numeric'
										}
									}
								},
								moderateLevelThreshold : {
									validators : {
										notEmpty : {
											message : 'Moderate level is required'
										},
										numeric : {
											message : 'Value should be numeric'
										}
									}
								},
								riskLevelThreshold : {
									validators : {
										notEmpty : {
											message : 'Risk level is required'
										},
										numeric : {
											message : 'Value should be numeric'
										}
									}
								}
							}
		}).on('success.form.bv',
				function(e) {
					// Prevent form submission
					e.preventDefault();

					var pointId = $("#pointId").val();
					var measuringName = $("#measuringName").val();
				    var latitude = $("#latitude").val();
				    var longitute = $("#longitute").val();
				    var moderateLevelThreshold = $("#moderateLevelThreshold").val();
				    var riskLevelThreshold = $("#riskLevelThreshold").val();

					//save form
					var reqData = {
							pointId : pointId,
							measuringName : measuringName,
							latitude : latitude,
							longitute : longitute,
							moderateLevelThreshold : moderateLevelThreshold,
							riskLevelThreshold : riskLevelThreshold,
					};

					console.log("Water level measuring point  :"
							+ JSON.stringify(reqData));

					var response = saveMeasuringPoint('./updateMeasuringPoint', reqData);
				});
		}
		

		function saveMeasuringPoint(posturl, postData) {
			var responseData;

			$.ajax({
				type : 'POST',
				contentType : "application/json; charset=utf-8",
				dataType : "json",
				url : posturl,
				data : JSON.stringify(postData),
				success : function(data) {
					responseData = data;
				},
				error : function(xhr, status) {
					window.location = "";
				},
				async : false
			});

			return responseData;
		}
		
		
		function fetchWaterMeasuringPoint(posturl, postData) {
			var responseData;

			$
					.ajax({
						type : 'POST',
						contentType : "application/json; charset=utf-8",
						dataType : "json",
						url : posturl,
						data : JSON.stringify(postData),
						success : function(data) {
							responseData = data;
						},
						error : function(xhr, status) {
							alert("Something went wrong during the data loading for table, Please try again");
						},
						async : false
					});

			return responseData;
		}
	</script>

</body>
</html>
