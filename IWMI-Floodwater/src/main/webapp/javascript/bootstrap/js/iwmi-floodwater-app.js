$(function() {

	$('#side-menu').metisMenu();

});

// Loads the correct sidebar on window load,
// collapses the sidebar on window resize.
// Sets the min-height of #page-wrapper to window size
$(function() {
	$(window)
			.bind(
					"load resize",
					function() {
						topOffset = 50;
						width = (this.window.innerWidth > 0) ? this.window.innerWidth
								: this.screen.width;
						if (width < 768) {
							$('div.navbar-collapse').addClass('collapse')
							topOffset = 100; // 2-row-menu
						} else {
							$('div.navbar-collapse').removeClass('collapse')
						}

						height = (this.window.innerHeight > 0) ? this.window.innerHeight
								: this.screen.height;
						height = height - topOffset;
						if (height < 1)
							height = 1;
						if (height > topOffset) {
							$("#page-wrapper").css("min-height",
									(height) + "px");
						}
					})
})

/**
 * 
 * Get today's date in YYYY-MM-DD format
 * 
 * @returns {String}
 */
function todayDate() {
	var today = new Date();
	var dd = today.getDate();
	var mm = today.getMonth() + 1; // January is 0!

	var yyyy = today.getFullYear();
	if (dd < 10) {
		dd = '0' + dd
	}
	if (mm < 10) {
		mm = '0' + mm
	}
	return today = yyyy + '-' + mm + '-' + dd;
}

/**
 * Logout user
 * 
 * @param url url to user logout
 */
function logout(url) {
	$.ajax({
		type : 'GET',
		dataType : "json",
		url : url,
		success : function(data) {
			if (data.status == '1000') {
				window.location = "../signin.jsp";
			}
		},
		error : function(xhr, status) {
			
		},
		async : false
	});
}
