/*
 * FILENAME
 *     hashCode.charts.java
 *
 * FILE LOCATION
 *     $Source$
 *
 * VERSION
 *     $Id$
 *         @version       $Revision$
 *         Check-Out Tag: $Name$
 *         Locked By:     $Lockers$
 *
 * FORMATTING NOTES
 *     * Lines should be limited to 78 characters.
 *     * Files should contain no tabs.
 *     * Indent code using four-character increments.
 *
 * COPYRIGHT
 *     Copyright (C) 2013 hashCode Solutions Pty. Ltd. All rights reserved.
 *     This software is the confidential and proprietary information of
 *     hashCode Solutions ("Confidential Information"). You shall not
 *     disclose such Confidential Information and shall use it only in
 *     accordance with the terms of the licence agreement you entered into
 *     with hashCode Solutions.
 *
 *  @author Amila Silva
 * 
 */

/**
 *[ {
 key : "Cumulative Return",
 values : [ {
 "key" : "A Label",
 "value" : 29.765957771107
 }, {
 "key" : "B Label",
 "value" : 0
 }
 */
/**
 * Line charts.
 * 
 * @param chartData
 *            chart data as JSON
 * @param elementId
 *            element id where should place in page
 */
function discreteBarChart(elementId, chartData) {

	return nv.addGraph(function() {
		var chart = nv.models.discreteBarChart().x(function(d) {
			return d.key
		}) // Specify the data accessors.
		.y(function(d) {
			return d.value
		}).staggerLabels(true) // Too many bars and not enough room? Try
		// staggering labels.
		.tooltips(true) // Don't show tooltips
		.showValues(true) // ...instead, show the bar value right on top of
		// each bar.
		.margin({left: 35})
		.transitionDuration(350);

		chart.yAxis.axisLabel('Water Level(m)').axisLabelDistance(50);
		chart.yAxis.tickFormat(d3.format('.0f'));
		
		d3.select('#' + elementId).datum(chartData).call(chart);

		nv.utils.windowResize(chart.update);

		return chart;
	});
}

/*
 * Data sample: [ { "key" : "North America" , "values" : [ [ 1025409600000 ,
 * 23.041422681023] , [ 1028088000000 , 19.854291255832]] }, { "key" : "Asia" ,
 * "values" : [ [ 1025409600000 , 23.041422681023] , [ 1028088000000 ,
 * 19.854291255832]] } ]
 */

/**
 * Stacked Area Chart
 * 
 * @param chartData
 *            chart data as JSON
 * @param elementId
 *            element id where should place in page
 * @param xAxisLabel
 *            X axis label
 * @param yAxisLabel
 *            Y axis label
 */
function stackedAreaChart(elementId, chartData, xAxisLabel, yAxisLabel) {

	return nv.addGraph(function() {
		var chart = nv.models.stackedAreaChart()
		.margin({left: 5})
		.x(function(d) {
			return d[0]
		}) // We can modify the data accessor functions...
		.y(function(d) {
			return d[1]
		}) // ...in case your data is formatted differently.
		.useInteractiveGuideline(true) // Tooltips which show all data points.
		// Very nice!
		.rightAlignYAxis(false) // Let's move the y-axis to the right side.
		.transitionDuration(500).showControls(false) // Allow user to choose
		// 'Stacked', 'Stream',
		// 'Expanded' mode.
		.clipEdge(true);

		// Format x-axis labels with custom function.
		chart.xAxis.tickFormat(function(d) {
			//			return d3.time.format('%Y-%m-%d')(new Date(d))
			return d3.time.format('%d/%m')(new Date(d))
		});
		chart.xAxis.tickValues(function(d) {
			return new Date(d);
		});

		chart.yAxis.axisLabel('Water Level(m)');
		chart.yAxis.tickFormat(d3.format('.0f'));

		d3.select('#' + elementId).datum(chartData).call(chart);

		nv.utils.windowResize(chart.update);

		return chart;
	});
}

/**
 * Stacked Area Chart
 * 
 * @param chartData
 *            chart data as JSON
 * @param elementId
 *            element id where should place in page
 * @param xAxisLabel
 *            X axis label
 * @param yAxisLabel
 *            Y axis label
 */
function stackedAreaChartWithNoXAxisFormat(elementId, chartData, xAxisLabel,
		yAxisLabel) {

	return nv.addGraph(function() {
		var chart = nv.models.stackedAreaChart()
		// .margin({right: 100})
		.x(function(d) {
			return d[0]
		}) // We can modify the data accessor functions...
		.y(function(d) {
			return d[1]
		}) // ...in case your data is formatted differently.
		.useInteractiveGuideline(true) // Tooltips which show all data points.
		// Very nice!
		.rightAlignYAxis(false) // Let's move the y-axis to the right side.
		.transitionDuration(500).showControls(false) // Allow user to choose
		// 'Stacked', 'Stream',
		// 'Expanded' mode.
		.clipEdge(true);

		chart.yAxis.axisLabel('Water Level(m)');
		chart.yAxis.tickFormat(d3.format('.0f'));

		d3.select('#' + elementId).datum(chartData).call(chart);

		nv.utils.windowResize(chart.update);

		return chart;
	});
}

/**
 * Multi Bar chart.
 * 
 * @param chartData
 *            chart data as JSON
 * @param elementId
 *            element id where should place in page
 * @param showControls
 *            Allow user to switch between 'Grouped' and 'Stacked' mode
 * @param xAxisLabel
 *            x axis label
 * @param yAxisLabel
 *            y axis label
 */
function multiBarChart(elementId, chartData, showControls, xAxisLabel,
		yAxisLabel) {
	return nv.addGraph(function() {
		var chart = nv.models.multiBarChart().x(function(d) {
			return d.label;
		}).y(function(d) {
			return d.value;
		}).transitionDuration(500).reduceXTicks(true) // If 'false',
		// every single
		// x-axis tick
		// label will be rendered.
		.showControls(showControls) // Allow user to switch between
		// 'Grouped' and 'Stacked' mode.
		.groupSpacing(0.2) // Distance between each group of bars.
		;

		chart.yAxis.axisLabel('Water Level(m)');
		d3.select('#' + elementId).datum(chartData).call(chart);
		nv.utils.windowResize(chart.update);

		return chart;
	});
}

/**
 * Cumulative Line Chart.
 * 
 * @param elementId
 *            element id where should place in page
 * @param chartData
 *            chart data as JSON
 * @param showControls
 *            Allow user to switch between 'Grouped' and 'Stacked' mode
 * @param xAxisLabel
 *            x axis label
 * @param yAxisLabel
 *            y axis label
 */
function cumulativeLineChart(elementId, chartData, showControls, xAxisLabel,
		yAxisLabel) {

	return nv.addGraph(function() {
		var chart = nv.models.cumulativeLineChart().x(function(d) {
			return d[0]
		}).y(function(d) {
			return d[1]
		}).color(d3.scale.category10().range()).useInteractiveGuideline(true)
		//.transitionDuration(500)
		//.showControls(false)
		//.clipEdge(true)
		;

		// Format x-axis labels with custom function.
		chart.xAxis.tickFormat(function(d) {
			return d3.time.format('%d/%m')(new Date(d))
		});

		chart.yAxis.tickFormat(d3.format('.0f'));
		d3.select('#' + elementId).datum(chartData).call(chart);
		nv.utils.windowResize(chart.update);
		return chart;
	});
}

/**
 * Cumulative Line Chart.
 * 
 * @param elementId
 *            element id where should place in page
 * @param chartData
 *            chart data as JSON
 * @param showControls
 *            Allow user to switch between 'Grouped' and 'Stacked' mode
 * @param xAxisLabel
 *            x axis label
 * @param yAxisLabel
 *            y axis label
 */
function cumulativeLineChartWithYear(elementId, chartData, showControls,
		xAxisLabel, yAxisLabel) {

	return nv.addGraph(function() {
		var chart = nv.models.cumulativeLineChart().x(function(d) {
			return d[0]
		}).y(function(d) {
			return d[1]
		}).color(d3.scale.category10().range()).useInteractiveGuideline(true)
		//.transitionDuration(500)
		//.showControls(false)
		//.clipEdge(true)
		;

		// Format x-axis labels with custom function.
		chart.xAxis.tickFormat(function(d) {
			return d
		});

		chart.yAxis.tickFormat(d3.format('.0f'));
		d3.select('#' + elementId).datum(chartData).call(chart);
		nv.utils.windowResize(chart.update);
		return chart;
	});
}

/*
 * [ { "key": "One", "value" : 29.765957771107 } , { "key": "Two", "value" : 0 } ]
 */
/**
 * PIE Chart.
 * 
 * @param elementId
 *            element id where should place in page
 * @param chartData
 *            chart data as JSON
 * @param showLabels
 *            Display pie labels
 * @param isDonut
 *            Turn on Donut mode. Makes pie chart look tasty!
 */
function pieChart(elementId, chartData, showLabels, isDonut) {
	return nv.addGraph(function() {
		var chart = nv.models.pieChart().x(function(d) {
			return d.key
		}).y(function(d) {
			return d.value
		}).showLabels(showLabels) // Display pie labels
		.labelThreshold(.05) // Configure the minimum slice size for labels
		// to show up
		.labelType("value") // Configure what type of data to show in the label.
		// Can be "key", "value" or "percent"
		.donut(isDonut) // Turn on Donut mode. Makes pie chart look tasty!
		.donutRatio(0.35) // Configure how big you want the donut hole size to
		// be.
		;

		d3.select("#" + elementId).datum(chartData).transition().duration(350)
				.call(chart);

		return chart;
	});
}

/**
 * C3 based multi line chart
 * 
 * @param legend
 *            data series map
 * @param columnsDataArray
 *           each column data
 */
function multiLineChart_c3(elementId, columnsDataArray) {
	var chart = c3.generate({
		bindto : '#' + elementId,
		data : {
			x : 'x',
			columns : columnsDataArray
		},
		legend : {
			show : true
		},
		tooltip : {
			show : true
		},
	    grid: {
	        x: {
	            show: false
	        },
	        y: {
	            show: true
	        }
	    },
	    axis: {
	        y: {
	            label: {
	            	text: 'Water Level(m)',
	            	position: 'outer-middle',
	            },
	            padding: {top:0, bottom:0},
	            tick : {format : function (d) { return d.toFixed(0); }}
	        }
	    }
	});
}

// columns: [
// ['x', '2013-01-01', '2013-01-02', '2013-01-03', '2013-01-04', '2013-01-05',
// '2013-01-06'],
////          ['x', '20130101', '20130102', '20130103', '20130104', '20130105', '20130106'],
//          ['data1', 30, 200, 100, 400, 150, 250],
//          ['data2', 130, 340, 200, 500, 250, 350]
//      ]
function timeSeriesLineChart(elementId, timeFormat, tickCount, columnsDataArray) {
	
	tickCount = (tickCount) ? tickCount : 0;
	
	var chart = c3.generate({
		bindto : '#' + elementId,
		data : {
			x : 'x',
			columns : columnsDataArray
		},
		axis : {
			x : {
				type : 'timeseries',
				tick : {
					count: tickCount,
					format : timeFormat
				//'%Y-%m-%d'
				}
			},
			 y: {
		            label: {
		            	text: 'Water Level(m)',
		            	position: 'outer-middle',
		            },
		            padding: {top:0, bottom:0},
		            tick : {format : function (d) { return d.toFixed(0); }}
		        }
		},
		legend : {
			show : true
		},
		tooltip : {
			show : true
		},
	    grid: {
	        x: {
	            show: false
	        },
	        y: {
	            show: true
	        }
	    }
	});
}

