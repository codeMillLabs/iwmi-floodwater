/*
 * FILENAME
 *     TestCreateData.java
 *
 * FILE LOCATION
 *     $Source$
 *
 * VERSION
 *     $Id$
 *         @version       $Revision$
 *         Check-Out Tag: $Name$
 *         Locked By:     $Lockers$
 *
 * FORMATTING NOTES
 *     * Lines should be limited to 78 characters.
 *     * Files should contain no tabs.
 *     * Indent code using four-character increments.
 *
 * COPYRIGHT
 *     Copyright (C) 2014 hashCode Solutions Pty. Ltd. All rights reserved.
 *     This software is the confidential and proprietary information of
 *     hashCode Solutions ("Confidential Information"). You shall not
 *     disclose such Confidential Information and shall use it only in
 *     accordance with the terms of the licence agreement you entered into
 *     with hashCode Solutions.
 */

package com.hashcode.iwmi.floodwater.test.data;

import java.time.LocalDate;

//
// IMPORTS
// NOTE: Import specific classes without using wildcards.
//

public class TestCreateData
{

    private String filePath;

    private static String pointsSQL =
        "INSERT WATER_LEVEL_MEASURING_POINT(id, measuring_point, latitude, longitude, moderate_level, risk_level) "
            + " VALUES( '%d', '%s', %f, %f, %.3f, %.3f); %n";

    private static String actualDataSQL =
        "INSERT ACTUAL_WATER_LEVEL(`created_date`, `last_updated_date`, `version`, `comment`, `meassured_date`, `user_id`, `water_level`, `measuringPoint_id`)"
            + " VALUES ('%s', '%s', 0, '', '%s', 3, %.3f, %d); %n";
    private static String predictedDataSQL =
        "INSERT PREDICT_WATER_LEVEL(`created_date`, `last_updated_date`, `version`, `comment`, `meassured_date`, `user_id`, `water_level`, `measuringPoint_id`) "
            + " VALUES ('%s', '%s', 0, '', '%s', 3, %.3f, %d); %n";

    private static String[] points = {
        "Ibi-Amar", "Ibi-Wukari", "Makadi", "Nassarawa", "Lakoja"
    };
    
    private static double[] waterLevels = {
        871.08,884.46,904.75,921.25,917.08,910.58,899.67,887.83,883.75,883.67,884.50,889.13,887.54,892.46,891.25,888.33,899.08,905.42,908.57,911.75,917.08,932.29,944.58,948.00,955.00,965.58,977.54,984.63,986.46,989.67,994.25,999.00,1006.33,1011.58,1019.67,1023.38,1029.83,1036.25,1048.42,1064.38,1086.04,1115.88,1142.17,1162.67,1184.50,1204.50,1219.58,1226.13,1227.33,1225.75,1224.88,1233.58,1246.21,1254.71,1256.75,1254.54,1247.13,1239.88,1229.00,1222.33,1208.92,1195.79,1186.38,1178.83,1175.54,1171.75,1171.67,1169.67
    };

    public static void main(String[] a)
    {
        // Measuring points
        int pointCountId = 1;
        for (String point : points)
        {
//                        System.out.format(pointsSQL, pointCountId, point, 10.33379015523481, 8.686851614725279,
//                            50.0 + (pointCountId * 5), 70.0 + (pointCountId * 5));

            pointCountId++;
        }

        System.out.println("");

        // Actual Data
       
        LocalDate date = LocalDate.now().minusDays(waterLevels.length);

        
        pointCountId = 1;
        int waterLevelChangeConstant = 10;
        for (String point : points)
        {
            waterLevelChangeConstant = (int) (waterLevelChangeConstant + Math.random() * (80 - 10f));
            for (int i = 0; i < waterLevels.length; i++)
            {
                double waterLevel = waterLevels[i] + waterLevelChangeConstant;
//                System.out.format(actualDataSQL, date, date, date.plusDays(i + 1), waterLevel/1000, pointCountId);
            }
            pointCountId++;
        }

        System.out.println("");

        date = LocalDate.now().plusDays(1);
        waterLevelChangeConstant = 10;
        pointCountId = 1;
        for (String point : points)
        {
            waterLevelChangeConstant = (int) (waterLevelChangeConstant + Math.random() * (80 - 10f));
            for (int i = 0; i < waterLevels.length; i++)
            {
                double waterLevel = waterLevels[i] + waterLevelChangeConstant;
                System.out.format(predictedDataSQL, date, date, date.plusDays(i), waterLevel/1000, pointCountId);
//                System.out.format(actualDataSQL, date, date, date.plusDays(i + 1), waterLevel/1000, pointCountId);
            }
            pointCountId++;
        }

    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString()
    {
        return String.format("TestCreateData [filePath=%s]", filePath);
    }

}
