/*
 * FILENAME
 *     TestPredictedWaterLevelDaoImpl.java
 *
 * FILE LOCATION
 *     $Source$
 *
 * VERSION
 *     $Id$
 *         @version       $Revision$
 *         Check-Out Tag: $Name$
 *         Locked By:     $Lockers$
 *
 * FORMATTING NOTES
 *     * Lines should be limited to 78 characters.
 *     * Files should contain no tabs.
 *     * Indent code using four-character increments.
 *
 * COPYRIGHT
 *     Copyright (C) 2013 hashCode Solutions Pty. Ltd. All rights reserved.
 *     This software is the confidential and proprietary information of
 *     hashCode Solutions ("Confidential Information"). You shall not
 *     disclose such Confidential Information and shall use it only in
 *     accordance with the terms of the licence agreement you entered into
 *     with hashCode Solutions.
 */

package com.hashcode.iwmi.floodwater.test.dao;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

import java.time.LocalDate;
import java.util.List;

import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.hashcode.iwmi.floodwater.dao.PredictedWaterLevelDao;
import com.hashcode.iwmi.floodwater.domain.PredictedWaterLevel;
import com.hashcode.iwmi.floodwater.domain.WaterLevelMeasuringPoint;

//
// IMPORTS
// NOTE: Import specific classes without using wildcards.
//

/**
 * <p>
 * Test PredictedWaterLevelDaoImpl class.
 * </p>
 *
 * @author Manuja
 *
 * @version $Id$
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
    "classpath:/entitymanager.xml"
})
public class TestPredictedWaterLevelDaoImpl
{
    @Autowired
    private PredictedWaterLevelDao predictedWaterLevelDao;

    @Transactional
    @Test
    public void testGetActualWaterLevelsSuccess()
    {
        WaterLevelMeasuringPoint waterLevelMeasuringPoint = new WaterLevelMeasuringPoint();
        waterLevelMeasuringPoint.setName("PWL TEST 01");

        PredictedWaterLevel predictedWaterLevel = new PredictedWaterLevel();
        predictedWaterLevel.setMeasuringPoint(waterLevelMeasuringPoint);
        waterLevelMeasuringPoint.addPredictedReading(predictedWaterLevel);
        predictedWaterLevel.setDate(LocalDate.now());

        predictedWaterLevelDao.create(predictedWaterLevel);

        List<PredictedWaterLevel> predActualWaterLevels =
            predictedWaterLevelDao.getPredictedWaterLevels("PWL TEST 01", LocalDate.now().minusDays(2), LocalDate.now()
                .plusDays(2));
        assertThat(predActualWaterLevels.size(), is(1));
    }
    

    @Transactional
    @Test
    public void testGetActualWaterLevelsWrongName()
    {
        WaterLevelMeasuringPoint waterLevelMeasuringPoint = new WaterLevelMeasuringPoint();
        waterLevelMeasuringPoint.setName("AWL TEST 02");

        PredictedWaterLevel predictedWaterLevel = new PredictedWaterLevel();
        predictedWaterLevel.setMeasuringPoint(waterLevelMeasuringPoint);
        waterLevelMeasuringPoint.addPredictedReading(predictedWaterLevel);
        predictedWaterLevel.setDate(LocalDate.now());

        predictedWaterLevelDao.create(predictedWaterLevel);

        List<PredictedWaterLevel> predActualWaterLevels =
            predictedWaterLevelDao.getPredictedWaterLevels("Wrong Name", LocalDate.now().minusDays(2), LocalDate.now()
                .plusDays(2));
        assertThat(predActualWaterLevels.size(), is(0));
    }
    

    @Transactional
    @Test
    public void testGetActualWaterLevelsWrongPeriod()
    {
        WaterLevelMeasuringPoint waterLevelMeasuringPoint = new WaterLevelMeasuringPoint();
        waterLevelMeasuringPoint.setName("AWL TEST 03");

        PredictedWaterLevel predictedWaterLevel = new PredictedWaterLevel();
        predictedWaterLevel.setMeasuringPoint(waterLevelMeasuringPoint);
        waterLevelMeasuringPoint.addPredictedReading(predictedWaterLevel);
        predictedWaterLevel.setDate(LocalDate.now());

        predictedWaterLevelDao.create(predictedWaterLevel);

        List<PredictedWaterLevel> predActualWaterLevels =
            predictedWaterLevelDao.getPredictedWaterLevels("AWL TEST 03", LocalDate.now().plusDays(1), LocalDate.now()
                .plusDays(2));
        assertThat(predActualWaterLevels.size(), is(0));
    }
}
