/*
 * FILENAME
 *     TestUserDaoImpl.java
 *
 * FILE LOCATION
 *     $Source$
 *
 * VERSION
 *     $Id$
 *         @version       $Revision$
 *         Check-Out Tag: $Name$
 *         Locked By:     $Lockers$
 *
 * FORMATTING NOTES
 *     * Lines should be limited to 78 characters.
 *     * Files should contain no tabs.
 *     * Indent code using four-character increments.
 *
 * COPYRIGHT
 *     Copyright (C) 2013 hashCode Solutions Pty. Ltd. All rights reserved.
 *     This software is the confidential and proprietary information of
 *     hashCode Solutions ("Confidential Information"). You shall not
 *     disclose such Confidential Information and shall use it only in
 *     accordance with the terms of the licence agreement you entered into
 *     with hashCode Solutions.
 */

package com.hashcode.iwmi.floodwater.test.dao;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.hashcode.iwmi.floodwater.dao.UserDao;
import com.hashcode.iwmi.floodwater.domain.User;

//
// IMPORTS
// NOTE: Import specific classes without using wildcards.
//

/**
 * <p>
 * Test UserDaoImpl class.
 * </p>
 *
 * @author Manuja
 *
 * @version $Id$
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
    "classpath:/entitymanager.xml"
})
public class TestUserDaoImpl
{
    @Autowired
    private UserDao userDao;
    
    @Transactional
    @Test
    public void testAuthenticateUserSuccess()
    {
        User user = new User();
        user.setEmail("testusername01");
        user.setPassword("testpassword01");
        
        userDao.create(user);
        
        User authenticatedUser = userDao.authenticateUser("testusername01", "testpassword01");
        assertNotNull(authenticatedUser);
    }
    
    @Transactional
    @Test
    public void testAuthenticateUserFailure()
    {
        User user = new User();
        user.setEmail("testusername02");
        user.setPassword("testpassword02");
        
        userDao.create(user);
        
        User authenticatedUser = userDao.authenticateUser("worngusername01", "wrongpasswor01");
        assertNull(authenticatedUser);
    }
}